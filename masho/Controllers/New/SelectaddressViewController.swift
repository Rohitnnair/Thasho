//
//  SelectaddressViewController.swift
//  Masho
//
//  Created by WC46 on 22/09/20.
//  Copyright © 2020 WC46. All rights reserved.
//

import UIKit
import KVSpinnerView
import Kingfisher
class SelectaddressViewController: UIViewController{
    var UserID : String = ""
    var guest_id : Int = 0
    var TableID : String = ""
    var delegate : displayNewVCdelegate! = nil
    var AddressData : viewAddressModel! = nil
    @IBOutlet var selectaddresstableview:UITableView!
    @IBOutlet weak var BottomBTNview: BaseView!
    @IBOutlet weak var BottomBTNlb: UILabel!
    @IBOutlet weak var dashedBorder:UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var ConfirmationMessage: UILabel!
    @IBOutlet weak var DeleteBTN: UIButton!
    @IBOutlet weak var EmptyView: UIView!
    var index  = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        selectaddresstableview.delegate = self
        selectaddresstableview.dataSource = self
        popupView.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getAddress()
    }
    @IBAction func AddNewBTNtapped(_ sender: Any) {
        let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHAdd_AddressVC") as! MHAdd_AddressVC
        vc.delegate = self
        vc.ForEdit = false
        vc.forAddNewAddress = true
        delegate.displayNewVC(VC: vc)
    }
    @IBAction func performTap(_ sender: UITapGestureRecognizer) {
        popupView.isHidden = true
    }
    @IBAction func popupCancelBTNtapped(_ sender: Any) {
        popupView.isHidden = true
    }
    @IBAction func popUpDeleteBTNtapped(_ sender: Any) {
        DeleteAddress(adrsId: AddressData.address.address[IndexForDelete].address_id)
    }
    var IndexForDelete = 0
    @IBAction func deliverhereEditBTNtapped(_ sender: Any) {
        if AddressData.address != nil{
            let filteredArray = AddressData.address.address.filter{$0.defultaddress == "0"}
            if AddressData.address.address.count != filteredArray.count{
//                if AddressData.address.address[0].defultaddress == "1"{
//                    index = AddressData.address.address.firstIndex(where: {$0.address_id == filteredArray[0].address_id})!
//                }
                setDefaultAddress(adrsId:AddressData.address.address[index].address_id)
                
            }else{
                Banner.main.showBanner(title: "", subtitle: "Please choose your delivery address", style: .danger)
            }
        }else{
            Banner.main.showBanner(title: "", subtitle: "Please add your delivery address", style: .danger)

        }
    }
}
extension SelectaddressViewController:UITableViewDelegate,UITableViewDataSource,displayNewVCdelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if AddressData != nil{
                if AddressData.address != nil{
                    return  AddressData.address.address.count
                }else{
                    return 0
                }
            }
            return 0
        }else{
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier:"SelectaddressTableViewCell",for: indexPath)as!SelectaddressTableViewCell
            cell.optionCV.tag = indexPath.row
            if AddressData != nil{
                if AddressData.address != nil{
                    cell.address.text = AddressData.address.address[indexPath.row].customer_name
                    cell.addressdetails.text = "\(AddressData.address.address[indexPath.row].address ?? "")\n\(AddressData.address.address[indexPath.row].mobile!)"
                    cell.TypeLb.text = AddressData.address.address[indexPath.row].addresstype
                    if index == indexPath.row || index == indexPath.row{
                        cell.radiobuttonimage.image = UIImage(named:"square-3")
                        cell.radiobuttonimage.backgroundColor = UIColor.MHGold.withAlphaComponent(0.5)
                    }else{
                        cell.radiobuttonimage.image = UIImage(named:"square-1")
                        cell.radiobuttonimage.backgroundColor = UIColor.clear
                    }
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier:"SelectaddressBTNTVC",for: indexPath) as! SelectaddressBTNTVC
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if AddressData != nil{
                if AddressData.address.address.count != 0{
                    return UITableView.automaticDimension
                }
                return 0
            }else{
                return 0
            }
            
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.section == 0{
            index = indexPath.row
            tableView.reloadData()
            
        }else{
            let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHAdd_AddressVC") as! MHAdd_AddressVC
            vc.delegate = self
            vc.ForEdit = false
            vc.forAddNewAddress = true
            delegate.displayNewVC(VC: vc)
        }
    }
    func displayNewVC(VC: UIViewController){
        delegate.displayNewVC(VC: VC)
    }
}
extension SelectaddressViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.AddressData != nil{
            return self.AddressData.address.options.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectaddressCollectionViewCell", for: indexPath) as! SelectaddressCollectionViewCell
        if self.AddressData != nil{
            cel.title.text = self.AddressData.address.options[indexPath.row].name
            cel.title.textColor = UIColor(hex: "\(self.AddressData.address.options[indexPath.row].btndata.color!)ff")
            cel.BGView.backgroundColor = UIColor(hex: "\(self.AddressData.address.options[indexPath.row].btndata.background!)ff")
            cel.Icon.kf.setImage(with: URL(string: self.AddressData.address.options[indexPath.row].btndata.icon!))
        }
        return cel
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = ((self.AddressData.address.options[indexPath.row].name)?.size(withAttributes: nil).width)!
        return CGSize(width: width + 50, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print(collectionView.tag)
        if self.AddressData.address.options[indexPath.row].name == "Edit"{
            let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHAdd_AddressVC") as! MHAdd_AddressVC
            vc.delegate = self
            vc.ForEdit = true
            vc.forAddNewAddress = false
            vc.AddressDataForEdit = AddressData.address.address[collectionView.tag]
            delegate.displayNewVC(VC: vc)
        }
        if self.AddressData.address.options[indexPath.row].name == "Delete"{
            popupView.isHidden = false
            IndexForDelete = collectionView.tag
        }
    }
}
extension SelectaddressViewController{
    func getAddress(){
        EmptyView.isHidden = false
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = [  "action":"viewaddress",
                            "userId":"\(UserID)",
            "tableid": TableID] as [String : Any]
//        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    self.AddressData = viewAddressModel(fromData: data)
                }
                if self.AddressData.address != nil{
                    for i in 0 ..< self.AddressData.address.address.count{
                        if self.AddressData.address.address[i].defultaddress == "1"{
                            self.index = i
                        }
                    }
                }else{
                    self.index = -1
                }
                
//                let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHBaseViewVC") as! MHBaseViewVC
//                super.TitleLb.text = self.AddressData.mainheading
                self.BottomBTNview.backgroundColor = UIColor(hex: "\(self.AddressData.btndata.background!)ff")
                self.BottomBTNlb.text = self.AddressData.btndata.value
                self.BottomBTNlb.textColor = UIColor(hex: "\(self.AddressData.btndata.color!)ff")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                self.selectaddresstableview.reloadData()
//                self.selectaddresstableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: false)
                self.EmptyView.isHidden = true
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func setDefaultAddress(adrsId:String){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""//"678"//
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = ["action":"savedeliveraddress",
                          "userId":"\(UserID)",
            "addressid":adrsId,
            "tableid": TableID] as [String : Any]
//        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHReviewVC") as! MHReviewVC
                vc.delegate = self
                self.delegate.displayNewVC(VC: vc)
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func DeleteAddress(adrsId:String){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""//"678"//
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = [  "action":"deleteaddress",
                            "userId":"\(UserID)",
            "addressid": adrsId,
            "tableid": TableID] as [String : Any]
//        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                self.popupView.isHidden = true
                self.getAddress()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Failed", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
