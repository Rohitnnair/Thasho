//
//  SuccessfullViewController.swift
//  masho
//
//  Created by WC46 on 23/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
var isorderPlaced = false
class SuccessfullViewController: UIViewController {
    @IBOutlet weak var mainheadLb:UILabel!
    @IBOutlet weak var ordertextLb:UILabel!
    @IBOutlet weak var orderidLb:UILabel!
    @IBOutlet weak var subheadLb:UILabel!
    @IBOutlet weak var BGView:UIView!
    @IBOutlet weak var TitleLb:UILabel!
    var UserID : String = ""
    var order_id : String = ""
    var TableID : String = ""
    var succesfullData :MHPaymentSuccessModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        isorderPlaced = true
        getSuccessFullData()
    }
    
    @IBAction func ContinueShoppingBTNtapped(_ sender: Any) {
        let navigationStack = navigationController?.viewControllers ?? []
        for vc in navigationStack{
            if let dashboardVC = vc as? MHdashBoardVC{
                DispatchQueue.main.async {
                    self.navigationController?.popToViewController(dashboardVC, animated: true)
                }
            }
        }
    }
    

}
extension SuccessfullViewController{
    func getSuccessFullData(){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        order_id = UserDefaults.standard.value(forKey: "orderid") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = ["action":"orderconfirmation",
                          "userId":"\(UserID)",
                          "orderid":order_id,
                          "tableid": TableID] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response as? [String: Any]{
                    self.succesfullData = MHPaymentSuccessModel(fromData: data)
                }
                
                self.mainheadLb.text = self.succesfullData.data.mainhead
                self.ordertextLb.text = "\(self.succesfullData.data.ordertext!) :"
                self.orderidLb.text = self.succesfullData.data.orderid
                self.subheadLb.text = self.succesfullData.data.subhead
                self.BGView.backgroundColor = UIColor(hex: "\(self.succesfullData.btndata.background!)ff")
                self.TitleLb.text = self.succesfullData.btndata.value
                self.TitleLb.textColor = UIColor(hex: "\(self.succesfullData.btndata.color!)ff")
                
                UserDefaults.standard.set("", forKey: "orderid")
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
