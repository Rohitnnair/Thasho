//
//  PaymentModels.swift
//  masho
//
//  Created by Castler on 28/10/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class MHPaymentModel{
    var btndata:MHPaymentBTNdataModel!
    var paymentoptions = [MHPaymentOptionsModel]()
    var pricedetails:OrderSummaryPriceModel!
    var paymentwebpages:MHPaymentWebpageOptionsModel!
    init(fromData data: [String:Any]) {
        if let btnData = data["btndata"] as? [String:Any]{
            self.btndata = MHPaymentBTNdataModel(fromData: btnData)
        }
        if let arrayData = data["paymentoptions"] as? [[String:Any]]{
            for item in arrayData{
                self.paymentoptions.append(MHPaymentOptionsModel(fromData: item))
            }
        }
        if let pricedetailsdatas = data["pricedetails"] as? [String:Any]{
            self.pricedetails = OrderSummaryPriceModel(fromData: pricedetailsdatas)
        }
        if let webdetailsdatas = data["paymentwebpages"] as? [String:Any]{
            self.paymentwebpages = MHPaymentWebpageOptionsModel(fromData: webdetailsdatas)
        }
    }
}
class MHPaymentBTNdataModel{
    var apiname:String!
    var background:String!
    var color:String!
    var value:String!
    init(fromData data: [String:Any]) {
        self.apiname = data["apiname"] as? String
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.value = data["value"] as? String
    }
}
class MHPaymentWebpageOptionsModel{
    var confirm:String!
    var failure:String!
    init(fromData data: [String:Any]) {
        self.confirm = data["confirm"] as? String
        self.failure = data["failure"] as? String
    }
}
class MHPaymentOptionsModel{
    var charge:String!
    var discount:String!
    var image:String!
    var name:String!
    var type:String!
    var url:String!
    init(fromData data: [String:Any]) {
        self.charge = data["charge"] as? String
        self.discount = data["discount"] as? String
        self.image = data["image"] as? String
        self.name = data["name"] as? String
        self.type = data["type"] as? String
        self.url = data["url"] as? String
    }
}
class MHPaymentPriceDetailsModel{
    var amountpayable:String!
    var entryloop:String!
    var text:String!
    init(fromData data: [String:Any]) {
        self.amountpayable = data["amountpayable"] as? String
        self.entryloop = data["entryloop"] as? String
        self.text = data["text"] as? String
    }
}
class MHPaymentSuccessModel{
    var data:MHPaymentSuccessdataModel!
    var btndata:MHPaymentSuccessBtndataModel!
    var errorcode:String!
    var message:String!
    init(fromData data: [String:Any]) {
        self.errorcode = data["errorcode"] as? String
        self.message = data["message"] as? String
        if let datas = data["data"] as? [String:Any]{
            self.data = MHPaymentSuccessdataModel(fromData: datas)
        }
        if let Btndatas = data["btndata"] as? [String:Any]{
            self.btndata = MHPaymentSuccessBtndataModel(fromData: Btndatas)
        }
    }
}
class MHPaymentSuccessdataModel{
    var mainhead:String!
    var orderid:String!
    var ordertext:String!
    var subhead:String!
    init(fromData data: [String:Any]) {
        self.mainhead = data["mainhead"] as? String
        self.orderid = data["orderid"] as? String
        self.ordertext = data["ordertext"] as? String
        self.subhead = data["subhead"] as? String
    }
}
class MHPaymentSuccessBtndataModel{
    var apiname:String!
    var background:String!
    var color:String!
    var value:String!
    init(fromData data: [String:Any]) {
        self.apiname = data["apiname"] as? String
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.value = data["value"] as? String
    }
}
