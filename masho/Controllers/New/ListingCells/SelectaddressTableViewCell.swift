//
//  SelectaddressTableViewCell.swift
//  Masho
//
//  Created by WC46 on 22/09/20.
//  Copyright © 2020 WC46. All rights reserved.
//

import UIKit

class SelectaddressTableViewCell: UITableViewCell {
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var addressdetails: UILabel!
    @IBOutlet weak var optionCV: UICollectionView!
    @IBOutlet weak var radiobuttonimage: UIImageView!
    @IBOutlet weak var TypeLb: UILabel!
    @IBOutlet weak var BGView: UIView!
}
class SelectaddressBTNTVC: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var BGView: UIView!
}
class SelectaddressCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var Icon:UIImageView!
}
class SelectaddressTableViewCell2: UITableViewCell {
    
}
class PaymentmodeTableViewCell1: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var gpayview: UIImageView!
    @IBOutlet weak var Paymentradiobuttonimage: UIImageView!
}
class PaymentmodeTableViewCell2: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tradiobuttonimageright: UIImageView!
    @IBOutlet weak var tradiobuttonimageleft: UIImageView!
}
class SetYourLocationCell1: UITableViewCell {
    //    @IBOutlet weak var title: UILabel!
    //    @IBOutlet weak var tradiobuttonimageright: UIImageView!
    //    @IBOutlet weak var tradiobuttonimageleft: UIImageView!
}
class SetYourLocationCell2: UITableViewCell {
    //    @IBOutlet weak var title: UILabel!
    //    @IBOutlet weak var tradiobuttonimageright: UIImageView!
    //    @IBOutlet weak var tradiobuttonimageleft: UIImageView!
}
class SetYourLocationCell3: UITableViewCell {
    @IBOutlet weak var setyourlocation: UILabel!
}
class SetYourLocationCell4: UITableViewCell {
    @IBOutlet weak var locationsubtitle: UILabel!
    @IBOutlet weak var textfiled: UITextField!
    @IBOutlet weak var TVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ContentTV: UITextView!
}
class SetYourLocationCell5: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
     var  collectionlabelarray = ["HOME","WORK","OTHER"]
     var index = 0
     @IBOutlet weak var homecollectionView: UICollectionView!
    override func awakeFromNib() {
       
        homecollectionView.dataSource = self
        homecollectionView.delegate = self
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
  return collectionlabelarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cel.collectionlabel.text = collectionlabelarray[indexPath.row]
        if index == indexPath.row
        {
            
            cel.collectionlabel.textColor = #colorLiteral(red: 0.831372549, green: 0.6862745098, blue: 0.2156862745, alpha: 1)
            cel.collectionlabelview.layer.borderColor = #colorLiteral(red: 0.831372549, green: 0.6862745098, blue: 0.2156862745, alpha: 1)
        }else{
            
            cel.collectionlabel.textColor = #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 1)
            cel.collectionlabelview.layer.borderColor = #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 1)
        }
        return cel
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
       return CGSize(width:collectionView.frame.width/3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        index = indexPath.row
        collectionView.reloadData()
    }
    //    @IBOutlet weak var title: UILabel!
    //    @IBOutlet weak var tradiobuttonimageright: UIImageView!
    //    @IBOutlet weak var tradiobuttonimageleft: UIImageView!
}
class SetYourLocationCell6: UITableViewCell {
    //    @IBOutlet weak var title: UILabel!
       @IBOutlet weak var savecheckimage: UIImageView!
       @IBOutlet weak var defautcheckimage: UIImageView!
    override func awakeFromNib() {
        
        savecheckimage.layer.borderColor = #colorLiteral(red: 0.831372549, green: 0.6862745098, blue: 0.2156862745, alpha: 1)
        defautcheckimage.layer.borderColor = #colorLiteral(red: 0.831372549, green: 0.6862745098, blue: 0.2156862745, alpha: 1)
       
        
    }
}
class SetYourLocationCell7: UITableViewCell {
    //    @IBOutlet weak var title: UILabel!
    //    @IBOutlet weak var tradiobuttonimageright: UIImageView!
    //    @IBOutlet weak var tradiobuttonimageleft: UIImageView!
}

