//
//  MyDropDownCell.swift
//  FlipSell
//
//  Created by WC46 on 13/08/19.
//  Copyright © 2019 flipsell. All rights reserved.
//

import UIKit
import DropDown

class MyDropDownCell: DropDownCell {

    @IBOutlet weak var logoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
