//
//  OrderSummaryModels.swift
//  masho
//
//  Created by Appzoc on 24/10/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class OrderSummaryModel{
    var address = [OrderSummaryAddressModel]()
    var cartitems = [OrderSummaryProductsModel]()
    var coupon : OrderSummaryCouponModel!
    var pricedetails : OrderSummaryPriceModel!
    var secondiv : OrderSummarySecondivModel!
    var btndata : viewAddress_btndataModel!
    init(fromData data: [String:Any]){
        if let addressList = data["address"] as? [[String:Any]]{
            address = [OrderSummaryAddressModel]()
            for item in addressList{
                self.address.append(OrderSummaryAddressModel(fromData: item))
            }
        }
        if let cartitemsList = data["cartitems"] as? [[String:Any]]{
            cartitems = [OrderSummaryProductsModel]()
            for item in cartitemsList{
                self.cartitems.append(OrderSummaryProductsModel(fromData: item))
            }
        }
        if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = viewAddress_btndataModel(fromData: btndatas)
        }
        if let coupondatas = data["coupon"] as? [String:Any]{
            self.coupon = OrderSummaryCouponModel(fromData: coupondatas)
        }
        if let pricedetailsdatas = data["pricedetails"] as? [String:Any]{
            self.pricedetails = OrderSummaryPriceModel(fromData: pricedetailsdatas)
        }
        if let secondivdatas = data["secondiv"] as? [String:Any]{
            self.secondiv = OrderSummarySecondivModel(fromData: secondivdatas)
        }
    }
}
class OrderSummaryAddressModel{
    var icon : String!
    var text : String!
    init(fromData data: [String:Any]){
        self.text = data["text"] as? String
        self.icon = data["icon"] as? String
    }
}
class OrderSummaryProductsModel{
    var description : String!
    var imageUrl : String!
    var price : String!
    var strikeprice : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var qty : String!
    var avalablestock : Int!
    var totalitemprice : String!
    var delivery : String!
    var deliveryMode : String!
    var favorite : String!
    var mashoAssured : String!
    var offerpercent : String!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var offers : String!
    var type : String!
    init(fromData data: [String:Any]) {
        self.description = data["description"] as? String
        self.imageUrl = data["imageUrl"] as? String
        self.strikeprice = data["strikeprice "] as? String
        self.price = data["price"] as? String
        self.productId = data["productId"] as? String
        self.productName = data["productName"] as? String
        self.productTitle = data["productTitle"] as? String
        self.qty = data["qty"] as? String
        self.avalablestock = data["avalablestock"] as? Int
        self.totalitemprice = data["totalitemprice"] as? String
        self.delivery = data["delivery"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
        self.favorite = data["favorite"] as? String
        self.mashoAssured = data["mashoAssured"] as? String
        self.offerpercent = data["offerpercent"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.offers = data["offers"] as? String
        self.type = data["type"] as? String
    }
}
class OrderSummaryCouponModel{
    var btndata : viewAddress_btndataModel!
    var fieldname : String!
    var status : Int!
    var text : String!
    init(fromData data: [String:Any]){
       if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = viewAddress_btndataModel(fromData: btndatas)
        }
        self.fieldname = data["fieldname"] as? String
        self.text = data["text"] as? String
        self.status = data["status"] as? Int
    }
}
class OrderSummaryPriceModel{
    var amountpayable:OrderSummaryAmountPayableModel!
    var entryloop = [OrderSummaryPriceEntryLoopModel]()
    var text : String!
    init(fromData data: [String:Any]){
        self.text = data["text"] as? String
        if let entryloopData = data["entryloop"] as? [[String:Any]]{
            for item in entryloopData{
                self.entryloop.append(OrderSummaryPriceEntryLoopModel(fromData: item))
            }
        }
        if let amtData = data["amountpayable"] as? [String:Any]{
            self.amountpayable = OrderSummaryAmountPayableModel(fromData: amtData)
        }
    }
}
class OrderSummaryAmountPayableModel{
    var amount : String!
    var bottombanner : OrderSummaryAmountPayableBottomBannerModel!
    var bottomtxt:OrderSummaryAmountPayableBottomtxtModel!
    var savingamounttxt: OrderSummaryAmountPayableSavingAmountTxtModel!
    var text : String!
    init(fromData data: [String:Any]){
        self.amount = data["amount"] as? String
        self.text = data["text"] as? String
        if let bottombannerdatas = data["bottombanner"] as? [String:Any]{
            self.bottombanner = OrderSummaryAmountPayableBottomBannerModel(fromData: bottombannerdatas)
        }
        if let bottomtxtdatas = data["bottomtxt"] as? [String:Any]{
            self.bottomtxt = OrderSummaryAmountPayableBottomtxtModel(fromData: bottomtxtdatas)
        }
        if let savingamounttxtdatas = data["savingamounttxt"] as? [String:Any]{
            self.savingamounttxt = OrderSummaryAmountPayableSavingAmountTxtModel(fromData: savingamounttxtdatas)
        }
    }
}
class OrderSummaryAmountPayableSavingAmountTxtModel{
    var color : String!
    var status : Int!
    var text : String!
    init(fromData data: [String:Any]){
        self.color = data["color"] as? String
        self.text = data["text"] as? String
        self.status = data["status"] as? Int
    }
}
class OrderSummaryAmountPayableBottomtxtModel{
    var color : String!
    var icon : String!
    var status : Int!
    var text : String!
    init(fromData data: [String:Any]){
        self.color = data["color"] as? String
        self.text = data["text"] as? String
        self.icon = data["icon"] as? String
        self.status = data["status"] as? Int
    }
}
class OrderSummaryAmountPayableBottomBannerModel{
    var image = [String]()
    var status : Int!
    init(fromData data: [String:Any]){
        self.status = data["status"] as? Int
        if let imgData = data["image"] as? [String]{
            for item in imgData{
                self.image.append(item)
            }
        }
    }
    
}
class OrderSummaryPriceEntryLoopModel{
    var price : String!
    var text : String!
    var subtext : String!
    var style : OrderSummaryPriceEntryLoopStyleModel!
    init(fromData data: [String:Any]){
        self.price = data["price"] as? String
        self.text = data["text"] as? String
        self.subtext = data["subtext"] as? String
        if let savingamounttxtdatas = data["style"] as? [String:Any]{
            self.style = OrderSummaryPriceEntryLoopStyleModel(fromData: savingamounttxtdatas)
        }
    }
}
class OrderSummaryPriceEntryLoopStyleModel{
    var background : String!
    var color : String!
    init(fromData data: [String:Any]){
        self.background = data["background"] as? String
        self.color = data["color"] as? String
    }
}

class OrderSummarySecondivModel{
    var changeaddressbtn : viewAddress_btndataModel!
    var mainhead: String!
    var subhead: String!
    init(fromData data: [String:Any]){
        if let btndatas = data["changeaddressbtn"] as? [String:Any]{
            self.changeaddressbtn = viewAddress_btndataModel(fromData: btndatas)
        }
        self.mainhead = data["mainhead"] as? String
        self.subhead = data["subhead"] as? String
    }
}
