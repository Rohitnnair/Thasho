//
//  ListingCells.swift
//  masho
//
//  Created by Appzoc on 23/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import DropDown
class MHCurrentLocationTVC: UITableViewCell{
    
}
class MHLocationTVC1: UITableViewCell{
    @IBOutlet weak var TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var TVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeholderLb: UILabel!
    @IBOutlet weak var ContentTF: UITextField!
    @IBOutlet weak var ContentTV: UITextView!
    // @IBOutlet weak var textfield: UITextField!
}
class MHLocationTVC2: UITableViewCell{
    var dropDown = DropDown()
    var CityArray = ["Thrissur","Ernakulam","Chennai"]
    var StateArray = ["Kerala","Tamilnadu"]
    var Cityselected : Bool?
    @IBOutlet weak var placeHolder1: UILabel!
    @IBOutlet weak var placeHolder2: UILabel!
    @IBOutlet weak var ContentTF1: UITextField!
    @IBOutlet weak var ContentTF2: UITextField!
    @IBOutlet weak var anchorView1: UIView!
    @IBOutlet weak var anchorView2: UIView!
    func setDropdown(View: UIView,dropDownArray : [String]){
        dropDown.anchorView = View
        dropDown.dataSource = dropDownArray
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if self.Cityselected!{
                self.ContentTF1.text = item
            }else{
                self.ContentTF2.text = item
            }
        }
        
    }
    @IBAction func StateDropDownBTNtapped(_ sender: UIButton) {
        Cityselected = false
        setDropdown(View: anchorView2, dropDownArray: StateArray)
        dropDown.show()
    }
    @IBAction func CityDropDownBTntapped(_ sender: Any) {
        Cityselected = true
        setDropdown(View: anchorView1, dropDownArray: CityArray)
        dropDown.show()
        
    }
}
class MHSaveLocationTVC: UITableViewCell,UITextViewDelegate{
    @IBOutlet weak var  SaveAddressbtn: UIButton!
    @IBOutlet weak var  Setasdefaultaddressbtn: UIButton!
    @IBOutlet weak var  Anydeliveryinstructionsbtn: UIButton!
    @IBOutlet weak var SaveAddressbtnimage: UIImageView!
    @IBOutlet weak var Setasdefaultaddressbtnimage: UIImageView!
    @IBOutlet weak var Anydeliverybtnimage: UIImageView!
    @IBOutlet weak var AnydeliveryTF1: UITextView!
    @IBOutlet weak var AnydeliveryTF2: UITextField!
    @IBOutlet weak var Baseview: BaseView!
    var button: UIButton!
    
    var buttonAction: ((Any) -> Void)?
    var buttonAction1: ((Any) -> Void)?
    var count = Int()
    @IBAction func SaveAddressbtnPressed(_ sender: Any) {
        self.buttonAction?(sender)
        
    }
    @IBAction func Setasdefaultaddressbtn(_ sender: Any) {
        self.buttonAction1?(sender)
    }
    override func awakeFromNib() {
        AnydeliveryTF1.delegate = self
       }
    func textViewDidChange(_ textView: UITextView) {
        
        if AnydeliveryTF1.text.length == 0 {
            Baseview.isHidden = false
            AnydeliveryTF1.text = ""
            AnydeliveryTF1.resignFirstResponder()
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        if AnydeliveryTF1.text.length == 0 {
            Baseview.isHidden = false
            AnydeliveryTF1.text = ""
            AnydeliveryTF1.resignFirstResponder()
        }
        return true
    }
}


