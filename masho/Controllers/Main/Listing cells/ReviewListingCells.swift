//
//  ReviewListingCells.swift
//  masho
//
//  Created by Appzoc on 24/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import FSPagerView
import DropDown
class MHReviewAddressTVC:UITableViewCell{
    @IBOutlet weak var adrsIcon: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var addressdetails: UILabel!
}
class MHchangeAddressTVC:UITableViewCell{
    @IBOutlet weak var FirstLb: UILabel!
    @IBOutlet weak var BGview: BaseView!
    @IBOutlet weak var BTNtxt: UILabel!
    
}
class MHReviewProductTVC:UITableViewCell{
    var dropDown = DropDown()
    var QtyArray = ["1","2","3","4","5"]
    @IBOutlet weak var percentageLb: UILabel!
    @IBOutlet weak var DeliveryLb: UILabel!
    @IBOutlet weak var OfferLb: UILabel!
    @IBOutlet weak var PriceLb: UILabel!
    @IBOutlet weak var PriceLb2: UILabel!
    @IBOutlet weak var TotalPriceLb: UILabel!
    @IBOutlet weak var ProductTitle: UILabel!
    @IBOutlet weak var priceStack:UIStackView!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var StrikePriceLb: UILabel!
    @IBOutlet weak var QtyLb: UILabel!
    @IBOutlet weak var anchorView: UIView!
    override func awakeFromNib() {
        dropDown.anchorView = anchorView
        dropDown.dataSource = QtyArray
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.QtyLb.text = "Qty: \(item)"
        }
    }
    @IBAction func QtyDropDownBTNtapped(_ sender: UIButton) {
        dropDown.show()
    }
}
class MHReviewCouponTVC:UITableViewCell{
    
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var CouponTF: UITextField!
    @IBOutlet weak var BGview: BaseView!
    @IBOutlet weak var ApplyTxt: UILabel!
}
class MHReviewPriceDetailsHeaderTVC:UITableViewCell{
    
    @IBOutlet weak var TitleLb: UILabel!
}
class MHReviewPriceDetailsHeader2TVC:UITableViewCell{
    @IBOutlet weak var descLb: UILabel!

}
class MHReviewPriceDetailsTVC:UITableViewCell{
    @IBOutlet weak var SeperatorLine: UIView!
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var SubTxtLb: UILabel!
    @IBOutlet weak var PriceLb: UILabel!
    @IBOutlet weak var TopConstraint: NSLayoutConstraint!
}
class MHReviewBannerTVC:UITableViewCell,FSPagerViewDelegate,FSPagerViewDataSource{
    var banners = [String]()
    @IBOutlet weak var BannerView: FSPagerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        BannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "SliderCell")
        BannerView.itemSize = CGSize(width: BannerView.frame.width, height: BannerView.frame.height)
        BannerView.transformer = FSPagerViewTransformer(type:.zoomOut)
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cel = pagerView.dequeueReusableCell(withReuseIdentifier: "SliderCell", at: index)
//        cel.imageView?.image = UIImage(named: "bannerr")
        cel.imageView?.kf.setImage(with: URL(string: banners[index]))
        return cel
    }
}
class MHReviewBottomTVC:UITableViewCell{
    @IBOutlet weak var descLb: UILabel!
    @IBOutlet weak var shieldIcon: UIImageView!
    
}
