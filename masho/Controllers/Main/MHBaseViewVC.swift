//
//  MHBaseViewVC.swift
//  masho
//
//  Created by Appzoc on 22/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class MHBaseViewVC: UIViewController,displayNewVCdelegate {
    var delegate : displayNewVCdelegate! = nil
    var currectVC : UIViewController!
    var UserID : String = ""
    var guest_id : Int = 0
    var TableID : String = ""
    var AddressData : viewAddressModel! = nil
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var FirstLb: UILabel!
    @IBOutlet weak var TickView1: BaseView!
    @IBOutlet weak var BGView1: BaseView!
    @IBOutlet weak var SecondLb: UILabel!
    @IBOutlet weak var TickView2: BaseView!
    @IBOutlet weak var BGView2: BaseView!
    @IBOutlet weak var ThirdLb: UILabel!
    @IBOutlet weak var TickView3: BaseView!
    @IBOutlet weak var BGView3: BaseView!
    lazy var AddAddressVC: UIViewController = {
        let vc = StoryBoard.new.instantiateViewController(withIdentifier: "SelectaddressViewController") as! SelectaddressViewController
        vc.delegate = self
        return vc
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        currectVC = StoryBoard.main.instantiateViewController(withIdentifier: "MHReviewVC") as! MHReviewVC
        getAddress()
        delegate = self
        
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        if(currectVC.isKind(of: MHAdd_AddressVC.self)){
            let vc = StoryBoard.new.instantiateViewController(withIdentifier: "SelectaddressViewController") as! SelectaddressViewController
            vc.delegate = self
            delegate.displayNewVC(VC: vc)
        }else if(currectVC.isKind(of: SelectaddressViewController.self)) || (currectVC.isKind(of: PaymentmodeViewController.self)){
            let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHReviewVC") as! MHReviewVC
            vc.delegate = self
            delegate.displayNewVC(VC: vc)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpView(vc:UIViewController){
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.addChildViewController(viewcontroller: vc)
        }, completion: nil)
    }
    func addChildViewController(viewcontroller:UIViewController){
        addChild(viewcontroller)
        containerView.addSubview(viewcontroller.view)
        viewcontroller.view.translatesAutoresizingMaskIntoConstraints = false
        viewcontroller.view.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        viewcontroller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        viewcontroller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        viewcontroller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        viewcontroller.didMove(toParent: self)
    }
    func displayNewVC(VC: UIViewController){
        currectVC = VC.self
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.addChildViewController(viewcontroller: VC)
        }, completion: nil)
        if(VC.isKind(of: SelectaddressViewController.self)){
            FirstLb.textColor = .white
            TickView1.isHidden = true
            BGView1.backgroundColor = UIColor.MHGold
            SecondLb.textColor = .black
            TickView2.isHidden = true
            BGView2.backgroundColor = .white
            ThirdLb.textColor = .black
            TickView3.isHidden = true
            BGView3.backgroundColor = .white
            TitleLb.text = "Select Delivery Address"
        }
        if(VC.isKind(of: MHAdd_AddressVC.self)){
            FirstLb.textColor = .white
            TickView1.isHidden = true
            BGView1.backgroundColor = UIColor.MHGold
            SecondLb.textColor = .black
            TickView2.isHidden = true
            BGView2.backgroundColor = .white
            ThirdLb.textColor = .black
            TickView3.isHidden = true
            BGView3.backgroundColor = .white
            TitleLb.text = "Delivery Address"
        }
        if(VC.isKind(of: MHReviewVC.self)){
            FirstLb.textColor = .white
            TickView1.isHidden = false
            BGView1.backgroundColor = UIColor.MHGold
            SecondLb.textColor = .white
            TickView2.isHidden = true
            BGView2.backgroundColor = UIColor.MHGold
            ThirdLb.textColor = .black
            TickView3.isHidden = true
            BGView3.backgroundColor = .white
            TitleLb.text = "Order Summary"
        }
        if(VC.isKind(of: PaymentmodeViewController.self)){
            FirstLb.textColor = .white
            TickView1.isHidden = false
            BGView1.backgroundColor = UIColor.MHGold
            SecondLb.textColor = .white
            TickView2.isHidden = false
            BGView2.backgroundColor = UIColor.MHGold
            ThirdLb.textColor = .white
            TickView3.isHidden = true
            BGView3.backgroundColor = UIColor.MHGold
            TitleLb.text = "Review"
        }
    }
}
extension MHBaseViewVC{
    func getAddress(){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""//"678"//
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = [  "action":"viewaddress",
                            "userId":"\(UserID)",
            "tableid": TableID] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    self.AddressData = viewAddressModel(fromData: data)
                }
                
                
                
                if self.AddressData.address == nil{
                    self.FirstLb.textColor = .white
                    self.TickView1.isHidden = true
                    self.BGView1.backgroundColor = UIColor.MHGold
                    self.SecondLb.textColor = .black
                    self.TickView2.isHidden = true
                    self.BGView2.backgroundColor = .white
                    self.ThirdLb.textColor = .black
                    self.TickView3.isHidden = true
                    self.BGView3.backgroundColor = .white
                    let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHAdd_AddressVC") as! MHAdd_AddressVC
                    vc.delegate = self
                    vc.ForEdit = false
                    vc.forAddNewAddress = true
                    self.setUpView(vc: vc)
                    self.TitleLb.text = "Delivery Address"
                }else{
                    
                    let filteredArray = self.AddressData.address.address.filter{$0.defultaddress == "0"}
                    if self.AddressData.address.address.count == filteredArray.count{
                        self.FirstLb.textColor = .white
                        self.TickView1.isHidden = true
                        self.BGView1.backgroundColor = UIColor.MHGold
                        self.SecondLb.textColor = .black
                        self.TickView2.isHidden = true
                        self.BGView2.backgroundColor = .white
                        self.ThirdLb.textColor = .black
                        self.TickView3.isHidden = true
                        self.BGView3.backgroundColor = .white
                        self.TitleLb.text = "Select Delivery Address"
                        let vc  = StoryBoard.new.instantiateViewController(withIdentifier: "SelectaddressViewController") as! SelectaddressViewController
                        vc.delegate = self
                        self.setUpView(vc: vc)
                    }else{
                        self.FirstLb.textColor = .white
                        self.TickView1.isHidden = false
                        self.BGView1.backgroundColor = UIColor.MHGold
                        self.SecondLb.textColor = .white
                        self.TickView2.isHidden = true
                        self.BGView2.backgroundColor = UIColor.MHGold
                        self.ThirdLb.textColor = .black
                        self.TickView3.isHidden = true
                        self.BGView3.backgroundColor = .white
                        self.TitleLb.text = "Order Summary"
                        let vc  = StoryBoard.main.instantiateViewController(withIdentifier: "MHReviewVC") as! MHReviewVC
                        vc.delegate = self
                        self.setUpView(vc: vc)
                    }
                    
                    
                }
                
            case .failure :
                print("failure")
            case .unknown:
                print("unknown")
            }
        }
    }
}
