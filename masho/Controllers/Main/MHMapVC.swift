//
//  MHMapVC.swift
//  masho
//
//  Created by Appzoc on 24/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import GoogleMaps
import Contacts
protocol selectedLocationDelegate{
    func locationSelect(LocationData : [String:Any])
}
class MHMapVC: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {
    @IBOutlet weak var mpView: GMSMapView!
    @IBOutlet weak var FirstLb:UILabel!
    @IBOutlet weak var SecondLb:UILabel!
    @IBOutlet weak var ConfirmBTNview:UIView!
    var delegate : selectedLocationDelegate!
    var SelectedData = [String:Any]()
    var locationManager = CLLocationManager()
    var location = CLLocationCoordinate2D()
    var centerMapCoordinate:CLLocationCoordinate2D!
    override func viewDidLoad() {
        super.viewDidLoad()
        ConfirmBTNview.isHidden = true
        mpView.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 1
        if location.latitude == 0{
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        else {
            let camera      = GMSCameraPosition.camera(withTarget: location, zoom: 15.5)
            let mapView     = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.width * (self.view.frame.height - 50)/375), camera: camera)
            mapView.mapType = .normal
            mapView.settings.scrollGestures = true
            mapView.delegate = self
            self.mpView.addSubview(mapView)
//            let marker      = GMSMarker()
//            marker.position = CLLocationCoordinate2D(latitude:  (location.latitude), longitude: (location.longitude))
//            marker.map      = mapView
            setAddress()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        location    = (locationManager.location?.coordinate)!
        let camera      = GMSCameraPosition.camera(withTarget: location, zoom: 15.5)
        let mapView     = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.width * (self.view.frame.height - 50)/375), camera: camera)
        mapView.mapType = .normal
        mapView.settings.scrollGestures = true
        self.mpView.addSubview(mapView)
        mapView.delegate = self
//        let marker      = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude:  (location.latitude), longitude: (location.longitude))
//        marker.map      = mapView
        setAddress()
        locationManager.stopUpdatingLocation()

        // print(marker.title!)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.distanceFilter = locationManager.distanceFilter*5//defaultDistanceFilter
        setAddress()

    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        location = position.target
        setAddress()
    }
    func setAddress(){

        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(location.latitude)")!
        let lon: Double = Double("\(location.longitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(center.latitude, center.longitude), completionHandler: { response, error in
            print("reverse geocoding results:")
            if let results = response?.results() {
                for addressObj in results {
                    guard let addressObj = addressObj as? GMSAddress else {
                        continue
                    }
                    print("state=\(addressObj.administrativeArea)")
                    if addressObj.administrativeArea != nil{
                        self.SelectedData["state"] = addressObj.administrativeArea!
                    }
                }
            }
        })
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks
                
                if pm?.count ?? 0 > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
//                    print(pm.subThoroughfare)
//
//                    self.SelectedData["city"]
//                    self.SelectedData["country"]
//                    self.SelectedData["landmark"]
//                    self.SelectedData["pincode"]
//                    self.SelectedData["address"]
                    
                    
                    var addressString : String = ""
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.subLocality != nil {
                        self.SelectedData["city"] = pm.subLocality!
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.locality != nil {
                        self.SelectedData["landmark"] = pm.locality!
                        addressString = addressString + pm.locality! + ", "
                    }
                    if self.SelectedData["state"] != nil{
                        addressString = addressString + (self.SelectedData["state"] as! String) + ", "
                    }
                    if pm.country != nil {
                        self.SelectedData["country"] = pm.country!
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.SelectedData["pincode"] = pm.postalCode!
                        addressString = addressString + pm.postalCode!
                    }
                    self.FirstLb.text = pm.subLocality
                    self.SecondLb.text = pm.locality
                    print(addressString)
                    self.SelectedData["address"] = addressString
                    self.ConfirmBTNview.isHidden = false
                }
                else{
                    self.locationManager.distanceFilter = self.locationManager.distanceFilter*5//defaultDistanceFilter
                    self.setAddress()
                }
        })
    }
    
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func ConfirmLocationBTNtapped(_ sender: Any) {
        print(SelectedData)
        delegate.locationSelect(LocationData: SelectedData)
        self.navigationController?.popViewController(animated: false)
    }
}
extension CLLocation {
    func fetchCityAndCountry(completion: @escaping (_ city: String?,_ state: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality,$0?.first?.state, $0?.first?.country, $1) }
    }
}
extension CLPlacemark {
    var streetName: String? { thoroughfare }
    var streetNumber: String? { subThoroughfare }
    var city: String? { locality }
    var neighborhood: String? { subLocality }
    var state: String? { administrativeArea }
    var county: String? { subAdministrativeArea }
    var zipCode: String? { postalCode }
    @available(iOS 11.0, *)
    var postalAddressFormatted: String? {
        guard let postalAddress = postalAddress else { return nil }
        return CNPostalAddressFormatter().string(from: postalAddress)
    }
}
extension CLLocation {
    func placemark(completion: @escaping (_ placemark: CLPlacemark?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first, $1) }
    }
}
