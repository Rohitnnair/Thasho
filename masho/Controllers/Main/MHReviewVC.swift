//
//  MHReviewVC.swift
//  masho
//
//  Created by Appzoc on 23/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import Kingfisher
class MHReviewVC: UIViewController,displayNewVCdelegate {
    var UserID : String = ""
    var guest_id : Int = 0
    var TableID : String = ""
    var order_id : String = ""
    var orderSummaryData : OrderSummaryModel!
    var cartData : MHCartmodel!
    @IBOutlet weak var ReviewTV: UITableView!
    @IBOutlet weak var BottomBTNview: BaseView!
    @IBOutlet weak var BottomBTNlb: UILabel!
    @IBOutlet weak var EmptyView:UIView!
    func displayNewVC(VC: UIViewController) {
        delegate.displayNewVC(VC: VC)
    }
    
    var delegate : displayNewVCdelegate! = nil
    var orderSummaryArray = ["Price (3 item)","Discount","Delivery Charges","Amount Payable"]
    var orderSummaryPriceArray = ["₹ 2369","₹ 479","₹ 30","₹ 1930"]
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrderSummary()
        // Do any additional setup after loading the view.
    }
    @IBAction func ChangeAddressBTNtapped(_ sender: Any) {
        let vc = StoryBoard.new.instantiateViewController(withIdentifier: "SelectaddressViewController") as! SelectaddressViewController
        vc.delegate = self
        delegate.displayNewVC(VC: vc)
    }
    @IBAction func ApplyCoupnBTNtapped(_ sender: Any) {
        let cell = ReviewTV.cellForRow(at: IndexPath(row: 0, section: 5	)) as! MHReviewCouponTVC
        if cell.CouponTF.text != ""{
            ApplyCoupon(code:cell.CouponTF.text!)
        }else{
            Banner.main.showBanner(title: "", subtitle: "Enter Coupon Code", style: .danger)
        }
        
        
    }
    @IBAction func ContinueBTNtapped(_ sender: Any) {
        PlaceOrder()

//        let vc = StoryBoard.new.instantiateViewController(withIdentifier: "PaymentmodeViewController") as! PaymentmodeViewController
//        self.delegate.displayNewVC(VC: vc)
    }
}
extension MHReviewVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 11
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            if orderSummaryData != nil {
                return orderSummaryData.address.count
            }
            return 0
        }else if section == 2{
            return 1
        }else if section == 3{
            return 1
        }else if section == 4{
            if orderSummaryData != nil {
                return orderSummaryData.cartitems.count
            }
            return 0
        }else if section == 5{
            return 1
        }else if section == 6{
            return 1
        }else if section == 7{
            if orderSummaryData != nil {
                return self.orderSummaryData.pricedetails.entryloop.count + 1
            }
            return 0
        }else if section == 8{
            return 1
        }else if section == 9{
            return 1
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsHeaderTVC") as! MHReviewPriceDetailsHeaderTVC
            if orderSummaryData != nil{
                cell.TitleLb.text = self.orderSummaryData.btndata.titledata[0]
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewAddressTVC") as! MHReviewAddressTVC
            if orderSummaryData != nil{
                
//                cell.address.text = orderSummaryData.address[0].customer_name + ", " + orderSummaryData.address[0].city
                cell.adrsIcon.kf.setImage(with: URL(string: orderSummaryData.address[indexPath.row].icon))
                cell.addressdetails.text = orderSummaryData.address[indexPath.row].text
            }
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHchangeAddressTVC") as! MHchangeAddressTVC
            if self.orderSummaryData != nil{
                cell.FirstLb.text = self.orderSummaryData.secondiv.subhead
                cell.BGview.backgroundColor = UIColor(hex: "\(self.orderSummaryData.secondiv.changeaddressbtn.background ?? "#111111")ff")
                cell.BTNtxt.text = self.orderSummaryData.secondiv.mainhead
                cell.BTNtxt.textColor = UIColor(hex: "\(self.orderSummaryData.secondiv.changeaddressbtn.color  ?? "#111111")ff")
            }
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsHeaderTVC") as! MHReviewPriceDetailsHeaderTVC
            if orderSummaryData != nil{
                cell.TitleLb.text = self.orderSummaryData.btndata.titledata[1]
            }
            return cell
        }else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewProductTVC") as! MHReviewProductTVC
            if self.orderSummaryData != nil{
                cell.ProductImage.kf.setImage(with: URL(string: self.orderSummaryData.cartitems[indexPath.row].imageUrl))
                cell.PriceLb.text = self.orderSummaryData.cartitems[indexPath.row].price
//                cell.PriceLb2.text = "\(self.orderSummaryData.cartitems[indexPath.row].qty ?? "") * \(self.orderSummaryData.cartitems[indexPath.row].price ?? "") = "
//                if self.orderSummaryData.cartitems[indexPath.row].qty == "1"{
//                    cell.priceStack.isHidden = true
//                }else{
//                    cell.priceStack.isHidden = false
//                }
                cell.QtyLb.text = "Qty : \(self.orderSummaryData.cartitems[indexPath.row].qty ?? "1")"
//                cell.TotalPriceLb.text = self.orderSummaryData.cartitems[indexPath.row].totalitemprice
                cell.ProductTitle.text = self.orderSummaryData.cartitems[indexPath.row].productName
                cell.StrikePriceLb.attributedText = "\(self.orderSummaryData.cartitems[indexPath.row].strikeprice ?? "")".updateStrikeThroughFont(UIColor.darkGray)
                cell.OfferLb.text = self.orderSummaryData.cartitems[indexPath.row].offers
                cell.DeliveryLb.text = self.orderSummaryData.cartitems[indexPath.row].delivery
                if self.orderSummaryData.cartitems[indexPath.row].offerpercent == "" || self.orderSummaryData.cartitems[indexPath.row].offerpercent == "0" || self.orderSummaryData.cartitems[indexPath.row].offerpercent == nil{
                    cell.percentageLb.text = ""
                }else{
                    cell.percentageLb.text = "\(self.orderSummaryData.cartitems[indexPath.row].offerpercent ?? "")"
                }
            }
            return cell
        }else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewCouponTVC") as! MHReviewCouponTVC
            if self.orderSummaryData != nil{
                cell.TitleLb.text = self.orderSummaryData.coupon.text
                cell.BGview.backgroundColor = UIColor(hex: "\(self.orderSummaryData.coupon.btndata.background ?? "#111111")ff")
                cell.ApplyTxt.text = self.orderSummaryData.coupon.btndata.value
                    cell.ApplyTxt.textColor = UIColor(hex: "\(self.orderSummaryData.coupon.btndata.color ?? "#111111")ff")
            }
            return cell
        }else if indexPath.section == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsHeaderTVC") as! MHReviewPriceDetailsHeaderTVC
            if orderSummaryData != nil{
                cell.TitleLb.text = self.orderSummaryData.btndata.titledata[2]
            }
            return cell
        }else if indexPath.section == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsTVC") as! MHReviewPriceDetailsTVC
            DispatchQueue.main.async {
                if indexPath.row >= self.orderSummaryData.pricedetails.entryloop.count{
                    cell.TitleLb.text = self.orderSummaryData.pricedetails.amountpayable.text
                    cell.SubTxtLb.text = ""
                    cell.PriceLb.text = "\(self.orderSummaryData.pricedetails.amountpayable.amount ?? "")"
                    cell.TitleLb.font = UIFont(name: "Roboto-Medium", size: 14)
                    cell.PriceLb.font = UIFont(name: "Roboto-Medium", size: 14)
                    cell.SeperatorLine.isHidden = false
                    cell.backgroundColor = UIColor.white
                    cell.TitleLb.textColor = UIColor.black
                    cell.SubTxtLb.textColor = UIColor.lightGray
                    cell.PriceLb.textColor = UIColor.black
//                    cell.TopConstraint.constant = 30
                }else{
                    cell.TitleLb.text = self.orderSummaryData.pricedetails.entryloop[indexPath.row].text
                    if self.orderSummaryData.pricedetails.entryloop[indexPath.row].subtext == nil{
//                        cell.SubTxtLb.isHidden = true
                        cell.SubTxtLb.text = ""
                    }else{
//                        cell.SubTxtLb.isHidden = false
                        cell.SubTxtLb.text = self.orderSummaryData.pricedetails.entryloop[indexPath.row].subtext
                    }
                    if self.orderSummaryData.pricedetails.entryloop[indexPath.row].style != nil{
                        if self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.background.count == 7{
                            cell.backgroundColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.background ?? "#111111")ff")
                        }else{
                            cell.backgroundColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.background ?? "#111111")")
                        }
                        if self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.background.count == 7{
                            cell.SubTxtLb.textColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.color ?? "#111111")ff")
                        }else{
                            cell.SubTxtLb.textColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.color ?? "#111111")")
                        }
                        cell.TitleLb.textColor = UIColor.black
                        cell.SubTxtLb.textColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.entryloop[indexPath.row].style.color  ?? "#111111")ff")
                        cell.PriceLb.textColor = UIColor.black
                    }else{
                        cell.backgroundColor = UIColor.white
                        cell.TitleLb.textColor = UIColor.black
                        cell.SubTxtLb.textColor = UIColor.lightGray
                        cell.PriceLb.textColor = UIColor.black
                    }
                    cell.PriceLb.text = self.orderSummaryData.pricedetails.entryloop[indexPath.row].price
                    cell.TitleLb.font = UIFont(name: "Roboto-Regular", size: 13)
                    cell.PriceLb.font = UIFont(name: "Roboto-Regular", size: 13)
                    cell.SeperatorLine.isHidden = true
//                    cell.TopConstraint.constant = 0
                }
            }
            return cell
        }else if indexPath.section == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsHeader2TVC") as! MHReviewPriceDetailsHeader2TVC
            if self.orderSummaryData != nil{
                cell.descLb.text = self.orderSummaryData.pricedetails.amountpayable.savingamounttxt.text
                cell.descLb.textColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.amountpayable.savingamounttxt.color ?? "#111111")ff")
            }
            return cell
        }else if indexPath.section == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewBannerTVC") as! MHReviewBannerTVC
            if self.orderSummaryData != nil{
                cell.banners = self.orderSummaryData.pricedetails.amountpayable.bottombanner.image
                cell.BannerView.reloadData()
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewBottomTVC") as! MHReviewBottomTVC
            if self.orderSummaryData != nil{
            cell.descLb.text = self.orderSummaryData.pricedetails.amountpayable.bottomtxt.text
            cell.descLb.textColor = UIColor(hex: "\(self.orderSummaryData.pricedetails.amountpayable.bottomtxt.color ?? "#111111")ff")
            cell.shieldIcon.kf.setImage(with: URL(string: self.orderSummaryData.pricedetails.amountpayable.bottomtxt.icon))
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 5{
            if self.orderSummaryData != nil{
                if self.orderSummaryData.coupon.status == 0{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }
            return 0
        }else if indexPath.section == 8{
            if self.orderSummaryData != nil{
                if self.orderSummaryData.pricedetails.amountpayable.savingamounttxt.status == 0{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }
            return 0
        }else if indexPath.section == 9{
            if self.orderSummaryData != nil{
                if self.orderSummaryData.pricedetails.amountpayable.bottombanner.status == 0{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }
            return 0
        }else if indexPath.section == 10{
            if self.orderSummaryData != nil{
                if self.orderSummaryData.pricedetails.amountpayable.bottomtxt.status == 0{
                    return 0
                }else{
                    return 70
                }
            }
            return 0
        }else{
            return UITableView.automaticDimension
        }
        return 0
    }
}
extension MHReviewVC{
    func getOrderSummary(){
        EmptyView.isHidden = false
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        order_id = UserDefaults.standard.value(forKey: "orderid") as? String ?? ""
        var parameters = ["action":"ordersummary",
                          "userId":"\(UserID)",
                          "tableid": TableID] as [String : Any]
        if order_id != ""{
            parameters["orderid"] = order_id
        }
//        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    self.orderSummaryData = OrderSummaryModel(fromData: data)
                }
                
                self.BottomBTNview.backgroundColor = UIColor(hex: "\(self.orderSummaryData.btndata.background  ?? "#111111")ff")
                self.BottomBTNlb.text = self.orderSummaryData.btndata.value
                self.BottomBTNlb.textColor = UIColor(hex: "\(self.orderSummaryData.btndata.color ?? "#111111")ff")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                self.ReviewTV.reloadData()
                self.EmptyView.isHidden = true
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func PlaceOrder(){
            UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
            guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
            TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
            order_id = UserDefaults.standard.value(forKey: "orderid") as? String ?? ""
        //https://www.masho.com/api?action=processorder&userId=678
        var parameters = ["action":"processorder",
                              "userId":"\(UserID)",
                              "tableid": TableID] as [String : Any]
        if order_id != ""{
            parameters["orderid"] = order_id
        }
    //        self.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            KVSpinnerView.show()
            NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
                print(response)
                switch status {
                case .noNetwork:
                    print("network error")
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                    Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
                case .success :
                    print("success")
                    print(response["data"]!)
                    if let data = response["data"] as? [String: Any]{
                        UserDefaults.standard.set(data["orderid"] as! String, forKey: "orderid")
                    }
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()

                    let vc = StoryBoard.new.instantiateViewController(withIdentifier: "PaymentmodeViewController") as! PaymentmodeViewController
                    self.delegate.displayNewVC(VC: vc)
                case .failure :
                    print("failure")
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                    Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
                case .unknown:
                    print("unknown")
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                    Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                }
            }
        }
    func ApplyCoupon(code:String){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = ["action":"applycoupon",
                          "userId":"\(UserID)",
                            "couponid":code,
                          "tableid": TableID] as [String : Any]
//        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                Banner.main.showBanner(title: "", subtitle: response["message"]! as! String, style: .danger)
                let cell = self.ReviewTV.cellForRow(at: IndexPath(row: 0, section: 5 )) as! MHReviewCouponTVC
                cell.CouponTF.text = ""
                self.getOrderSummary()
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: response["message"]! as! String, style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
