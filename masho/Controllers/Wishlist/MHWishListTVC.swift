//
//  MHWishListTVC.swift
//  masho
//
//  Created by Appzoc Technologies on 19/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView

class MHWishListTVC: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLBL: UILabel!
    
    @IBOutlet weak var strikePriceLBL: UILabel!
    
    @IBOutlet weak var priceLBL: UILabel!
    
    @IBOutlet weak var deleteBTNRef: UIButton!
    
    var delegate:RedloadList?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBAction func deleteBTNTapped(_ sender: UIButton) {
        let deviceToken = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = [  "action":"Shortlist",
                            "deviceid": deviceToken,
                            "devicetype": 2,
                            "userid":"\(userID)",
                            "guestId": guest_id,
                            "type":"2",
                            "productid":"\(deleteBTNRef.tag)",
                            "firebaseRegID": "1234"] as [String : Any]
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.dashboardContentSetup, object: nil)
                }
                self.delegate?.reloadList()
                
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    
}
