//
//  MHWishListDataModel.swift
//  masho
//
//  Created by Appzoc Technologies on 19/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import Foundation

class MHWishListDataModel{
    var image : String!
    var imagetitle : String!
    var productid : String!
    var productlink : String!
    var productname : String!
    var specialprice : String!
    var strikeprice : String!
    var deliveryMode : String!
    var favorite : String!
    var mashoAssured : String!
    var offerPercentage : String!
    var paymentMode : String!
    var productTitle : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var type : String!
    init(fromData data: [String:Any]) {
        self.imagetitle = data["imagetitle"] as? String
        self.image = data["image"] as? String
        self.productname = data["productname"] as? String
        self.strikeprice = data["strikeprice"] as? String
        self.specialprice = data["specialprice"] as? String
        self.productlink = data["productlink"] as? String
        self.favorite = "\(data["favorite"] as? String ?? "0")"
        self.productid = "\(data["productid"] as? String ?? "0")"
        self.offerPercentage =  data["offerPercentage"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
        self.mashoAssured =  data["mashoAssured"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.rating =  data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.type =  data["type"] as? String
    }
}

