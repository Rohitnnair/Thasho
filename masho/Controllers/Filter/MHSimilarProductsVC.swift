//
//  MHSimilarProductsVC.swift
//  masho
//
//  Created by Jabir ML on 18/11/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit
import DropDown
import KVSpinnerView
class MHSimilarProductsVC: UIViewController {
    var Productlist : MHcategoryProductModel!
    
    let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
    let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var currentProductId : String!
    var dropDown = DropDown()
    var api_id : String!
    var apiname : String!
    @IBOutlet weak var listEmptyIndicatorLBL: UILabel!
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    @IBOutlet weak var SimilarPdtCV: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDropdown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
            if cartitemsCount == 0{
                cartItemsView.isHidden = true
            }else{
                cartItemsView.isHidden = false
                CartItemLb.text = "\(cartitemsCount)"
            }
        }
        similarlist()
    }
    @IBAction func CartBTNTaooed(_ sender: Any) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//        WebViewVC.categoryArray = self.categoryListArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
   
    
        
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
//        if frompush {
//            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
//            self.navigationController?.pushViewController(dashBoardVC, animated: true)
//
//        }
//        else {
          self.navigationController?.popViewController(animated: true)
//        }
        
        
        
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        dropDown.show()
    }
    
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                    CartVC.categoryArray = self.categoryArray
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}

extension MHSimilarProductsVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,rescentProductsFavDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Productlist != nil{
            return Productlist.products.count
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RescentProductsCVC", for: indexPath) as! RescentProductsCVC
        if Productlist != nil{
        cell.delegate = self
        cell.index = indexPath.row
        cell.ProductImage.kf.setImage(with: URL(string: "\(Productlist.products[indexPath.row].image[0].imageUrl!)"))
        cell.ProductName.text = Productlist.products[indexPath.row].productName
        cell.StrikePrice.attributedText = "\(Productlist.products[indexPath.row].strikeprice!)".updateStrikeThroughFont(UIColor.red)
        cell.StrikePrice.textColor = UIColor.red//(hex: "\(Productlist.products[indexPath.row].pricecolor!)ff")
        cell.PriceLb.text = "\(Productlist.products[indexPath.row].price!)"
        cell.PriceLb.textColor = UIColor.black//(hex: "\(Productlist.products[indexPath.row].sp_pricecolor!)ff")
        cell.PrecentageLb.text = Productlist.products[indexPath.row].offerpercent
        if Productlist.products[indexPath.row].favorite == 0{
            cell.FavImage.image = UIImage(named: "favUnselected")
        }else{
            cell.FavImage.image = UIImage(named: "favSelected")
        }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2.1
        let multiplier = width / 8
        let height = 10 * multiplier
        return CGSize(width: width, height: height + 65)
//        return CGSize(width: collectionView.frame.width / 2.1, height: 265)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//        MHProductDetailsVC.categoryArray = categoryArray
        MHProductDetailsVC.isFrom = .productDetailsRecentProducts
//        MHProductDetailsVC.BasicData7 = Productlist.products[indexPath.row]
//        MHProductDetailsVC.productID = Productlist.products[indexPath.row].productId
        productID = Productlist.products[indexPath.row].productId
         frompush = false
        self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
    }
    func rescentProductsFav(index: Int, cell: RescentProductsCVC){
        isUpdateRecent = true
        if currentProductId == Productlist.products[index].productId{
            isDashboardUpdted = true
            updatedProduct = Productlist.products[index].productId
        }
        var type:String = ""
        if Productlist.products[index].favorite == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = [  "action":"Shortlist",
                            "userid":"\(UserID)",
            "guestId": guest_id,
            "productid": Productlist.products[index].productId!,
            "type" : type] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                if self.Productlist.products[index].favorite == 0{
                    self.Productlist.products[index].favorite = 1
                }else{
                    self.Productlist.products[index].favorite = 0
                }
                self.SimilarPdtCV.reloadData()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }

    func similarlist(){
       
        let parameters = [  "action":apiname ?? "",
                            "api_id" :api_id ?? "",
                            "batchSize": 20,
                            "catsubcat":"sc",
                            "userId":"\(UserID)",
                            "guest_id": guest_id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String:Any]{
                    self.Productlist = MHcategoryProductModel(fromData: data)
                }
                self.SimilarPdtCV.reloadData()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"] ?? "")", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
