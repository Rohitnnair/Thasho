//
//  ViewController.swift
//  swiftlint:disable numbers_smell
//
//  Created by Yonat Sharon on 17.11.2016.
//  Copyright © 2016 Yonat Sharon. All rights reserved.
//

import MultiSlider
import UIKit

class MultiSliderViewController: UIViewController {
    @IBOutlet var multiSlider: MultiSlider!
    @IBOutlet var minValue:UILabel!
    @IBOutlet var maxValue:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//        multiSlider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
//        multiSlider.disabledThumbIndices = [3]
//
//        if #available(iOS 13.0, *) {
//            multiSlider.minimumImage = UIImage(systemName: "moon.fill")
//            multiSlider.maximumImage = UIImage(systemName: "sun.max.fill")
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            self.multiSlider.value = [0.4, 2.8]
//            self.multiSlider.valueLabelPosition = .top
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//            self.multiSlider.thumbCount = 5
//            self.multiSlider.valueLabelPosition = .right
//            self.multiSlider.isValueLabelRelative = true
//        }

//        let horizontalMultiSlider = MultiSlider()
        multiSlider.transform = CGAffineTransform(rotationAngle: .pi)
        multiSlider.orientation = .vertical
        multiSlider.minimumValue = 0
        multiSlider.maximumValue = 100
        multiSlider.outerTrackColor = .gray
        multiSlider.value = [40, 60]
        multiSlider.valueLabelPosition = .left
        multiSlider.tintColor = .purple
        multiSlider.trackWidth = 5
        multiSlider.showsThumbImageShadow = false
//        multiSlider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
//        view.addConstrainedSubview(horizontalMultiSlider, constrain: .leftMargin, .rightMargin, .bottomMargin)
//        view.layoutMargins = UIEdgeInsets(top: 32, left: 32, bottom: 32, right: 32)

//        multiSlider.keepsDistanceBetweenThumbs = false
        multiSlider.keepsDistanceBetweenThumbs = false
        multiSlider.valueLabelFormatter.positiveSuffix = " 𝞵s"
        multiSlider.valueLabelColor = .clear
        multiSlider.valueLabelFont = UIFont.italicSystemFont(ofSize: 18)

//        if #available(iOS 13.0, *) {
//            horizontalMultiSlider.minimumImage = UIImage(systemName: "scissors")
//            horizontalMultiSlider.maximumImage = UIImage(systemName: "paperplane.fill")
//        }
    }
    @IBAction func sliderChanged(_ sender : MultiSlider) {
//    @objc func sliderChanged(_ slider: MultiSlider) {
        print("thumb \(sender.draggedThumbIndex) moved")
        print("now thumbs are at \(sender.value)") // e.g., [1.0, 4.5, 5.0]
        minValue.text = "\(Int(sender.value[0]))"
        maxValue.text = "\(Int(sender.value[1]))"
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
