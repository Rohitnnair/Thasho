//
//  MHFilterVC.swift
//  masho
//
//  Created by Castler on 30/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import MultiSlider
import DropDown

protocol applyFilterDelegate{
    func getFilteredCategoryDetails(filter:[String:Any],count: Int)
}

class MHFilterVC: UIViewController,UITextFieldDelegate {
    
    var Device_token : String!
    var UserID : String!
    var TableID : String!
    var userName : String!
    var guest_id : Int! = nil
    var categoryId = ""
    var SubCategoryId = ""
    var nav_title = ""
    var vidString = ""
    var bidString = ""
    var SelectedMainIndex = 0
    var filterData : MHFiltermodel!
    var filterDataCopy : MHFiltermodel!
    var searchedArray = [MHFilterPropertiesItemsModel]()
    var mySlider = MultiSlider()
    var dropdown = DropDown()
    var DropDownDataSource = [String]()
    var sliderValueChanged = false
    var delegate : applyFilterDelegate!
    var SelectedIndexArray = [Int]()
    var selectedSortType = 0
    var FilterAppliedArray = [String:Any]()
    @IBOutlet var ResultCountLb:UILabel!
    @IBOutlet var minValue:UILabel!
    @IBOutlet var minValueTopConstraint:NSLayoutConstraint!
    @IBOutlet var maxValue:UILabel!
    @IBOutlet var maxValueBottomConstraint:NSLayoutConstraint!
    @IBOutlet weak var anchorView:UIView!
    @IBOutlet weak var SortTypeLb:UILabel!
    @IBOutlet weak var sliderView:UIView!
    @IBOutlet weak var slider: MultiSlider!
    @IBOutlet weak var SearchTF:UITextField!
    @IBOutlet weak var MainCategoryTV:UITableView!
    @IBOutlet weak var SubCategoryTV:UITableView!
    @IBOutlet weak var NavTitle :UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        NavTitle.text = nav_title
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        
        SearchTF.delegate = self
        
        self.sliderView.isHidden = true
        slider.transform = CGAffineTransform(rotationAngle: .pi)
        slider.orientation = .vertical
        slider.valueLabelColor = .clear
        
        getFilterContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    }
    func dropDownSetUp(dataSource:[String]){
        self.dropdown.anchorView = self.anchorView
        self.dropdown.dataSource = dataSource
        self.dropdown.dismissMode = .onTap
        self.dropdown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        self.dropdown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
            cell.backgroundColor = #colorLiteral(red: 0.8296601772, green: 0.6882546544, blue: 0.2148788273, alpha: 1)
        }
        self.dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            SortTypeLb.text = item
            for data in self.filterData.sort.options{
                if data.text == item{
                    selectedSortType = data.value
                }
            }
        }
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func DropDownBTNtapped(_ sender: Any) {
        self.dropdown.show()
    }
    @IBAction func sliderChanged(_ sender : MultiSlider) {
        print("thumb \(sender.draggedThumbIndex) moved")
        print("now thumbs are at \(sender.value)") // e.g., [1.0, 4.5, 5.0]
        sliderValueChanged = true
        let priceIndex = self.filterData.data.firstIndex(where: {$0.filterproperties[0].property_name == "Price"})
        let cell = MainCategoryTV.cellForRow(at: IndexPath(item: priceIndex!, section: 0)) as? FilterMainTVC
        cell?.CountView.isHidden = false
        cell?.CountLb.text = "1"
        for data in self.filterData.data{
            if data.filterproperties[0].property_name == "Price"{
                self.minValue.text = "\(data.filterpropertieitems[0].currency!) \(Int(sender.value[0]))"
                self.maxValue.text = "\(data.filterpropertieitems[0].currency!) \(Int(sender.value[1]))"
            }
        }
    }
    @IBAction func searchBTNTapped(_ sender: Any) {
        let search = StoryBoard.login.instantiateViewController(withIdentifier: "searchVC") as! searchVC
        self.navigationController?.pushViewController(search, animated: true)
        
    }
    @IBAction func EditChanged(_ sender: UITextField) {
        print(SearchTF.text!)
        let searchedArray1 = filterData.data[SelectedMainIndex].filterpropertieitems.filter { $0.property_name.localizedCaseInsensitiveContains(SearchTF.text!) }
        searchedArray = (SearchTF.text?.isEmpty)! ? filterData.data[SelectedMainIndex].filterpropertieitems : searchedArray1
        self.MainCategoryTV.reloadData()
        self.SubCategoryTV.reloadData()
    }
    @IBAction func ApplyBTNtapped(_ sender: UIButton){
        SelectedIndexArray = SelectedIndexArray.uniqued()
        print(SelectedIndexArray)
        var selectedFilterContent = [String:Any]()
        var selectedFilterItems1 :String!

        for SelectedIndex in SelectedIndexArray{
            let selectedFilterItems = filterData.data[SelectedIndex].filterpropertieitems.filter { (data) -> Bool in
                data.selectionStatus == 1
            }
            for item in selectedFilterItems{
                if selectedFilterItems1 == nil{
                    selectedFilterItems1 = item.property_id
                }else{
                    selectedFilterItems1 = selectedFilterItems1 + "," + item.property_id
                }
            }
            if filterData.data[SelectedIndex].filterproperties[0].name != nil{
                selectedFilterContent[filterData.data[SelectedIndex].filterproperties[0].name] = selectedFilterItems1
            }
        }
//        print(selectedFilterItems)
        let bid = selectedFilterContent["bid"] as? String
        let vid = selectedFilterContent["vid"] as? String
        if bid != nil && vid != nil{
            selectedFilterContent["vid"] = vid!.replacingOccurrences(of: "\(bid!),", with: "")
        }
        selectedFilterContent[self.filterData.sort.name] = self.selectedSortType
        for data in self.filterData.data{
            if data.filterproperties[0].property_name == "Price"{
                selectedFilterContent[data.filterproperties[0].from] = "\(Int(slider.value[0]))"
                selectedFilterContent[data.filterproperties[0].to] = "\(Int(slider.value[1]))"
            }
        }
        print(selectedFilterContent)
        if sliderValueChanged{
            delegate.getFilteredCategoryDetails(filter:selectedFilterContent,count: SelectedIndexArray.count + 1)
        }else{
            delegate.getFilteredCategoryDetails(filter:selectedFilterContent,count: SelectedIndexArray.count)

        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ClearBTNtapped(_ sender: UIButton){
        sliderValueChanged = false
        for data in self.filterData.data{
            for item in data.filterpropertieitems{
                item.selectionStatus = 0
            }
        }
        SelectedIndexArray.removeAll()
        for data in self.filterData.data{
            if data.filterproperties[0].property_name == "Price"{
                self.slider.value = [CGFloat(data.filterpropertieitems[0].pricefrom), CGFloat(data.filterpropertieitems[0].priceto)]
                self.minValue.text = "\(data.filterpropertieitems[0].currency!) \(data.filterpropertieitems[0].pricefrom!)"
                self.maxValue.text = "\(data.filterpropertieitems[0].currency!) \(data.filterpropertieitems[0].priceto!)"
            }
        }
        self.searchedArray = self.filterData.data[self.SelectedMainIndex].filterpropertieitems

        MainCategoryTV.reloadData()
        SubCategoryTV.reloadData()
    }
    
    @IBAction func SelectionBTNtapped(_ sender: UIButton){
        print(sender.tag)
        
        if searchedArray[sender.tag].selectionStatus == 0{
            searchedArray[sender.tag].selectionStatus = 1
        }else{
            searchedArray[sender.tag].selectionStatus = 0
        }
//        selectedIndexArray.append(sender.tag)
//        selectedIndexArray = selectedIndexArray.uniqued()
//        print(selectedIndexArray)
//        print(searchedArray[sender.tag].property_name)
//        selectedArray.append(searchedArray[sender.tag].property_name)
//        print(selectedArray)
//        selectedArray = selectedArray.uniqued()
//        print(selectedArray)
        MainCategoryTV.reloadData()
        SubCategoryTV.reloadData()
    }
    
}
extension MHFilterVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == MainCategoryTV{
            if self.filterData != nil{
                return self.filterData.data.count
            }else{
                return 0
            }
        }else{
            if self.filterData != nil{
                return searchedArray.count
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == MainCategoryTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterMainTVC") as! FilterMainTVC
            if self.filterData != nil{
                cell.TitleLb.text = self.filterData.data[indexPath.row].filterproperties[0].property_name
                if SelectedMainIndex == indexPath.row{
                    cell.BGView.backgroundColor = UIColor.black
                    cell.TitleLb.textColor = UIColor.white
                }else{
                    cell.BGView.backgroundColor = UIColor.clear
                    cell.TitleLb.textColor = UIColor.black
                }
                cell.CountView.isHidden = true
                if self.filterData.data[indexPath.row].filterproperties[0].property_name == "Price"{
                    if sliderValueChanged{
                        cell.CountView.isHidden = false
                        cell.CountLb.text = "1"
                    }else{
                        cell.CountView.isHidden = true
                    }
                }else{
                    let selectedCount = self.filterData.data[indexPath.row].filterpropertieitems.filter({ (filterData) -> Bool in
                        filterData.selectionStatus == 1
                    })
                    if selectedCount.count != 0{
                        cell.CountView.isHidden = false
                        cell.CountLb.text = "\(selectedCount.count)"
                        SelectedIndexArray.append(indexPath.row)
                        
                    }else{
                        cell.CountView.isHidden = true
                    }
                }
                
                
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterSubTVC") as! FilterSubTVC
            if self.filterData != nil{
                cell.TitleLb.text = searchedArray[indexPath.row].property_name
                cell.SelectionBTN.tag = indexPath.row
                if searchedArray[indexPath.row].selectionStatus == 1{
                    cell.CheckImg.image = UIImage(named: "Checked")
                    cell.TitleLb.textColor = UIColor.MHGold
                }else{
                    cell.CheckImg.image = UIImage(named: "Rectangle 29")
                    cell.TitleLb.textColor = UIColor.black
                }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == MainCategoryTV{
            SearchTF.text = ""
            SearchTF.resignFirstResponder()
//            self.selectedMainArray[self.SelectedMainIndex] = selectedArray
//            selectedArray.removeAll()
            SelectedMainIndex = indexPath.row
            if self.filterData.data[self.SelectedMainIndex].filterproperties[0].property_name == "Price"{
                self.sliderView.isHidden = false
            }else{
                self.sliderView.isHidden = true
            }
            self.searchedArray = self.filterData.data[self.SelectedMainIndex].filterpropertieitems

            self.MainCategoryTV.reloadData()
            self.SubCategoryTV.reloadData()
        }else{
            print(searchedArray[indexPath.row].property_name)
        }
        
    }
    func getFilterContent(){
        //https://www.masho.com/api_v3?action=filter&catsubcat=ssc&id=860&userId=123&tableid=32323432435234
        let parameters = ["action":"filter",
                          "catsubcat":self.SubCategoryId,
                          "id":self.categoryId,
                          "tableid": TableID!,
                          "userId" : UserID!,
                          "guest_id": guest_id!] as [String : Any]
        //        EmptyView.isHidden = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response as? [String:Any]{
                    self.filterData = MHFiltermodel(fromData: data)
                }
                for data in self.filterData.sort.options{
                    self.DropDownDataSource.append(data.text)
                }
                self.ResultCountLb.text = "Result(\(self.filterData.filtercount!))"
                self.searchedArray = self.filterData.data[self.SelectedMainIndex].filterpropertieitems
                for data in self.filterData.data{
                    if data.filterproperties[0].property_name == "Price"{
                        self.slider.minimumValue = CGFloat(data.filterpropertieitems[0].pricefrom)
                        self.slider.maximumValue = CGFloat(data.filterpropertieitems[0].priceto)
                        self.slider.value = [CGFloat(data.filterpropertieitems[0].pricefrom), CGFloat(data.filterpropertieitems[0].priceto)]
                        self.minValue.text = "\(data.filterpropertieitems[0].currency!) \(data.filterpropertieitems[0].pricefrom!)"
                        self.maxValue.text = "\(data.filterpropertieitems[0].currency!) \(data.filterpropertieitems[0].priceto!)"
                    }
                }
                self.sliderValueChanged = false
                for data in self.filterData.data{
                    for item in data.filterpropertieitems{
                        item.selectionStatus = 0
                    }
                }
                self.SelectedIndexArray.removeAll()
                for data in self.filterData.data{
                    if data.filterproperties[0].property_name == "Price"{
                        self.slider.value = [CGFloat(data.filterpropertieitems[0].pricefrom), CGFloat(data.filterpropertieitems[0].priceto)]
                        self.minValue.text = "\(data.filterpropertieitems[0].currency!) \(data.filterpropertieitems[0].pricefrom!)"
                        self.maxValue.text = "\(data.filterpropertieitems[0].currency!) \(data.filterpropertieitems[0].priceto!)"
                    }
                }
                print(self.FilterAppliedArray)
                
                if self.FilterAppliedArray.keys.count > 3{
                    if self.FilterAppliedArray["vid"] as? String != nil || self.FilterAppliedArray["bid"] as? String != nil{
                        if self.FilterAppliedArray["vid"] as? String != nil{
                            self.vidString = self.FilterAppliedArray["vid"] as! String
                        }
                        if self.FilterAppliedArray["bid"] as? String != nil{
                            self.bidString = self.FilterAppliedArray["bid"] as! String
                        }
                        let vidArray = self.vidString.components(separatedBy: ",")
                        let bidArray = self.bidString.components(separatedBy: ",")
                        print(vidArray)
                        for vid in vidArray{
                            for searchedAry in self.filterData.data{
                                for items in searchedAry.filterpropertieitems{
                                    if items.property_id == vid{
                                        items.selectionStatus = 1
                                    }
                                }
                            }
                        }
                        for bid in bidArray{
                            for searchedAry in self.filterData.data{
                                for items in searchedAry.filterpropertieitems{
                                    if items.property_id == bid{
                                        items.selectionStatus = 1
                                    }
                                }
                            }
                        }
                    }
                    //                        self.sliderValueChanged = true

                }
                if self.FilterAppliedArray["pricefrom"] as? String != nil || self.FilterAppliedArray["priceto"] as? String != nil{
                    
                    for data in self.filterData.data{
                        if data.filterproperties[0].property_name == "Price"{
                            let pricefrmAftrFilter = Int((self.FilterAppliedArray["pricefrom"] as? String)!)
                            let pricefrmBefrFilter = data.filterpropertieitems[0].pricefrom
                            let pricetoAftrFilter = Int((self.FilterAppliedArray["priceto"] as? String)!)
                            let pricetoBefrFilter = data.filterpropertieitems[0].priceto
                            if pricefrmAftrFilter != pricefrmBefrFilter || pricetoAftrFilter != pricetoBefrFilter{
                                self.sliderValueChanged = true
                                self.slider.value = [CGFloat(pricefrmAftrFilter!), CGFloat(pricetoAftrFilter!)]
                                self.minValue.text = "\(data.filterpropertieitems[0].currency!) \(pricefrmAftrFilter!)"
                                self.maxValue.text = "\(data.filterpropertieitems[0].currency!) \(pricetoAftrFilter!)"
                            }
                            
                        }
                    }
                }
                self.SortTypeLb.text = self.DropDownDataSource[0]
                self.selectedSortType = self.filterData.sort.options[0].value
                self.dropDownSetUp(dataSource:self.DropDownDataSource)
                if self.FilterAppliedArray.keys.count != 0{
                let sortby = self.FilterAppliedArray["sortby"] as! Int
                print(sortby)
                for options in self.filterData.sort.options{
                    if options.value == sortby{
                        self.SortTypeLb.text = options.text
                        self.selectedSortType = options.value
                    }
                }
                }
                self.searchedArray = self.filterData.data[self.SelectedMainIndex].filterpropertieitems

                self.MainCategoryTV.reloadData()
                self.SubCategoryTV.reloadData()
                
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}

extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
