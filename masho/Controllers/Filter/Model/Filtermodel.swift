//
//  Filtermodel.swift
//  masho
//
//  Created by Castler on 30/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit

class MHFiltermodel{
    var data = [MHFilterDatamodel]()
    var clearin_refresh : Int!
    var errorcode : Int!
    var filtercount : Int!
    var message:String!
    var name:String!
    var sort :MHFilterSortModel!
    var type :Int!
    init(fromData data: [String:Any]) {
        self.clearin_refresh = data["clearin_refresh"] as? Int
        self.errorcode = data["errorcode"] as? Int
        self.filtercount = data["filtercount"] as? Int
        self.message = data["message"] as? String
        self.name = data["name"] as? String
        self.type = data["type"] as? Int
        if let datas = data["data"] as? [[String:Any]]{
            for data in datas{
                self.data.append(MHFilterDatamodel(fromData: data))
            }
        }
        if let sortData = data["sort"] as? [String:Any]{
            self.sort = MHFilterSortModel(fromData: sortData)
        }
    }
}
class MHFilterDatamodel{
    var filterpropertieitems = [MHFilterPropertiesItemsModel]()
    var filterproperties = [MHFilterPropertiesModel]()
    init(fromData data: [String:Any]) {
        if let filterpropertieitemsData = data["filterpropertieitems"] as? [[String:Any]]{
            for data in filterpropertieitemsData{
                self.filterpropertieitems.append(MHFilterPropertiesItemsModel(fromData: data))
            }
            
        }
        if let filterpropertyData = data["filterproperties"] as? [[String:Any]]{
            for data in filterpropertyData{
                self.filterproperties.append(MHFilterPropertiesModel(fromData: data))
            }
            
        }
    }
}
class MHFilterPropertiesItemsModel{
    var property_id :String!
    var property_name :String!
    var currency :String!
    var pricefrom :Int!
    var priceto :Int!
    var selectionStatus : Int!
    init(fromData data: [String:Any]) {
        self.property_id = data["property_id"] as? String
        self.property_name = data["property_name"] as? String
        self.currency = data["currency"] as? String
        self.pricefrom = data["pricefrom"] as? Int
        self.priceto = data["priceto"] as? Int
        self.selectionStatus = 0

    }
}
class MHFilterPropertiesModel{
    var name :String!
    var property_name :String!
    var property_id :String!
    var type :String!
    var from :String!
    var to : String!
    init(fromData data: [String:Any]) {
        self.name = data["name"] as? String
        self.property_name = data["property_name"] as? String
        self.property_id = data["property_id"] as? String
        self.type = data["type"] as? String
        self.from = data["from"] as? String
        self.to = data["to"] as? String
    }
}
class MHFilterSortModel{
    var display_filter :Int!
    var name:String!
    var options = [MHFilterSortOptionsModel]()
    var text:String!
    init(fromData data: [String:Any]) {
        self.name = data["name"] as? String
        self.text = data["text"] as? String
        self.display_filter = data["display_filter"] as? Int
        if let optionsData = data["options"] as? [[String:Any]]{
            for data in optionsData{
                self.options.append(MHFilterSortOptionsModel(fromData: data))
            }
        }
    }
}
class MHFilterSortOptionsModel{
    var image :String!
    var text :String!
    var value : Int!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.text = data["text"] as? String
        self.value = data["value"] as? Int
    }
}
