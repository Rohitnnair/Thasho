//
//  popupModel.swift
//  masho
//
//  Created by Appzoc on 10/06/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class getCurrencyByCountryIdModel {
    var currency = [SideMenuCurrencySelectDataModel]()
    var language = [SideMenuLanguageSelectDataModel]()
    init(fromData data: [String:Any]) {
        if let dataList = data["currency"] as? [[String : Any]]{
            currency = [SideMenuCurrencySelectDataModel]()
            for item in dataList{
                self.currency.append(SideMenuCurrencySelectDataModel(fromData: item))
            }
        }
        if let dataList = data["language"] as? [[String : Any]]{
            language = [SideMenuLanguageSelectDataModel]()
            for item in dataList{
                self.language.append(SideMenuLanguageSelectDataModel(fromData: item))
            }
        }
    }
}
class DropDownModel{
    var api_id : Int!
    var apiname : String!
    var image : String!
    var link : String!
    var text : String!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.api_id = data["api_id"] as? Int ?? 0
        self.apiname = data["apiname"] as? String ?? ""
        self.image = data["image"] as? String ?? ""
        self.link = data["link"] as? String ?? ""
        self.text = data["text"] as? String ?? ""
        self.type = data["type"] as? Int ?? 0
    }
}
class searchModel{
    var api_id : String!
    var apiname : String!
    var catsubcat : String!
    var numrows : String!
    var searchkey : String!
    var searchvalue : String!
    init(fromData data: [String:Any]) {
        self.api_id = data["api_id"] as? String
        self.apiname = data["apiname"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.numrows = data["numrows"] as? String
        self.searchkey = data["searchkey"] as? String
        self.searchvalue = data["searchvalue"] as? String
    }
}
