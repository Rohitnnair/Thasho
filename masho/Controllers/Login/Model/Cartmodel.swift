//
//  Cartmodel.swift
//  masho
//
//  Created by Appzoc on 20/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
class MHCartItemModel{
    var popup : MHpopupModel!
    init(fromData data: [String:Any]) {
        if let PopUpData = data["popup"] as? [String:Any]{
            self.popup = MHpopupModel(fromData: PopUpData)
        }
    }
}
class MHCartmodel{
    var OrderSummary = [MHcartOrderSummaryModel]()
    var cartItems = [MHcartProductsModel]()
    var popup : MHpopupModel!
    var labels : MHcartLabelsModel!
    var checkouturl : String!
    init(fromData data: [String:Any]) {
        self.checkouturl = data["checkouturl"] as? String
        if let OrderSummaryList = data["OrderSummary"] as? [[String:Any]]{
            OrderSummary = [MHcartOrderSummaryModel]()
            for item in OrderSummaryList{
                self.OrderSummary.append(MHcartOrderSummaryModel(fromData: item))
            }
        }
        
        if let PopUpData = data["popup"] as? [String:Any]{
            self.popup = MHpopupModel(fromData: PopUpData)
        }
        if let cartItemList = data["cartItems"] as? [[String:Any]]{
            cartItems = [MHcartProductsModel]()
            for item in cartItemList{
                self.cartItems.append(MHcartProductsModel(fromData: item))
            }
        }
        if let labelList = data["labels"] as? [String:Any]{
            self.labels = MHcartLabelsModel(fromData: labelList)
        }
    }
}
class MHcartOrderSummaryModel{
    var text : String!
    var val : String!
    var subtext : String!
    var style : OrderSummaryPriceEntryLoopStyleModel!
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        self.val = data["val"] as? String
        self.subtext = data["subtext"] as? String
        if let savingamounttxtdatas = data["style"] as? [String:Any]{
            self.style = OrderSummaryPriceEntryLoopStyleModel(fromData: savingamounttxtdatas)
        }
    }
}
class MHcartProductsModel{
    var description : String!
    var imageUrl : String!
    var price : String!
    var strikeprice : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var qty : String!
    var avalablestock : Int!
    var totalitemprice : String!
    var deliveryMode : String!
    var favorite : String!
    var mashoAssured : String!
    var offerpercent : Int!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var type : String!
    init(fromData data: [String:Any]) {
        self.description = data["description"] as? String
        self.imageUrl = data["imageUrl"] as? String
        self.strikeprice = data["strikeprice "] as? String
        self.price = data["price"] as? String
        self.productId = data["productId"] as? String
        self.productName = data["productName"] as? String
        self.productTitle = data["productTitle"] as? String
        self.qty = data["qty"] as? String
        self.avalablestock = data["avalablestock"] as? Int
        self.totalitemprice = data["totalitemprice"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
        self.favorite = data["favorite"] as? String
        self.mashoAssured = data["mashoAssured"] as? String
        self.offerpercent = data["offerpercent"] as? Int
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.type = data["type"] as? String
    }
}
class MHcartLabelsModel{
    var buttonone : MHCartbuttononeLabelModel!
    var buttontwo : MHCartbuttontwoLabelModel!
    var maintext : String!
    var secondtext : MHCartsecondtextLabelModel!
    var textfive : String!
    var textfour : String!
    var thirdtext : String!
    init(fromData data: [String:Any]) {
        self.maintext = data["maintext"] as? String
        self.textfive = data["textfive"] as? String
        self.textfour = data["textfour"] as? String
        self.thirdtext = data["thirdtext"] as? String
        if let buttononeList = data["buttonone"] as? [String:Any]{
            self.buttonone = MHCartbuttononeLabelModel(fromData: buttononeList)
        }
        if let buttontwoList = data["buttontwo"] as? [String:Any]{
            self.buttontwo = MHCartbuttontwoLabelModel(fromData: buttontwoList)
        }
        if let secondtextList = data["secondtext"] as? [String:Any]{
            self.secondtext = MHCartsecondtextLabelModel(fromData: secondtextList)
        }
    }
}
class MHCartbuttononeLabelModel{
    var background : String!
    var color : String!
    var text : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.text = data["text"] as? String
    }
}
class MHCartbuttontwoLabelModel{
    var background : String!
    var color : String!
    var text : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.text = data["text"] as? String
    }
}
class MHCartsecondtextLabelModel{
    var background : String!
    var color : String!
    var text : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.text = data["text"] as? String
    }
}
