//
//  DropDownVC.swift
//  masho
//
//  Created by Appzoc on 24/06/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class DropDown{
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: self.DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if self.DropDownData[index].type == 1{
                if self.DropDownData[index].apiname == "homepagedetails"{
                    self.navigationController?.popViewController(animated: true)
                }else if self.DropDownData[index].apiname == "getProductsByCatId"{
                    let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCategoryDetailsVC") as! MHCategoryDetailsVC
                    categoryDetailsVC.categoryArray = self.categoryArray
                    categoryDetailsVC.api_Key = self.DropDownData[index].api_id
                    if self.DropDownData[index].api_id == 147{
                        categoryDetailsVC.nav_Title = "Abayas/Burqas"
                    }else if self.DropDownData[index].api_id == 145{
                        categoryDetailsVC.nav_Title = "Hijabs/Scarves/Wraps"
                    }else if self.DropDownData[index].api_id == 5 {
                        categoryDetailsVC.nav_Title = "Full Sleeve Kurtis"
                    }else if self.DropDownData[index].api_id == 51{
                        categoryDetailsVC.nav_Title = "Bags"
                    }else{
                        categoryDetailsVC.nav_Title = ""
                    }
                    self.navigationController?.pushViewController(categoryDetailsVC, animated: true)
                }else  if self.DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else{
                    //getProductDetails
                }
            }else if self.DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = self.DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: self.DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
}
