//
//  MHdashBoardVC.swift
//  masho
//
//  Created by Appzoc on 27/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher
import KVSpinnerView
import CCBottomRefreshControl
import Firebase
var DropDownData = [DropDownModel]()
var DropDownDataSource = [String]()
var categoryArray = [categorymodel]()
var isDashboardUpdted = false
var isCountryUpdationCancelled = false
var CountryPopupFromDashboard = false
var updatedProduct : String!
var keyboardHeight: CGFloat = 0
var screenWidth: CGFloat = 0
var screenHeight: CGFloat = 0
var categoryListArray1 = [categoryListmodel]()
enum CurrencyPopupScreens{
    case dashboard
    case sidemenu
}
class MHdashBoardVC: UIViewController {
//    var categoryArray = ["Abayas - Burqas","Hijabs - carves - Wraps","Full Sleeve Kurti's","Bags"]
    var banners  = [bannerImagesModel]()//
    var bannersBottom  = [bannerImagesModel]()
    var categoryListArray = [categoryListmodel]()
    var fresharrivalsArray = [freshArrivalsModel]()
    var recentlyviewedArray : recentlyviewedModel?
    var SideMenuArray = [SideMenuModel]()
    var SettingsArray = [SideMenuSettingsModel]()
    var ContactUsArray = [SideMenuContactUsModel]()
    var sideMenuColorArray = [UIColor]()
    var SideMenucountrydetails : SideMenucountrydetailsModel!
    var WelcomeDic : welcomeModel?
    let refreshControl = UIRefreshControl()
    var pageNo = 0
    var limit = 6
    var firstTimeCountryPopup : Bool!
//    let Device_token : String = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
//    let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
//    let TableID : String = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
//    let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var colorArray = [UIColor]()
    var ShopByCategoriesBackgroundColorArray = [UIColor]()
//    var SolidColorArray = [UIColor(red: 45/255, green: 126/255, blue: 193/255, alpha: 1),
//                        UIColor(red: 44/255, green: 33/255, blue: 143/255, alpha: 1),
//                        UIColor(red: 145/255, green: 16/255, blue: 59/255, alpha: 1),
//                        UIColor(red: 236/255, green: 60/255, blue: 25/255, alpha: 1),
//                        UIColor(red: 243/255, green: 175/255, blue: 34/255, alpha: 1),
//                        UIColor(red: 87/255, green: 159/255, blue: 43/255, alpha: 1)]
    var SolidColorArray = [UIColor.red,
                           UIColor.green,
                           UIColor.blue,
                           UIColor.orange,
                           UIColor.brown,
                           UIColor.cyan,
                           UIColor.green,
                           UIColor.magenta,
                           UIColor.purple,
                           UIColor.darkGray,
                           UIColor.black,
                           UIColor.red,
                           UIColor.green,
                           UIColor.blue,
                           UIColor.orange,
                           UIColor.brown,
                           UIColor.cyan,
                           UIColor.green,
                           UIColor.magenta,
                           UIColor.purple,
                           UIColor.darkGray,
                           UIColor.black]
    @IBOutlet weak var EmptyView: UIView!
    @IBOutlet weak var BannerPageControll: FSPageControl!
    @IBOutlet weak var BannerView: FSPagerView!
    @IBOutlet weak var DashboadrdTV: UITableView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var GoldBannerView: UIView!
    lazy var refreshControl1 = UIRefreshControl()
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    
    var wantReload :Bool = true
    var wantReload2 :Bool = false
    var first_time:Bool = true
    var shouldUpdate:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("BACKED FROM PAGE")
        initializeView()
        firstTimeCountryPopup = true
        NotificationCenter.default.addObserver(self, selector: #selector(contentSetup), name: Notification.dashboardContentSetup, object: nil)
        refreshControl.triggerVerticalOffset = 100.0
        refreshControl.tintColor = UIColor.MHGold
        DashboadrdTV.bottomRefreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(reloadList), for: UIControl.Event.valueChanged)
        self.Leftmenu()
        let screenRect = UIScreen.main.bounds
        screenWidth = screenRect.size.width
        screenHeight = screenRect.size.height
//        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification,object: nil)
    }
     func initializeView(){
        refreshControl1.tintColor = .gray
        refreshControl1.addTarget(self,action: #selector(reloadList1),for: .valueChanged)
        //refreshControl1.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        DashboadrdTV.refreshControl = refreshControl1
         let screenRect = UIScreen.main.bounds
         screenWidth = screenRect.size.width
         screenHeight = screenRect.size.height
//        ProductCV.addSubview(refreshControl1)
//        ProductCV.alwaysBounceVertical = true
        
    }
     
    @objc func reloadList1() {
        self.DashboadrdTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        self.Leftmenu()
        self.DashboardContentSetup()
        self.FreshArrivals(pageno: pageNo)
    }

//    @objc func keyboardWillShow(_ notification: Notification) {
//        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//            let keyboardRectangle = keyboardFrame.cgRectValue
//            keyboardHeight = keyboardRectangle.height
//        }
//    }
    
    @objc func reloadList(){
        if wantReload{
            self.FreshArrivals1(pageno: pageNo + 1)
        }
        if wantReload2 == false{
            self.FreshArrivals1(pageno: pageNo + 1)
        }
        self.refreshControl.endRefreshing()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let order_id = UserDefaults.standard.value(forKey: "orderid") as? String ?? ""
        if order_id != ""{
            UserDefaults.standard.set("", forKey: "orderid")
        }
//        DashboadrdTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
//        self.bannerSetup()
//        self.BannerView.reloadData()
        logEvent()
        if shouldUpdate{
            shouldUpdate = false
            self.DashboardContentSetup()
        }
        if isDashboardUpdted{
            isDashboardUpdted = false
            for category in categoryArray{
                for products in category.products{
                    if updatedProduct == products.productID{
                        if products.shortlist == "0"{
                            products.shortlist = "1"
                        }else{
                            products.shortlist = "0"
                        }
                    }
                }
            }
            for products in fresharrivalsArray{
                if updatedProduct == products.api_id{
                    if products.favorite == 0{
                        products.favorite = 1
                    }else{
                        products.favorite = 0
                    }
                }
            }
            for products in recentlyviewedArray!.data{
                if updatedProduct == products.productId{
                    if products.favorite == 0{
                        products.favorite = 1
                    }else{
                        products.favorite = 0
                    }
                }
            }
            self.DashboadrdTV.reloadData()
        }
        
        
        if isorderPlaced{
            isorderPlaced = false
            cartItemsView.isHidden = true
            UserDefaults.standard.setValue(0, forKey: "CartItems")
        }else{
            if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
                if cartitemsCount == nil || cartitemsCount == 0{
                    cartItemsView.isHidden = true
                }else{
                    cartItemsView.isHidden = false
                    CartItemLb.text = "\(cartitemsCount)"
                }
            }
        }
        if isLanguageUpdated{
            isLanguageUpdated = false
            self.Leftmenu()
          //  self.DashboardContentSetup()
            pageNo = 0
           // self.FreshArrivals(pageno: pageNo)
        }
//        if isCountryUpdationCancelled{
//            isCountryUpdationCancelled = false
//            self.FreshArrivals(pageno: self.pageNo)
//            self.DashboardContentSetup()
//            self.DropDownList()
//        }
    }
    
    @objc func contentSetup(){
        shouldUpdate = true
        //self.DashboardContentSetup()
    }
    @IBAction func refreshDashboard(_ sender: Any) {
        self.DashboadrdTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        self.Leftmenu()
        self.DashboardContentSetup()
        self.FreshArrivals(pageno: pageNo)
    }
    func logEvent(){
        Analytics.logEvent("Dashboard_Opened", parameters: [:])
    }
    func bannerSetup(){
        self.DashboadrdTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        BannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "SliderCell")
        BannerView.itemSize = CGSize(width: BannerView.frame.width * 0.8, height: 190)
        BannerView.transformer = FSPagerViewTransformer(type:.linear)
        BannerPageControll.numberOfPages = self.banners.count//dot
        BannerPageControll.setImage(UIImage(named: "dot"), for: UIControl.State.selected)
        BannerPageControll.setImage(UIImage(named: "dot1"), for: UIControl.State.normal)
        BannerView.reloadData()
    }
    @IBAction func SideMenuBTNtapped(_ sender: Any) {
        if SideMenucountrydetails != nil{
            let DashboardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHSideMenuVC") as! MHSideMenuVC
    //        DashboardVC.categoryArray = categoryListArray
            DashboardVC.SideMenuArray = SideMenuArray
            DashboardVC.WelcomeDic = WelcomeDic
            DashboardVC.SettingsArray = SettingsArray
            DashboardVC.ContactUsArray = ContactUsArray
            DashboardVC.SideMenucountrydetails = SideMenucountrydetails
            DashboardVC.ColorArray = sideMenuColorArray
            self.navigationController?.pushViewController(DashboardVC, animated: true)
        }
        
//        let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewSideMenuVC") as! NewSideMenuVC
//        vc.categoryArray = categoryListArray
//        vc.SideMenuArray = SideMenuArray
//        vc.WelcomeDic = WelcomeDic
//        vc.SettingsArray = SettingsArray
//        vc.ContactUsArray = ContactUsArray
//        vc.SideMenucountrydetails = SideMenucountrydetails
//        vc.ColorArray = sideMenuColorArray
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ViewallrecentProducts(_ sender: Any) {
        
        let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
//        categoryDetailsVC.categoryArray = categoryListArray
        frompush = false
        self.navigationController?.pushViewController(categoryDetailsVC, animated: true)
        
    }
    
    
    @IBAction func CartBTNtapped(_ sender: Any) {
   //     Analytics.logEvent("CartBTNtapped", parameters: ["id":1])
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//        WebViewVC.categoryArray = self.categoryListArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
    }
    
    @IBAction func SearchBTNtapped(_ sender: UIButton) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "searchVC") as! searchVC
//        WebViewVC.categoryArray = self.categoryListArray
//        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
    }
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
        
    }
    

extension MHdashBoardVC: UITableViewDataSource,UITableViewDelegate,FSPagerViewDelegate,FSPagerViewDataSource, FavoriteProductDelegate,scrollToTopDelegate{
    func scrollToTop() {
       // self.DashboadrdTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    func redirectToLogin() {
        //thd
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 1
        }else if section == 2{
            return categoryArray.count
        }else if section == 3{
            return 1
        }else if section == 4{
            return 1
        }else if section == 5{
            return 1
        }else{
            return fresharrivalsArray.count
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return Cell
        }else if indexPath.section == 1 {
           let headerCell = tableView.dequeueReusableCell(withIdentifier: "MHheaderCategoryTVC") as! MHheaderCategoryTVC
            headerCell.navnCtler = self.navigationController
            headerCell.categoryArray = categoryListArray
            headerCell.ColorArray = ShopByCategoriesBackgroundColorArray
            headerCell.CategoryCV.reloadData()
            return headerCell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductsTVC", for: indexPath) as! MHProductsTVC
            cell.layoutIfNeeded()
            cell.layoutSubviews()
//            let randomNumber = arc4random_uniform(UInt32(colorArray.count))
            if colorArray.count == categoryArray.count{
                cell.ShowAllBTNColorView.backgroundColor = colorArray[indexPath.row].withAlphaComponent(0.5)
                cell.BackGroundView.backgroundColor = colorArray[indexPath.row].withAlphaComponent(0.15)
            }
            cell.categoryNameLb.text = categoryArray[indexPath.row].name
            cell.ProductArray = categoryArray[indexPath.row].products
            cell.navnCtler = self.navigationController
            cell.delegate = self
            cell.delegatee = self
//            cell.categoryArray = categoryListArray
            cell.index = indexPath.row
          
            self.view.setNeedsLayout()
            cell.ProductCV.reloadData()
            return cell
        }else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHBanner2TVC", for: indexPath) as! MHBanner2TVC
            cell.banners = self.banners
            cell.navCtler = self.navigationController
            cell.BannerView.reloadData()
            return cell
        }else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "recentProductsInDashboardTVC", for: indexPath) as! recentProductsInDashboardTVC
            if self.recentlyviewedArray != nil{
                cell.NewArrivalLb.text = self.recentlyviewedArray?.text
//                cell.categoryArray = categoryListArray
                cell.ProductArray = (self.recentlyviewedArray?.data)!
                cell.navnCtler = self.navigationController
                cell.ProductCV.reloadData()
            }
            return cell
        }else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "freshTVC", for: indexPath)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHNewProductsTVC", for: indexPath) as! MHNewProductsTVC
            cell.delegate = self
            cell.index = indexPath.row
            cell.ColorView.backgroundColor = UIColor(hex: "\(self.fresharrivalsArray[indexPath.row].backgroundcolor!)ff")
            cell.ProductImage1.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].mainimage))
            if self.fresharrivalsArray[indexPath.row].subimages.count >= 3{
                cell.ProductImage2.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].subimages[0].image))
                cell.ProductImage3.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].subimages[1].image))
                cell.ProductImage4.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].subimages[2].image))
            }else if self.fresharrivalsArray[indexPath.row].subimages.count == 2{
                cell.ProductImage2.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].subimages[0].image))
                cell.ProductImage3.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].subimages[1].image))
            }else{
                cell.ProductImage2.kf.setImage(with: URL(string: self.fresharrivalsArray[indexPath.row].subimages[0].image))
            }
            if fresharrivalsArray[indexPath.row].favorite == 1{
                cell.FavImage.image = UIImage(named: "favSelected")
            }else{
                cell.FavImage.image = UIImage(named: "favUnselected")
            }
            cell.OfferPercentage.text = "\(self.fresharrivalsArray[indexPath.row].offerpercent!) % Off"
            cell.OfferPercentage2Lb.text = "(\(self.fresharrivalsArray[indexPath.row].offerpercent!) % Off )"
            cell.ProductName.text = self.fresharrivalsArray[indexPath.row].productname
//            cell.ProductTitle.text = self.fresharrivalsArray[indexPath.row].Title
            cell.StrikePrice.textColor = UIColor.red
            cell.StrikePrice.attributedText = "\(self.fresharrivalsArray[indexPath.row].strikeprice!)".updateStrikeThroughFont(UIColor.red)
            cell.specialPrice.text = "\(self.fresharrivalsArray[indexPath.row].specialprice!)"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 190
        }else if indexPath.section == 2 {
            if categoryArray[indexPath.row].products.count != 0{
               return UITableView.automaticDimension
            }else{
                return 0
            }
           
            
        }else if indexPath.section == 4 {
            if self.recentlyviewedArray?.data.count != 0{
                return 300
            }else{
                return 0
            }
        }

        else{
            return UITableView.automaticDimension
        }
//        recentlyviewedArray
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 6 {
            let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//            MHProductDetailsVC.categoryArray = categoryListArray
            MHProductDetailsVC.isFrom = .dashboardFreshArrivals
            MHProductDetailsVC.BasicData3 = fresharrivalsArray[indexPath.row]
//            MHProductDetailsVC.productID = fresharrivalsArray[indexPath.row].api_id
            productID = fresharrivalsArray[indexPath.row].api_id
             frompush = false
            self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 4 &&  indexPath.row == fresharrivalsArray.count - 1 {
//            wantReload = true
//            self.pageNo =  pageNo + 1
//            self.FreshArrivals1(pageno: pageNo + 1)
//            self.perform(#selector(loadTable), with: nil, afterDelay: 0.3)
        }
    }
    
    @objc func loadTable() {
        wantReload = true
        self.pageNo =  pageNo + 1
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//            let headerCell = tableView.dequeueReusableCell(withIdentifier: "MHheaderCategoryTVC") as! MHheaderCategoryTVC
//            headerCell.navnCtler = self.navigationController
//        headerCell.categoryArray = categoryArray
//            return headerCell
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        if section == 0 {
//            return 0
//        }else{
//            return 145
//        }
//
//    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = DashboadrdTV.contentOffset.y
        let percentage = offset / 190
        BannerHeightConstraint.constant = 190 - DashboadrdTV.contentOffset.y
        BannerView.alpha = 1 - percentage
        BannerPageControll.alpha =  1 - percentage
        GoldBannerView.alpha =  1 - percentage
        //bannerSetup()
        BannerView.reloadData()
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cel = pagerView.dequeueReusableCell(withReuseIdentifier: "SliderCell", at: index)
//        cel.imageView?.image = UIImage(named: banners[index])
        cel.imageView?.kf.setImage(with: URL(string: banners[index].image))
        return cel
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if banners[index].type == 1{
            if banners[index].apiname == "homepagedetails"{
                let navigationStack = self.navigationController?.viewControllers ?? []
                for vc in navigationStack{
                    if let dashboardVC = vc as? MHdashBoardVC{
                        DispatchQueue.main.async {
                            self.navigationController?.popToViewController(dashboardVC, animated: true)
                        }
                    }
                }
            }else if banners[index].apiname == "getProductsByCatId"{
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                vc.categoryId = "\(banners[index].api_id!)"
                vc.SubCategoryId = "sc"
                self.navigationController?.pushViewController(vc, animated: true)
            }else if banners[index].apiname == "getCartItems" {
                let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                CartVC.categoryArray = self.categoryListArray
                frompush = false
                self.navigationController?.pushViewController(CartVC, animated: true)
            }else if banners[index].apiname == "getProductDetails" {
                let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                //        MHProductDetailsVC.categoryArray = categoryArray
                MHProductDetailsVC.isFrom = .other
//                MHProductDetailsVC.productID = String(banners[index].api_id)
                productID = String(banners[index].api_id)
                 frompush = false
                self.navigationController!.pushViewController(MHProductDetailsVC, animated: true)
            }else{
                //
            }
        }else if banners[index].type == 2{
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            WebViewVC.WebURL = banners[index].link
            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }else{
            if let newURL = URL(string: banners[index].link){
                UIApplication.shared.open(newURL)
            }
        }
    }
//    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
//        return false
//    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        if pagerView == self.BannerView{
            self.BannerPageControll.currentPage = pagerView.currentIndex
        }
    }
    func didSelectFavorite(indx: Int){
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        var type:String = ""
        if fresharrivalsArray[indx].favorite == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = [  "action":"Shortlist",
                            "userid":"\(UserID)",
                            "guestId": guest_id,
                            "productid": "\(fresharrivalsArray[indx].api_id!)",
                            "type" : "\(type)"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                if self.fresharrivalsArray[indx].favorite == 0{
                    self.fresharrivalsArray[indx].favorite = 1
                }else{
                    self.fresharrivalsArray[indx].favorite = 0
                }
                self.DashboadrdTV.reloadData()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension MHdashBoardVC: ViewAllProductDelegate{
    
    func DidSelectViewAll(index: Int){
        let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
        vc.categoryId = "\(categoryArray[index].api_id!)"
        vc.SubCategoryId = "sc"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension MHdashBoardVC: updateCountryDetailsDelegate{
    
    func DashboardContentSetup(){
        self.EmptyView.isHidden = false
        let Device_token : String = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let TableID : String = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = ["action":"homepagedetails",
                          "deviceid": Device_token,
                          "devicetype": 2,
                          "userid":"\(UserID)",
                          "guest_Id": guest_id,
                          "firebaseRegID": "1234"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let dashboardData = response["data"] as? [String: Any]{
                    categoryArray.removeAll()
                    self.categoryListArray.removeAll()
                    self.banners.removeAll()
                    self.bannersBottom.removeAll()
                    if let categoryListData = dashboardData["categorylist"] as? [[String: Any]]{
                        for CategoryList in categoryListData{
                            self.categoryListArray.append(categoryListmodel(fromData: CategoryList))
                        }
                    }
                    categoryListArray1 = self.categoryListArray
                    if let categoryData = dashboardData["categories"] as? [[String: Any]]{
                        for Category in categoryData{
                            categoryArray.append(categorymodel(fromData: Category))
                        }
                    }
                    if let Banners = dashboardData["bannerImages"] as? [[String: Any]]{
                        for item in Banners{
                            self.banners.append(bannerImagesModel(fromData: item))
                        }
                    }
                    if let Banners = dashboardData["bannerImagesbottom"] as? [[String: Any]]{
                        for item in Banners{
                            self.bannersBottom.append(bannerImagesModel(fromData: item))
                        }
                    }
                    if let recentlyviewed = dashboardData["recentlyviewed"] as? [String: Any]{
                        self.recentlyviewedArray = recentlyviewedModel(fromData: recentlyviewed)
                    }
//                    self.banners = ["banner1","banner2","banner3"]
                    
                }
                self.SolidColorArray.shuffle()
                for i in 0 ..< categoryArray.count{
                    self.colorArray.append(self.SolidColorArray[i])
                }
                self.SolidColorArray.shuffle()
                for i in 0 ..< self.categoryListArray.count{
                    self.ShopByCategoriesBackgroundColorArray.append(self.SolidColorArray[i])
                }
                self.bannerSetup()
                self.BannerView.reloadData()
                self.EmptyView.isHidden = true
                self.DashboadrdTV.reloadData()
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
        
        
        
    }
    
//    func BannerImages(){
//        let parameters = ["action":"fetch_banner",
//                          "deviceid": Device_token,
//                          "devicetype": 2,
//                          "firebaseRegID": "1234"] as [String : Any]
//        self.view.alpha = 0.5
//        self.view.isUserInteractionEnabled = false
//        KVSpinnerView.show()
//        NetworkManager.webcallWithErrorCode(urlString: "https://www.masho.com/api/", methodeType: .post,parameter: parameters) { (status, response) in
//            print(response)
//            switch status {
//            case .noNetwork:
//                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
//            case .success :
//                print("success")
//                print(response["data"]!)
//                if let Bannerdata = response["data"] as? [String]{
//                    for bannerItem in Bannerdata{
//                        self.banners.append(bannerItem)
//                    }
//                }
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                self.BannerView.reloadData()
//            case .failure :
//                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
//            case .unknown:
//                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
//            }
//        }
//    }
    
    func Leftmenu(){
        
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = ["action":"leftmenu",
                          "userid": UserID,
                          "guestId": guest_id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                self.SideMenuArray.removeAll()
                
                if let leftMenuData = response["data"] as? [[String : Any]]{
                    for data in leftMenuData{
                        self.SideMenuArray.append(SideMenuModel(fromData: data))
                    }
                }
                if let WelcomeData = response["logindetails"] as? [String : Any]{
                    self.WelcomeDic = welcomeModel(fromData: WelcomeData)
                }
                if let countrydetails = response["countrydetails"] as? [String : Any]{
                    self.SideMenucountrydetails = SideMenucountrydetailsModel(fromData: countrydetails)
                }
                if let SettingsData = response["settings"] as? [[String : Any]]{
                    self.SettingsArray = [SideMenuSettingsModel]()
                    for item in SettingsData{
                        self.SettingsArray.append(SideMenuSettingsModel(fromData: item))
                    }
                }
                if let contactusData = response["contactus"] as? [[String : Any]]{
                    self.ContactUsArray = [SideMenuContactUsModel]()
                    for item in contactusData{
                        self.ContactUsArray.append(SideMenuContactUsModel(fromData: item))
                    }
                }
                for _ in 0 ..< self.SideMenuArray.count{
                    self.sideMenuColorArray.append(UIColor.random)
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                UserDefaults.standard.set(self.SideMenucountrydetails.countryname, forKey: "CountryName")
                UserDefaults.standard.set(self.SideMenucountrydetails.countryid, forKey: "CountryCode")
                if let ShowCountryPopup =  UserDefaults.standard.value(forKey: "ShowCountryPopup") as? Bool{
                    if ShowCountryPopup{
                        
                        let screenshot  = self.view.takeScreenshot()
                        let PopupVC = StoryBoard.login.instantiateViewController(withIdentifier: "Country_Language_CurrencyPopupVC") as! Country_Language_CurrencyPopupVC
                        PopupVC.CurrencyPopupScreen = .dashboard
                        PopupVC.BackGroundImage = screenshot
                        PopupVC.delegate = self
                        PopupVC.SideMenucountrydetails = self.SideMenucountrydetails
                        self.navigationController?.pushViewController(PopupVC, animated: false)
                    }else{
                        self.FreshArrivals(pageno: self.pageNo)
                        self.DashboardContentSetup()
                        self.DropDownList()
                    }
                }
                
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func FreshArrivals(pageno: Int){
        self.EmptyView.isHidden = false
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = [  "action":"fresharrivals",
                            "userid":"\(UserID)",
                            "guest_Id": guest_id,
                            "pageno" : pageno] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                
                print("success")
                print(response["data"]!)
                if let dataList = response["data"] as? [[String : Any]]{
                    self.fresharrivalsArray = [freshArrivalsModel]()
                    for data in dataList{
                        self.fresharrivalsArray.append(freshArrivalsModel(fromData: data))
                    }
                }
//                for _ in 0 ..< self.fresharrivalsArray.count{
//                    self.NewProductcolorArray.append(UIColor.random)
//                }
                self.wantReload = true
                self.wantReload2 = false
                self.EmptyView.isHidden = true
                self.DashboadrdTV.reloadData()
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                if response["Message"] == nil{
                    Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                }else{
                    Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)" , style: .danger)
                }
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func FreshArrivals1(pageno: Int){
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = [  "action":"fresharrivals",
                               "userid":"\(UserID)",
            "guest_Id": guest_id,
            "pageno" : pageno] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.refreshControl.endRefreshing()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                
                print("success")
                print(response["data"]!)
                if let dataList = response["data"] as? [[String : Any]]{
                    for data in dataList{
                        self.fresharrivalsArray.append(freshArrivalsModel(fromData: data))
                    }
                }
//                for _ in 0 ..< self.fresharrivalsArray.count{
//                    self.NewProductcolorArray.append(UIColor.random)
//                }
                self.refreshControl.endRefreshing()
                if self.wantReload == true && self.wantReload2 == false{
                    self.wantReload = false
                    self.wantReload2 = true
                }else{
                    self.wantReload = true
                    self.wantReload2 = false
                }

//                print(self.NewProductcolorArray.count)
//                self.DashboadrdTV.scrollToRow(at: IndexPath(row: self.fresharrivalsArray.count, section: 4), at: UITableView.ScrollPosition.none, animated: false)
                self.DashboadrdTV.reloadData()
            case .failure :
                print("failure")
                self.refreshControl.endRefreshing()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.refreshControl.endRefreshing()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
        self.refreshControl.endRefreshing()
    }
    func DropDownList(){
        //https://www.masho.com/api?action=dropmenu&userid=803
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = ["action" : "dropmenu",
                          "userid" : UserID,
                          "guestId": guest_id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                DropDownData.removeAll()
                DropDownDataSource.removeAll()
                if let data = response["data"] as? [[String: Any]]{
                    DropDownData = [DropDownModel]()
          
                    for item in data{
                        DropDownData.append(DropDownModel(fromData: item))
                    }
                }
                
                for item in DropDownData{
                    DropDownDataSource.append(item.text)
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func updateCountryDetails(CurrencyCode: String,languageCode: String,CountryCode : String,countryName: String ){
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        UserDefaults.standard.set(countryName, forKey: "CountryName")
        let parameters = ["action" : "updatecountrysettings",
                          "userId" : UserID,
                          "guestId": guest_id,
                          "currency" : CurrencyCode,
                          "language" : languageCode,
                          "country" : CountryCode,
                          "countryname" : countryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                UserDefaults.standard.set(false, forKey: "ShowCountryPopup")
                self.FreshArrivals(pageno: self.pageNo)
                self.DashboardContentSetup()
                self.DropDownList()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
