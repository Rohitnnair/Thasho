//
//  dummyViewController.swift
//  masho
//
//  Created by Appzoc on 19/08/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks
class dummyViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        if let incomingURL = URL(string: "https://mashoproduct.page.link/P6kBCcvyjHGokbHa6") {
            print("Incoming URL is :\(incomingURL)")
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                guard error == nil else {
                    print("Found an error :\(String(describing: error?.localizedDescription))")
                    return
                }
                if let dynamicLink = dynamicLink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            }
            if linkHandled {
                print("linkHandled")
            } else {
                print("link Not Handled")
                // Handle Other cases
            }
        }
    }
    
    fileprivate func handleIncomingDynamicLink(_ dynamicLink:DynamicLink) {
        guard let url = dynamicLink.url else {
            print("link Object have no url")
            return
        }
        print("Incoming link parameter is :\(url.absoluteString)")
        var info: [String: String] = [:]
        
        URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
            info[$0.name] = $0.value
        }
        
        guard dynamicLink.matchType == .unique || dynamicLink.matchType == .default else {return}
        redirect(withInfo: info)
    }
    
    fileprivate func redirect(withInfo info:[String:Any]) {
        if let value = info.first?.value {
            let product_id = (value as AnyObject).description
            let destinationVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            destinationVC.isFrom = .other
//            destinationVC.productID = product_id ?? ""
            productID = product_id ?? ""
             frompush = false
            self.present(destinationVC, animated: true, completion: nil)
            
        }
    }

}
