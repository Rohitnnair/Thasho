//
//  MHProductDetailsVC.swift
//  masho
//
//  Created by Appzoc on 06/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//MARK: - PLAYER , PLAYER LAYER , sizes
//--------------------------------------
var MHplayer:AVPlayer?
var MHavPlayerLayer : AVPlayerLayer?
var selectedsizes :String?
var productIDForAPI:String = ""
var productID:String = ""
//--------------------------------------

import UIKit
import KVSpinnerView
import Kingfisher
import Cosmos
import Firebase
import FirebaseDynamicLinks
import DropDown
import AlamofireImage
import ImageSlideshow
import SafariServices
import AVKit
import SkeletonView
import AVFoundation
import NotificationBannerSwift
import Alamofire

var addReviewBeforeLogin = false
var ViewReviewBeforeLogin = false
var refreshforplayer = false
var indicator : UIActivityIndicatorView!

var MHrefreshControl = UIRefreshControl()
enum toProductDetails{
    case dashboard
    case dashboardRecent
    case categoryDetails
    case dashboardFreshArrivals
    case wishlist
    case cart
    case productDetailsSimilarProducts
    case productDetailsRecentProducts
    case newCategoryDetails
    case newCategoryDetailsDeals
    case other
    
}

class MHProductDetailsVC: UIViewController {
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        MHplayer?.seek(to: .zero)
        print("replaying")
          MHplayer?.play()
        }
 
    var refreshControlz = UIRefreshControl()
    var ColorArray = ["S","M","L","XL","XS","XXL","3XL","4XL","5XL","6XL","7XL"]
    var ProductArray = ["S","M","L","XL"]
    var StringArray = [String]()
    
    var isrefreshed = false
    var isfromcolor = false
   
    var UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
    var guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var SpecificationSection = 0
    var ProductDetailsData : MHProductDetailsModel! {
        didSet {
            guard ProductDetailsData != nil else {return}
            var copy = self.ProductDetailsData.ItemImageList
            copy.removeFirst()
            arrSubOverView = copy
           // arrSubOverView = self.ProductDetailsData.ItemImageList
        }
    }
    var arrSubOverView = [MHItemImageListModel]()
   
    var ProductDetailsData2 : MHProductDetailsModel2!
    
    var thumImgArray = [MHItemImageListModel]()
    var categoryArray = [categoryListmodel]()
    var dropDown = DropDown()
    var isFrom : toProductDetails = .other
    var BasicData : ProductModel!
    var BasicData1 : recentlyviewedProductModel!
    var BasicData2: CategoryProductModel!
    var BasicData3: freshArrivalsModel!
    var BasicData4: MHWishListDataModel!
    var BasicData5: MHcartProductsModel!
    var BasicData6: MHsimilarProductsbodyModel!
    var BasicData7: MHrecentlyViewedbodyModel!
    var BasicData8: MHcategoryProduct_ProductModel!
    var BasicData9: MHcategoryProduct_ProductModel2!
    var offset : CGFloat! = nil
    var isReadMoreSelected = false
    var AddToBagData : MHpopupModel!
    var VideoResolutiontype : Int!
    
//    @IBOutlet weak var ProductNameView: UIView!
//    @IBOutlet weak var smallProductWidthConstraint: NSLayoutConstraint!
//    @IBOutlet weak var SmallProductImage: UIImageView!
//    @IBOutlet weak var FavImageView: UIImageView!
//    @IBOutlet weak var PercentageLb: UILabel!
//    @IBOutlet weak var PercentageView: BaseView!
//    @IBOutlet weak var SpecialPriceLB: UILabel!
//    @IBOutlet weak var OldPriceLb: UILabel!
//    @IBOutlet weak var ProductNameLb: UILabel!
//    @IBOutlet weak var rateViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet weak var ProductImageViewHeightConstraint: NSLayoutConstraint!
  
    @IBOutlet weak var CloseFullimageIcon: UIImageView!
    @IBOutlet weak var FullScreenIcon: UIImageView!
    @IBOutlet weak var ViewImageInFullScreenBTN: UIButton!
    
    @IBOutlet weak var AddtoBagBTN: UIButton!
    @IBOutlet weak var outOfstockView: UIView!
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var AnimationImage: UIImageView!
    
    @IBOutlet weak var EmptyView: UIView!
    @IBOutlet weak var mtimageview: UIImageView!
    @IBOutlet weak var BottomStrikePrice: UILabel!
    @IBOutlet weak var BottomPiceLb: UILabel!
    
    @IBOutlet public weak var ProductTV: UITableView!
    @IBOutlet weak var ProductImageView: UIView!
    @IBOutlet weak var ProductImageCV: UICollectionView!
    
    @IBOutlet weak var ThumnailView: UIView!
    @IBOutlet weak var ThumnailCV: UICollectionView!
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    
    @IBOutlet weak var MainPopUpView:UIView!
    @IBOutlet weak var PopUpMainTitle: UILabel!
    @IBOutlet weak var PopUpSubTitle: UILabel!
    @IBOutlet weak var PopUpBTNView:UIView!
    @IBOutlet weak var PopUpBTNTitle: UILabel!
    @IBOutlet weak var CloseBTNView:UIView!
    @IBOutlet weak var ShareBTNView:UIView!
    var index = 0
    @IBOutlet weak var fullimageDisplayingView: UIView!
    @IBOutlet weak var IMGslideShow: ImageSlideshow!
    
    override func viewDidLoad() {
        self.EmptyView.isHidden = false
        let wronggiffer = UIImage.gifImageWithName("wrong", ft: 1000)
        mtimageview.image = wronggiffer
        super.viewDidLoad()
        selectedsizes = nil
       initializeView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: MHplayer?.currentItem)
        fullimageDisplayingView.isHidden = true
        IMGslideShow.delegate = self
        self.view.showAnimatedGradientSkeleton()

        ProductDetailsSplitApi1()
        self.AnimationImage.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height - 60 )
        self.AnimationImage.isHidden = true
        if isrefreshed{
            self.navigationController?.popViewController(animated: false)
            ProductDetailsSplitApi1()
        }
    }
   
    override func viewDidLayoutSubviews() {
        CloseBTNView.layer.cornerRadius = (CloseBTNView.frame.width)/2
    }
    var imageSource = [AlamofireSource]()
    func ImageSlideSetup(inputSource : [AlamofireSource]){
        IMGslideShow.zoomEnabled = true
        IMGslideShow.circular = true
        let pagecontrol = UIPageControl()
        pagecontrol.hidesForSinglePage = true
        pagecontrol.currentPageIndicatorTintColor = UIColor.MHGold
        pagecontrol.pageIndicatorTintColor = UIColor.lightGray.withAlphaComponent(0.7)
        IMGslideShow.pageIndicator = pagecontrol
        IMGslideShow.setImageInputs(inputSource)
        
    }
    
    @IBAction func ShippingBTN(_ sender: UIButton) {
        if let Url = URL(string: ProductDetailsData.size_shipping.shipping_return.weburl) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
            
            let vc = SFSafariViewController(url: Url, configuration: config)
                present(vc, animated: true)
        }}
    
    @IBAction func SizeGuideBTN(_ sender: UIButton) {
        if let Url = URL(string: ProductDetailsData.size_shipping.sizechart.weburl)  {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true

                let vc = SFSafariViewController(url: Url, configuration: config)
                present(vc, animated: true)
        }}
    
    
    
    @IBAction func mashologoBTNtapped(_ sender: Any) {
        let navigationStack = self.navigationController?.viewControllers ?? []
        for vc in navigationStack{
            if let dashboardVC = vc as? MHdashBoardVC{
                DispatchQueue.main.async {
                    self.navigationController?.popToViewController(dashboardVC, animated: false)
                }
            }
        }
    }
    @IBAction func ClosePopUpView(_ sender: Any) {
        self.MainPopUpView.isHidden = true
    }
    @IBAction func ContinueShoppingInPopUpView(_ sender: Any) {
        self.MainPopUpView.isHidden = true
        if ProductDetailsData.popup.apiname == "getProductsByCatId"{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
            vc.categoryId = "\(DropDownData[index].api_id!)"
            vc.SubCategoryId = "sc"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ProductDetailsData.popup.apiname == "homepagedetails"{
            let navigationStack = navigationController?.viewControllers ?? []
            for vc in navigationStack{
                if let dashboardVC = vc as? MHdashBoardVC{
                    DispatchQueue.main.async {
                        self.navigationController?.popToViewController(dashboardVC, animated: true)
                    }
                }
            }
        }else if ProductDetailsData.popup.apiname == "getProductDetails"{
            let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            MHProductDetailsVC.isFrom = .other
//            MHProductDetailsVC.productID = ProductDetailsData.popup.api_id
            productID = ProductDetailsData.popup.api_id
            frompush = false
            self.navigationController!.pushViewController(MHProductDetailsVC, animated: true)
        }else if ProductDetailsData.popup.apiname == "getCartItems" {
            let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
            //                CartVC.categoryArray = self.categoryListArray
            frompush = false
            self.navigationController?.pushViewController(CartVC, animated: true)
        }
    }
    @IBAction func CloseFullImageBTNtapped(_ sender: Any) {
        fullimageDisplayingView.isHidden = true
    }
    @IBAction func FullImageBTNtapped(_ sender: Any) {
        let MHViewImageVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHViewImageVC") as! MHViewImageVC
        MHViewImageVC.index = self.index
        MHViewImageVC.imageSource = self.imageSource
        MHViewImageVC.ProductDetailsData = self.ProductDetailsData
        self.navigationController?.pushViewController(MHViewImageVC, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ((self.ProductTV.refreshControl?.isRefreshing) != nil){
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {return}
                  self.ProductTV.refreshControl?.endRefreshing()
                self.ProductTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: false)
               }
        }
        
       

        ProductTV.reloadData()
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        self.outOfstockView.isHidden = true
        self.AddtoBagBTN.isUserInteractionEnabled = true
        if isUpdateRecent{
            isUpdateRecent = false
            ProductDetailsSplitApi1()
        }else{
            if self.ProductDetailsData != nil{
                self.logEvent(name: self.ProductDetailsData.productdetails.productName,price: "\(self.ProductDetailsData.productdetails.specialPrice!)")
            }
        }
        if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
            if cartitemsCount == nil || cartitemsCount == 0 {
                cartItemsView.isHidden = true
            }else{
                cartItemsView.isHidden = false
                CartItemLb.text = "\(cartitemsCount)"
            }
        }
        if addReviewBeforeLogin{
            addReviewBeforeLogin = false
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = ProductDetailsData.sizechart.review.addreview.link
            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }
        if ViewReviewBeforeLogin{
            ViewReviewBeforeLogin = false
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = ProductDetailsData.sizechart.review.button.link
            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }
        //        ProductTV.reloadData()
        
    }
    func logEvent(name: String,price: String){
        Analytics.logEvent("Product_Details_Opened", parameters: [ "ProductName" : name,
                                                                   "Price" : price])
    }
    
    @IBAction func searchBTNTapped(_ sender: UIButton) {
        let VC = StoryBoard.login.instantiateViewController(withIdentifier: "searchVC") as! searchVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func AddReviewBTNtapped(_ sender: Any) {
        if UserID == ""{
            addReviewBeforeLogin = true
            let loginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }else{
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = ProductDetailsData.sizechart.review.addreview.link
            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }
        
    }
    @IBAction func ViewReviewBTNtaped(_ sender: Any) {
        if UserID == ""{
            ViewReviewBeforeLogin = true
            let loginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }else{
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = ProductDetailsData.sizechart.review.button.link
            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }
    }
    
    @IBAction func FavBTNtapped(_ sender: Any) {
        if ProductDetailsData != nil{
            AddOrRemovefav()
        }
        
    }
    
    @IBAction func ShowSizeChartBTNtapped(_ sender: Any) {
        FullScreenIcon.isHidden = true
        ViewImageInFullScreenBTN.isHidden = true
        IMGslideShow.backgroundColor = .white
        CloseFullimageIcon.image = UIImage(named: "close")
        fullimageDisplayingView.isHidden = false
        ThumnailView.isHidden = true
        self.imageSource.removeAll()
//        let img = AlamofireSource(urlString: ProductDetailsData.sizechart.image)
//        self.imageSource.append(img!)
        self.ImageSlideSetup(inputSource: self.imageSource)
        self.StringArray.removeAll()
//        self.StringArray.append(ProductDetailsData.sizechart.image)
        self.index = 0
    }
    
    @IBAction func shareBTNtapped(_ sender: Any) {
        if ProductDetailsData != nil{
            shareShow()
        }
        
    }
    func shareShow() {
        //        let linkToShare = "https://mashoproduct.page.link/?pdtid=\(self.productID)&link=\(self.ProductDetailsData.productdetails.webshare!)"
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "thashoapp.page.link"
        components.path = "/"
        
//        let queryItem1 = URLQueryItem(name: "pdtid", value: self.productID)
        let queryItem1 = URLQueryItem(name: "pdtid", value: productID)
        let queryItem2 = URLQueryItem(name: "link", value: self.ProductDetailsData.productdetails.webshare)
        components.queryItems = [queryItem1,queryItem2]
        
        guard let linkParameter = components.url else {return}
        print("sharing Link :\(linkParameter.absoluteString)")
        guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://thashoapp.page.link") else { return }
        // IOS PARAMETERS
        if let bundleID = Bundle.main.bundleIdentifier {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
        }
        shareLink.iOSParameters?.appStoreID = "1616825242"
        // Android PARAMETERS
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.thasho")
        // Config MetaData
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters ()
        shareLink.socialMetaTagParameters?.title = self.ProductDetailsData.productdetails.productName
        //        shareLink.socialMetaTagParameters?.descriptionText = self.ProductDetailsData.productdetails.product_description
        if let imageString = self.ProductDetailsData.ItemImageList[0].imageUrl , let imageURL = URL(string: imageString) {
            shareLink.socialMetaTagParameters?.imageURL = imageURL
        }
        
        guard let longURL = shareLink.url else { return }
        print("The long dynamcLink is :\(longURL)")
        
        shareLink.shorten { (url, warnings, error) in
            if let error = error {
                print("Oh no! got an error :\(error.localizedDescription)")
                return
            }
            
            if let warnings = warnings {
                for warning in warnings {
                    print("FDL warning :\(warning)")
                }
            }
            
            guard let url = url else { return }
            print("Short url :\(url.absoluteString)")
            let vc = UIActivityViewController(activityItems: ["Share \(self.ProductDetailsData.productdetails.productName ?? "")",url], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func CartBTNtapped(_ sender: Any) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
        //        WebViewVC.categoryArray = self.categoryArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
    }
    @IBAction func RescentProductViewall(_ sender: Any) {
        //
       if ProductDetailsData2.recentlyViewed.apiname == "homepagedetails"{
                let navigationStack = self.navigationController?.viewControllers ?? []
                for vc in navigationStack{
                    if let dashboardVC = vc as? MHdashBoardVC{
                        DispatchQueue.main.async {
                            self.navigationController?.popToViewController(dashboardVC, animated: true)
                        }
                    }
                }
            }else if ProductDetailsData2.recentlyViewed.apiname == "getProductsByCatId"{
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                vc.categoryId = "\(ProductDetailsData2.recentlyViewed.api_id ?? 0)"
                vc.SubCategoryId = "sc"
                self.navigationController?.pushViewController(vc, animated: true)
            }else  if ProductDetailsData2.recentlyViewed.apiname == "getCartItems" {
                let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                    CartVC.categoryArray = self.categoryArray
                frompush = false
                self.navigationController?.pushViewController(CartVC, animated: true)
            }else  if ProductDetailsData2.recentlyViewed.apiname == "categorys" {
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else if ProductDetailsData2.recentlyViewed.apiname == "recentlyviewed" {
                let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
                categoryDetailsVC.currentProductId = productID
                categoryDetailsVC.isfromsimilar = false
                frompush = false
                self.navigationController?.pushViewController(categoryDetailsVC, animated: true)
            }
    }
    
    @IBAction func SimilarProductViewallBTNtapped(_ sender: Any) {
//        let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHSimilarProductsVC") as! MHSimilarProductsVC
//        categoryDetailsVC.currentProductId = productID
////        categoryDetailsVC.isfromsimilar = true
//        categoryDetailsVC.api_id = ProductDetailsData2.similarProducts.api_id
//        categoryDetailsVC.apiname = ProductDetailsData2.similarProducts.apiname
////        frompush = false
//        self.navigationController?.pushViewController(categoryDetailsVC, animated: true)
//        //
        if ProductDetailsData2.similarProducts.apiname == "homepagedetails"{
                 let navigationStack = self.navigationController?.viewControllers ?? []
                 for vc in navigationStack{
                     if let dashboardVC = vc as? MHdashBoardVC{
                         DispatchQueue.main.async {
                             self.navigationController?.popToViewController(dashboardVC, animated: true)
                         }
                     }
                 }
             }else if ProductDetailsData2.similarProducts.apiname == "getProductsByCatId"{
                 let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                 vc.categoryId = ProductDetailsData2.similarProducts.api_id
                 vc.SubCategoryId = "sc"
                 self.navigationController?.pushViewController(vc, animated: true)
             }else  if ProductDetailsData2.similarProducts.apiname == "getCartItems" {
                 let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
 //                    CartVC.categoryArray = self.categoryArray
                 frompush = false
                 self.navigationController?.pushViewController(CartVC, animated: true)
             }else  if ProductDetailsData2.similarProducts.apiname == "categorys" {
                 let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                 self.navigationController?.pushViewController(vc, animated: true)
             }else if ProductDetailsData2.similarProducts.apiname == "recentlyviewed" {
                 let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
                 categoryDetailsVC.currentProductId = productID
                 categoryDetailsVC.isfromsimilar = false
                 frompush = false
                 self.navigationController?.pushViewController(categoryDetailsVC, animated: true)
             }
        //
//        let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
//        if let apiId = ProductDetailsData2.similarProducts.api_id{
//        vc.categoryId = apiId
//        }
//        vc.SubCategoryId = "sc"
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - add to cart button
    @IBAction func AddToBag(_ sender: Any) {
        print(ProductDetailsData.type)
        if ProductDetailsData.type != 1{
           
            if selectedsizes != nil {
                print("did selected size",selectedsizes)
                dataDict.removeAll()
            print("top index clicked",topindex)
            
            dataDict[ProductDetailsData.postfields[0].fieldname] =
            ProductDetailsData.postfields[0].options[topindex].sizeId
            SelecteD = true
            print("dict = ",dataDict)
            if SelectedSize != nil {
                dataDict[ProductDetailsData.postfields[0].fieldname] =
                ProductDetailsData.postfields[0].options[topindex].sizeId
                print("Data ready to go ")
                self.AddToCartItems(from: "top")
             }
            }
            else {
                
                let PopupVC = StoryBoard.login.instantiateViewController(withIdentifier: "AddToBagPopupVC") as! AddToBagPopupVC
                PopupVC.postfieldsArray = ProductDetailsData.postfields
                PopupVC.isFreeSize = ProductDetailsData.availableSizes.count == 0 ? true : false
                PopupVC.type = ProductDetailsData.type
                PopupVC.productID = productID
                PopupVC.presentingvc =  self
               // PopupVC.delegate = self
                self.present(PopupVC, animated: false, completion: nil)
            }
          
        }else{
            self.AnimationImage.isHidden = false
            self.AddToCartItems(from: "nothing")
            
        }
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.ProductDetailsData = nil
        self.ProductDetailsData2 = nil
        MHavPlayerLayer?.removeFromSuperlayer()
        MHavPlayerLayer = nil
        MHplayer = nil
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
        if frompush {
          
            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
        else {
            Alamofire.SessionManager.default.session.getTasksWithCompletionHandler({ dataTasks, uploadTasks, downloadTasks in
                        dataTasks.forEach { $0.cancel() }
                        uploadTasks.forEach { $0.cancel() }
                        downloadTasks.forEach { $0.cancel() }
                    })
            self.navigationController?.popViewController(animated: true)
            DispatchQueue.main.async { [weak self] in
                  self?.ProductTV.refreshControl?.endRefreshing()
               }
        }
//        }
    }
    
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        if MHplayer != nil {
            
//            NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: MHplayer?.currentItem)
//            MHavPlayerLayer = nil
//            MHplayer = nil
//
//        }
//    }
   
    
    @IBAction func playbutton(_ sender: UIButton) {
        print("playing")
        
    }
    
    @IBAction func ReadMoreOrReadLessBTNtapped(_ sender: UIButton) {
        
        //        let cell = ProductTV.cellForRow(at: IndexPath(row: 0, section: 6)) as! MHReadMoreTVC
        //        cell.layoutIfNeeded()
        //        cell.layoutSubviews()
        if isReadMoreSelected{
            isReadMoreSelected = false
        }else{
            isReadMoreSelected = true
        }
        //        if cell.ReadMoreOrReadLessLB.text == "Read More"{
        //            cell.ReadMoreOrReadLessLB.text = "Read Less"
        //            cell.DescriptionLb.isHidden = false
        //            cell.DescTitleLB.isHidden = false
        //            cell.DisclaimerLb.isHidden = false
        //            cell.DisclaimerTitleLb.isHidden = false
        //        }else{
        //            cell.ReadMoreOrReadLessLB.text = "Read More"
        //            cell.DescriptionLb.isHidden = true
        //            cell.DescTitleLB.isHidden = true
        //            cell.DisclaimerLb.isHidden = true
        //            cell.DisclaimerTitleLb.isHidden = true
        //        }
        offset = ProductTV.contentOffset.y
        ProductTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: false)
        ProductTV.scrollToRow(at: IndexPath(row: 0, section: 5), at: UITableView.ScrollPosition.top, animated: true)
        ProductTV.reloadData()
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        offset = ProductTV.contentOffset.y
      //  print("offset",offset)
        let percentage = offset / 351.5
     //   print("351.5 - offset",351.5 - offset)
//        ProductImageViewHeightConstraint.constant = 351.5 - offset
        if 351.5 - ProductTV.contentOffset.y >= 0{
//            rateViewHeightConstraint.constant = 351.5 - offset
//            smallProductWidthConstraint.constant = offset / 5.44 //5.44 = 245 / 45
//            ProductNameView.backgroundColor = UIColor(red: 247/255, green: 245/255, blue: 247/255, alpha: 1)
        }else{
//            rateViewHeightConstraint.constant = 0
//            smallProductWidthConstraint.constant = 45
//            ProductNameView.backgroundColor = UIColor.white
        }
        //        if ProductTV.contentOffset.y >= 45{
        //            smallProductWidthConstraint.constant = 45
        //        }else{
        //            smallProductWidthConstraint.constant = 245 - offset
        //        }
        
        let percentage1 = offset / 45
        //        print("percentage1",percentage1)
        //        smallProductWidthConstraint.constant = percentage1 + 45
//        SmallProductImage.alpha = percentage
    
        ProductImageView.alpha = 1 - percentage
        
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        
        dropDown.show()
        
    }
    
    @IBAction func ChangeSizeBTNtapped(_ sender: Any) {
        
        ProductTV.scrollToRow(at: IndexPath(row: 0, section: 1), at: .none, animated: true)
        
    }
    var i = 0
    @IBAction func performTapped(_ sender: UITapGestureRecognizer) {
        if i == 0{
            i = 1
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.ThumnailView.transform = CGAffineTransform(translationX: 0, y: self.ThumnailView.frame.height)
            }, completion: nil)
        }else{
            i = 0
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.ThumnailView.transform = .identity
            }, completion: nil)
        }
    }
    //MARK: - Pull refresh
    private func initializeView(){
        self.refreshControlz.endRefreshing()
            refreshControlz.tintColor = .MHGold
            refreshControlz.addTarget(self,
                                     action: #selector(refreshData(sender:)),
                                     for: .valueChanged)
        self.ProductTV.refreshControl = refreshControlz
        self.ProductTV.addSubview(refreshControlz)

    }

        @objc func refreshData(sender: UIRefreshControl) {
            Recallsamepage()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {return}
                  self.ProductTV.refreshControl?.endRefreshing()
               }
        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         
        }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let _cell = cell as? MHProductImageTVC {
            let cellRect = tableView.rectForRow(at: indexPath)
            let completelyVisible = tableView.bounds.contains(cellRect)
            print("didEndDisplaying completelyVisible",completelyVisible)
            if !completelyVisible {
                MHavPlayerLayer?.removeFromSuperlayer()
                MHavPlayerLayer = nil
                MHplayer = nil
            }
        }
    }



    
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
//        DropDown.appearance().backgroundColor = #colorLiteral(red: 0.8306156993, green: 0.6888336539, blue: 0.2119885683, alpha: 1)
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                    //                    CartVC.categoryArray = self.categoryArray
                    
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
    
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}
var reloadStatus = 0
extension MHProductDetailsVC: UITableViewDataSource,UITableViewDelegate,didSelectColorOrSizeDelegate,animationDelegate,FullImageDisplayingDelegate,reloadDelegate{
   
    
    
    func reloadTable() {
//        self.tableView(ProductTV, heightForRowAt: IndexPath(row: 0, section: 0))
//        ProductTV.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)//Data()//Sections(IndexSet(integersIn: 0...0), with: UITableView.RowAnimation.none)
    }
    //MARK: - Full screen view (image and video )
    func openFullSCreenImage(index: Int, collection: String) {
        var type = -1
        if collection == "MainProductImageCV" {
            type = self.ProductDetailsData!.ItemImageList[index].type
        }else {
            type = self.arrSubOverView[index].type
        }
        if type == 2{
            var videoUrl = ""
            if collection == "MainProductImageCV" {
                videoUrl =  self.ProductDetailsData!.ItemImageList[index].imageUrl ?? ""
            }else {
                videoUrl =  self.arrSubOverView[index].imageUrl ?? ""
            }
            let player = AVPlayer(url: URL(string: videoUrl)!)
            let vc = AVPlayerViewController()
            vc.player = player
            present(vc, animated: true) {
                vc.player?.play()
            }
              
        }else{
            FullScreenIcon.isHidden = true
            ViewImageInFullScreenBTN.isHidden = true
            self.imageSource.removeAll()
                if collection == "MainProductImageCV" {
                    for image in self.ProductDetailsData!.ItemImageList {
                        if image.type == 1{
                            let img = AlamofireSource(urlString: image.imageUrl)
                            self.imageSource.append(img!)
                        }
                    }
                }else {
                    for image in self.ProductDetailsData!.ItemImageList {
                        if image.type == 1{
                            let img = AlamofireSource(urlString: image.imageUrl)
                            self.imageSource.append(img!)
                        }
                    }
                }
            self.ImageSlideSetup(inputSource: self.imageSource)
            IMGslideShow.setCurrentPage(index, animated: false)
            CloseFullimageIcon.image = UIImage(named: "close (2)")
            IMGslideShow.backgroundColor = .black
            fullimageDisplayingView.isHidden = false
            ThumnailView.isHidden = false
            self.StringArray.removeAll()
              DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                                    self.ThumnailView.transform = CGAffineTransform(translationX: 0, y: self.ThumnailView.frame.height)
                                }, completion: nil)
                            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 17
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return 1
        }else  if section == 2{
            return 1
        }else if section == 3{
            return 1
        }else if section == 4{
            return 1
        }else if section == 5{
            return 1
        }else if section == 6{
            if ProductDetailsData != nil{
                return ProductDetailsData.specification.count
            }
                return 1
        }else if section == 7{
            if ProductDetailsData != nil{
                return self.ProductDetailsData.productdetails.product_description.count
            }
            return 1
        }else if section == 8{if ProductDetailsData != nil{
            return self.ProductDetailsData.productdetails.Disclaimer.count
        }
        return 1
        
        }else if section == 9{
            return 1
        }else if section == 10{
            return 1
        }else if section == 11{
            if ProductDetailsData2 != nil{
//                return 1
                return ProductDetailsData2.review.reviews.count
            }
            return 1
        }else if section == 12{
            return 1
        }else if section == 13{
            return 1
        }else if section == 14{
            if ProductDetailsData2 != nil{
                 print("u might be count",ProductDetailsData2.suggestionList.body.count)
                return ProductDetailsData2.suggestionList.body.count
            }
            return 1
        }else if section == 15{
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductImageTVC", for: indexPath) as! MHProductImageTVC
            if ProductDetailsData != nil{
                cell.overviewarray = self.ProductDetailsData.ItemImageList
                cell.arrSubOverView = arrSubOverView
                
                cell.ProductNameLBL.text =  self.ProductDetailsData.productdetails.productName
                cell.ProductPriceLBL.text = self.ProductDetailsData.productdetails.specialPrice
                cell.ProductStrikedPriceLBL.attributedText = "\(self.ProductDetailsData.productdetails.price!)".updateStrikeThroughFont(.red)
                cell.OfferPercentageLBL.text = "\(self.ProductDetailsData.productdetails.offerPercentage!)% Off"
                cell.delegate = self
                if self.ProductDetailsData.ItemImageList[0].resolution == 2 {
                    // 1080x192
                    let ratio:CGFloat = 1080/1920
                    let width = (tableView.frame.width-12)-(tableView.frame.width*0.25)
                    let height = width/ratio
                   // print("subproduct 1080x1440",CGSize(width: width, height: height))
                    cell.nslPreviewHeight.constant = height
                   
                } else {
                    // 1080x1440
                    let ratio:CGFloat = 1080/1440
                    let width = (tableView.frame.width-12)-(tableView.frame.width*0.25)
                    let height = width/ratio
                   // print("subproduct 1080x1440",CGSize(width: width, height: height))
                    cell.nslPreviewHeight.constant = height
                }
                
               
                DispatchQueue.main.async {
                    cell.layoutIfNeeded()
                    cell.MainProductImageCV.reloadData()
                    cell.SubProductImageCV.reloadData()
                    cell.loadDelegate = self
                }
               
                
                //MARK: - Resolution Handling IMAGE/VIDEO
//                if self.ProductDetailsData.ItemImageList[0].type == 2  {
                    
//                    if self.ProductDetailsData.ItemImageList[0].resolution == 2{
//                        let newConstraint2 = cell.MainImageViewAspectRatioConstraint.constraintWithMultiplier(1080/1920)
//                        cell.MainImageView.removeConstraint(cell.MainImageViewAspectRatioConstraint)
//                        cell.MainImageView.addConstraint(newConstraint2)
//                        cell.MainImageView.layoutIfNeeded()
//                        cell.MainImageViewAspectRatioConstraint = newConstraint2
//                    }
//                    else {
//                        let newConstraint2 = cell.MainImageViewAspectRatioConstraint.constraintWithMultiplier(1080/1440)
//                        cell.MainImageView.removeConstraint(cell.MainImageViewAspectRatioConstraint)
//                        cell.MainImageView.addConstraint(newConstraint2)
//                        cell.MainImageView.layoutIfNeeded()
//                        cell.MainImageViewAspectRatioConstraint = newConstraint2
//                    }
//
//                }else {
//                    let newConstraint2 = cell.MainImageViewAspectRatioConstraint.constraintWithMultiplier(8/11)
//                    cell.MainImageView.removeConstraint(cell.MainImageViewAspectRatioConstraint)
//                    cell.MainImageView.addConstraint(newConstraint2)
//                    cell.MainImageView.layoutIfNeeded()
//                    cell.MainImageViewAspectRatioConstraint = newConstraint2
//                }
                
                if self.ProductDetailsData.productdetails.premiumtags.status == 1{
                    cell.PremiumBGvew.backgroundColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.premiumtags.background!)ff")
                    cell.PremiumBGvew.layer.borderWidth = 1
                    cell.PremiumBGvew.layer.borderColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.premiumtags.border!)ff")?.cgColor
                    cell.PremiumTxt.text = self.ProductDetailsData.productdetails.premiumtags.text ?? ""
                    cell.PremiumTxt.textColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.premiumtags.color!)ff")
                    cell.PremiumImg.kf.setImage(with: URL(string: self.ProductDetailsData.productdetails.premiumtags.image))
                    cell.PremiumBGvew.isHidden = false
                    cell.PremiumVewHeightConstraint.constant = 105
                }else{
                    cell.PremiumBGvew.isHidden = true
                    //MARK: - PREMIUM TAGS
                    cell.PremiumVewHeightConstraint.constant = 0
                }
                
                
                
                cell.ImportedView.isHidden = true
                
                DispatchQueue.main.async{
                    if self.ProductDetailsData.productdetails.imagelefttext.status == 1{
                        cell.ImportedView.backgroundColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.imagelefttext.background!)ff")
                        cell.ImportedView.layer.borderWidth = 1
                        cell.ImportedView.layer.borderColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.imagelefttext.border!)ff")?.cgColor
                        cell.Imported_Lb.text = self.ProductDetailsData.productdetails.imagelefttext.text
                        cell.Imported_Lb.textColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.imagelefttext.color!)ff")
//                        cell.Imported_Lb.textColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.imagelefttext.text)ff")?.cgColor
                            cell.ImportedView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
                            cell.ImportedViewLeading.constant = -(cell.ImportedView.frame.height / 2) + 14
                            cell.ImportedView.clipsToBounds = true
                            cell.ImportedView.layer.cornerRadius = 10
                            cell.ImportedView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {

                            cell.ImportedView.isHidden = false
                        }
                        
                    }else{
                        cell.ImportedView.isHidden = true
                    }
                    if self.ProductDetailsData.productdetails.imagebottomtext.status == 1{
                        cell.inStockView.backgroundColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.imagebottomtext.background!)ff")
                        cell.inStockView.layer.borderWidth = 1
                        cell.inStockView.layer.borderColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.imagebottomtext.border!)ff")?.cgColor
                        cell.inStockLb.text = self.ProductDetailsData.productdetails.imagebottomtext.text
                        cell.inStockimg.kf.setImage(with: URL(string: "\(self.ProductDetailsData.productdetails.imagebottomtext.image!)"))
                        cell.inStockView.isHidden = false
                    }else{
                        cell.inStockView.isHidden = true
                    }
                    if self.ProductDetailsData.productdetails.deal?.status == 1{
                        cell.dealOftheDayView.isHidden = false
                        cell.dealOftheDayimg.isHidden = false
                        cell.dealOftheDayimg.kf.setImage(with: URL(string: "\(self.ProductDetailsData.productdetails.deal!.image!)"))
                    }else{
                        cell.dealOftheDayView.isHidden = true
                        cell.dealOftheDayimg.image = nil
                    }
                }
                if self.ProductDetailsData.productdetails.favorite == 1{
                    cell.FavImg.image = UIImage(named: "favSelected")
                }else{
                    cell.FavImg.image = UIImage(named: "favUnselected")
                }
                
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: indexPath) as! MHProductRateTVC
            if ProductDetailsData != nil{
                
                if ProductDetailsData.productdetails.mashoAssured == "1"{
                    cell.AssuredLogo.isHidden = false
                }else{
                    cell.AssuredLogo.isHidden = true
                }
                //cell.ProductTitleLB.text = self.ProductDetailsData.productdetails.productTitle
                cell.RatingView.rating = Double(self.ProductDetailsData.productdetails.rating)!
                cell.ToyalRatingLb.text = "(\(self.ProductDetailsData.productdetails.ratingCount!))"
                cell.ReviewsCountLb.text = "\(self.ProductDetailsData.productdetails.reviews!) Reviews"
//                cell.SelfiesCountLb.text = "\(self.ProductDetailsData.productdetails.selfies!) Selfies"
//                cell.deliveryModeLb.text = ProductDetailsData.productdetails.deliveryMode
                cell.ExpectedDeliveryLb.text = ProductDetailsData.productdetails.expected_delivery
                cell.paymentModeLb.text = ProductDetailsData.productdetails.paymentMode
                cell.easyReturnLb.text = ProductDetailsData.productdetails.returnPolicy
                cell.shippingLb.text = ProductDetailsData.productdetails.higilight_text
            }
            
            return cell
        }else if indexPath.section == 2{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductSizeTVC", for: indexPath) as! MHProductSizeTVC
            cell.ProductSizeBGView.dropShadow()
            if ProductDetailsData != nil{
                cell.ProductDetailsDataqq = self.ProductDetailsData
//                cell.selectedSize = self.productID
                cell.selectedSize = self.ProductDetailsData.selectedproductid
                cell.SizeCV.register(MHProductSizenewCVC.self, forCellWithReuseIdentifier: "MHProductSizenewCVC")
               
                if self.ProductDetailsData.availableSizes.count == 0{
                    cell.isFreeSize = true
                    // self.ProductDetailsData.selectedproductid
                }else{
                    cell.isFreeSize = false
                    cell.SizeArray = self.ProductDetailsData.availableSizes
                }
                cell.SizeCV.reloadData()
                cell.delegate = self
            }
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductColorsTVC", for: indexPath) as! MHProductColorsTVC
            
            if ProductDetailsData != nil{
//                cell.selectedColor = self.productID
                cell.selectedColor = productID
                cell.ColorArray = self.ProductDetailsData.availableColors
                if ProductDetailsData.availableSizes.count == 0{
                    cell.SizeChartImage.isHidden = false
                    cell.ShowSizeChartBTN.isHidden = false
                }else{
                    cell.SizeChartImage.isHidden = true
                    cell.ShowSizeChartBTN.isHidden = true
                }
                cell.ColorCV.reloadData()
                cell.delegate = self
            }
            return cell
        }else  if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductSizeChartTVC", for: indexPath) as! MHProductSizeChartTVC
            guard ProductDetailsData != nil else {return cell}
            cell.B1IMG.kf.setImage(with: URL(string: ProductDetailsData.size_shipping.shipping_return.image))
            cell.B1LBL.text = ProductDetailsData.size_shipping.shipping_return.text
            if ProductDetailsData.size_shipping.shipping_return.status == 0 {
                cell.B1View.isHidden = true
            } else if ProductDetailsData.size_shipping.sizechart.status == 0 {
                cell.B2View.isHidden = true
            }
            cell.B2IMG.kf.setImage(with: URL(string: ProductDetailsData.size_shipping.sizechart.image))
            cell.B2LBL.text = ProductDetailsData.size_shipping.sizechart.text
                return cell
        }else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "specCell", for: indexPath)
            return cell
        }else if indexPath.section == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductSpecTVC", for: indexPath) as! MHProductSpecTVC
            DispatchQueue.main.async {
                if indexPath.row % 2 == 0{
                    cell.BGView.backgroundColor = UIColor.clear
                }else{
                    cell.BGView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            }
            
            }
            if ProductDetailsData != nil{
                cell.specCategoryLb.text = self.ProductDetailsData.specification[indexPath.row].property_name_english
                cell.specValueLb.text = self.ProductDetailsData.specification[indexPath.row].value_name_eng
                if self.ProductDetailsData.specification[indexPath.row].property_name_english == "Size" {
                    if ProductDetailsData.availableSizes.count != 0{
                        cell.ChangeSizeBTN.isHidden = false
                    }else{
                        cell.ChangeSizeBTN.isHidden = true
                    }
                }else{
                    cell.ChangeSizeBTN.isHidden = true
                }
                
            }
            return cell
        }else if indexPath.section == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductdescriptionTVC", for: indexPath) as! MHProductdescriptionTVC
         //cell.descriptionLBL.attributedText = htmlText.htmlToAttributedString
            if ProductDetailsData != nil{
                cell.descriptionLBL.text = self.ProductDetailsData.productdetails.product_description[indexPath.row]
                
            }
            
            if indexPath.row == 0 {
                cell.TitleLBL.isHidden = false
                cell.TitleLBL.text = "Description"
                cell.graybottom.constant = 0
                cell.graytop.constant = 15
                cell.grayview.layer.cornerRadius = 15
                cell.grayview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            }else if indexPath.row == self.ProductDetailsData.productdetails.product_description.count-1 && self.ProductDetailsData.productdetails.Disclaimer.count == 0 {
                cell.TitleLBL.isHidden = true
                cell.graytop.constant = 0
                cell.grayview.layer.cornerRadius = 15
                cell.graybottom.constant = 10
                cell.grayview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
            }else{
                cell.TitleLBL.isHidden = true
                cell.graytop.constant = 0
                cell.grayview.layer.cornerRadius = 0
            }
            
            return cell
        
    }else if indexPath.section == 8{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductdescriptionTVC", for: indexPath) as! MHProductdescriptionTVC
        if ProductDetailsData != nil{
            cell.descriptionLBL.text = self.ProductDetailsData.productdetails.Disclaimer[indexPath.row]
            
        }
        if indexPath.row == 0  {
            cell.TitleLBL.isHidden = false
            cell.TitleLBL.text = "Disclaimer"
            cell.graybottom.constant = 0
            print("desclimer",self.ProductDetailsData?.productdetails)
            if self.ProductDetailsData?.productdetails.product_description.count == 0 {
                cell.graytop.constant = 15
                cell.grayview.layer.cornerRadius = 15
                cell.grayview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            } else {
                if self.ProductDetailsData?.productdetails.Disclaimer.count == 1 {
                    cell.grayview.layer.cornerRadius = 15
                    cell.grayview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
                } else {
                    cell.grayview.layer.cornerRadius = 0
                }
                cell.graytop.constant = 0
            }
            
        }else if indexPath.row == self.ProductDetailsData.productdetails.Disclaimer.count-1  {
            cell.TitleLBL.isHidden = true
            cell.graytop.constant = 0
            cell.grayview.layer.cornerRadius = 15
            cell.graybottom.constant = 10
            cell.grayview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        }else{
            cell.TitleLBL.isHidden = true
            cell.graytop.constant = 0
            cell.grayview.layer.cornerRadius = 0
        }
        return cell
    
}else if indexPath.section == 9{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductbannerTVC", for: indexPath) as! MHProductbannerTVC
    if ProductDetailsData != nil{
   
    
    
    cell.ProductImage.kf.setImage(with: URL(string: ProductDetailsData.checkout_delivery.image))
    }
        
 
        return cell
    }else if indexPath.section == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductRatingTVC", for: indexPath) as! MHProductRatingTVC
            if ProductDetailsData2 != nil{
                cell.MainHeadingLb.text = ProductDetailsData2.review.mainheading
                cell.RatingCount.text = ProductDetailsData2.review.playstorerating
                cell.SecondTextLb.text = ProductDetailsData2.review.platstoreratingtxt
                cell.ViewAllLB.text = ProductDetailsData2.review.button.text
                cell.ViewAllLB.textColor = UIColor(hex: "\(ProductDetailsData2.review.button.colour!)ff")
                cell.ViewAllView.backgroundColor = UIColor(hex: "\(ProductDetailsData2.review.button.background!)ff")
                cell.AddReviewLB.text = ProductDetailsData2.review.addreview.text
                cell.AddReviewLB.textColor = UIColor(hex: "\(ProductDetailsData2.review.addreview.color!)ff")
            }
            return cell
        }else if indexPath.section == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductReviewsTVC", for: indexPath) as! MHProductReviewsTVC
            if ProductDetailsData2 != nil{
                cell.ReviewerName.text = ProductDetailsData2.review.reviews[indexPath.row].name
                cell.ReviewerImage.kf.setImage(with: URL(string: ProductDetailsData2.review.reviews[indexPath.row].image))
                cell.Rating.rating = Double(ProductDetailsData2.review.reviews[indexPath.row].rating)
                cell.ContentLb.text = ProductDetailsData2.review.reviews[indexPath.row].review
            }
            return cell
        }else if indexPath.section == 12{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHProductzTVC", for: indexPath) as! MHProductzTVC
//            DispatchQueue.main.async {
//                cell.ViewAllView.layer.cornerRadius = cell.ViewAllView.frame.height / 2
//            }
            if ProductDetailsData2 != nil{
                cell.ViewAllLb.text = self.ProductDetailsData2.similarProducts.button
                cell.TitleLb.text = self.ProductDetailsData2.similarProducts.heading
                cell.ProductArray = self.ProductDetailsData2.similarProducts.body
                cell.navnCtler = self.navigationController
                //                cell.categoryArray = self.categoryArray
                cell.ProductCV.reloadData()
            }
            return cell
        }else if indexPath.section == 13{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHTitleTVC", for: indexPath) as! MHTitleTVC
            if ProductDetailsData2 != nil{
                cell.TitleLb.text = self.ProductDetailsData2.suggestionList.heading
            }
            return cell
        }else if indexPath.section == 14{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHInterestsTVC", for: indexPath) as! MHInterestsTVC
            if ProductDetailsData2 != nil{
                cell.CategoryNameLb.text = self.ProductDetailsData2.suggestionList.body[indexPath.row].ListName
//                cell.ViewAllLb.text = self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].buttonval
                if self.ProductDetailsData2.suggestionList.body[indexPath.row].productList.count >= 3{
                    cell.FirstImage.kf.setImage(with: URL(string: "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].Productimage!)"))
                    cell.SecondImage.kf.setImage(with: URL(string: "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[1].Productimage!)"))
                    cell.ThirdImage.kf.setImage(with: URL(string: "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[2].Productimage!)"))
                }else if self.ProductDetailsData2.suggestionList.body[indexPath.row].productList.count == 2{
                    cell.FirstImage.kf.setImage(with: URL(string: "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].Productimage!)"))
                    cell.SecondImage.kf.setImage(with: URL(string: "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[1].Productimage!)"))
                }else if self.ProductDetailsData2.suggestionList.body[indexPath.row].productList.count == 1{
                    cell.FirstImage.kf.setImage(with: URL(string: "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].Productimage!)"))
                } else{
                    
                }
                /*
                DispatchQueue.main.async{
                    if indexPath.row == self.ProductDetailsData2.suggestionList.body.count - 1{
                        cell.LineView.isHidden = true
                    }else{
                        cell.LineView.isHidden = false
                    }
                }
                 */

            }
            return cell
        }else if indexPath.section == 15{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsBottomBannerTVC", for: indexPath) as! ProductDetailsBottomBannerTVC
            if ProductDetailsData2 != nil{
            cell.banners = self.ProductDetailsData2.bannerbottom
            cell.navCtler = self.navigationController
            cell.BannerView.reloadData()
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHRescentProductzTVC", for: indexPath) as! MHRescentProductzTVC
//            DispatchQueue.main.async {
//                cell.ViewAllView.layer.cornerRadius = cell.ViewAllView.frame.height / 2
//            }
            if ProductDetailsData2 != nil{
                cell.ViewAllLb.text = self.ProductDetailsData2.recentlyViewed.button
                cell.TitleLb.text = self.ProductDetailsData2.recentlyViewed.heading
                cell.ProductArray = self.ProductDetailsData2.recentlyViewed.body
                cell.navnCtler = self.navigationController
                //                cell.categoryArray = self.categoryArray
                cell.ProductCV.reloadData()
            }
            return cell
        }
    }
    
  
    
    
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return UITableView.automaticDimension //height + 96
        }else if indexPath.section == 1{
            //MHProductrateTVC
            return 150
            
        }else if indexPath.section == 2{
            
            if ProductDetailsData != nil{
                if ProductDetailsData.availableSizes.count == 0{
                    return ((((tableView.frame.width - 80) / 7) + 10)  * 1) + 70
                }else{
                    var CVHeight : CGFloat!
                    if ProductDetailsData.availableSizes.count % 6 == 0{
                        CVHeight = CGFloat((ProductDetailsData.availableSizes.count / 6))
                    }else{
                        CVHeight = CGFloat(((ProductDetailsData.availableSizes.count / 6) + 1))
                    }
                    return ((((tableView.frame.width - 80) / 7) + 10)  * CVHeight) + 70
                }
                
            }else{
                return 0
            }
        }else if indexPath.section == 3{
            if ProductDetailsData != nil{
                if ProductDetailsData.availableColors.count == 0{
                    return 0
                }else{
                    var CVHeight : CGFloat!
                    if ProductDetailsData.availableColors.count % 6 == 0{
                        CVHeight = CGFloat((ProductDetailsData.availableColors.count / 6))
                    }else{
                        CVHeight = CGFloat(((ProductDetailsData.availableColors.count / 6) + 1))
                    }
                    return (((((tableView.frame.width - 80) / 7) * 1.5) + 5)  * CVHeight) + 70
                }
            }else{
                return 0
            }
        }else  if indexPath.section == 4{
            if ProductDetailsData != nil{
            if ProductDetailsData.size_shipping.shipping_return.status == 0 &&   ProductDetailsData.size_shipping.sizechart.status == 0 {
                return 0
            }
                else {
                    return 75
                    }
            }
            else {
                return 75
            }
        }else  if indexPath.section == 5{
            if ProductDetailsData != nil{
                return 45
            }else{
                return 0
            }
        }else if indexPath.section == 6{
            if ProductDetailsData != nil{
                return 30
            }else{
                return 0
            }
        }
//        if self.ProductDetailsData.checkout_delivery.status == 1{
        else if indexPath.section == 9{
            if ProductDetailsData != nil{
                if self.ProductDetailsData.checkout_delivery.status == 1{
                return UITableView.automaticDimension
                    
                }
                else {
                    return 0
                }
            }
            else
            {
                return 0
            }
        }
            else if indexPath.section == 7{
            if ProductDetailsData != nil{
                return UITableView.automaticDimension
            }else{
                return 0
            }
        }else if indexPath.section == 8{
            if ProductDetailsData != nil{
                return UITableView.automaticDimension
            }else{
                return 0
            }
        }else if indexPath.section == 10{
            if ProductDetailsData2 != nil{
                
                //MARK: - CRASH (1 TEST FLIGHT)
                guard ProductDetailsData2 != nil else { return 0 }
                if (ProductDetailsData2.review.status == 0){
                    return 0
                }
                else {
                    return UITableView.automaticDimension
                }
                
            }else{
                return 0
            }
        }else if indexPath.section == 11{
            if indexPath.row > 3{
                return 0
            }else{
                if ProductDetailsData2
                    != nil{
                    if (ProductDetailsData2.review.status == 0){
                        return 0
                    }
                    else{
                        return UITableView.automaticDimension
                    }
                   
                    
                }else{
                    return 0
                }
            }
        }else if indexPath.section == 12{
            //MARK: -   recently viewed / similar view height
            if ProductDetailsData != nil{
                let TVwidth = (tableView.frame.width - 40)
                let CVWidth = TVwidth / 2.15
                let CVheight = (CVWidth * (10 / 8)) + 80
                return (CVheight * 2) + 110
            }else{
                return 0
            }
            
        }else if indexPath.section == 13{
            if ProductDetailsData != nil{
                return 45
            }else{
                return 0
            }
        }else if indexPath.section == 14{
            if ProductDetailsData != nil{
                return 80
//                return UITableView.automaticDimension
            }else{
                return 0
            }
        }else if indexPath.section == 15{
            return 220
        }else{
          //MARK: -   recently viewed
            if ProductDetailsData2 != nil{
                let TVwidth = (tableView.frame.width - 40)
                let CVWidth = TVwidth / 2.15
                let CVheight = (CVWidth * (10 / 8)) + 80
                if ProductDetailsData2.recentlyViewed.body.count == 1 || ProductDetailsData2.recentlyViewed.body.count == 2{
                    return (CVheight * 1) + 85
                }else if ProductDetailsData2.recentlyViewed.body.count == 3 || ProductDetailsData2.recentlyViewed.body.count == 4{
                    return (CVheight * 2) + 110
                }else if ProductDetailsData2.recentlyViewed.body.count == 0{
                    return 0
                }
            else if ProductDetailsData2.recentlyViewed.body.count > 4{
                return (CVheight * 2) + 110
            }
                else {
                    return 0
                }
            }else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 14{
            if ProductDetailsData != nil{
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
//                vc.categoryId = "\(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].api_id!)"
                if self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].api_id != nil{
                    vc.categoryId = String(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].api_id)
                }
                else {
                    vc.categoryId = String(self.ProductDetailsData2.suggestionList.body[indexPath.row].productList[0].apiid)
                }
                vc.SubCategoryId = "sc"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    func didSelectColorOrSize(productId: String,isfromcolor:Bool){
        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//        MHProductDetailsVC.productID = productId
        productID = productId
        selectedsizes = productId
        Recallsamepage()
        //self.navigationController?.pushViewController(MHProductDetailsVC, animated: false)
    }
    func Recallsamepage(){
        refreshforplayer = true
        MHavPlayerLayer?.removeFromSuperlayer()
        MHavPlayerLayer = nil
        MHplayer = nil
        print("product id originals --",productID)
        print("product id size or color --",selectedsizes)
        self.ProductTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: false)
        ProductDetailsSplitApi1()
    }
    func Animation(from: String) {
        
    }
    
   
}

extension MHProductDetailsVC{
    func ProductDetails(){
        if isFrom == .other{
            self.EmptyView.isHidden = false
        }else{
            self.EmptyView.isHidden = true
        }
        let parameters = ["action":"getProductDetails",
                          "userId":"\(UserID)",
                          "guest_Id": guest_id,
                          "productId": productID] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                
                print("ProductDetails success")
                print(response["data"]!)
                if let data = response["data"] as? [String:Any]{
                    self.ProductDetailsData = MHProductDetailsModel(fromData: data)
                }
                
                self.logEvent(name: self.ProductDetailsData.productdetails.productName ?? "",price: "\(self.ProductDetailsData.productdetails.specialPrice!)")
//                self.ProductNameLb.text = self.ProductDetailsData.productdetails.productName
//                if self.ProductDetailsData.productdetails.favorite == 1{
//                    self.FavImageView.image = UIImage(named: "favSelected")
//                }else{
//                    self.FavImageView.image = UIImage(named: "favUnselected")
//                }
                self.AnimationImage.kf.setImage(with: URL(string: self.ProductDetailsData.ItemImageList[0].imageUrl))
//                self.SmallProductImage.kf.setImage(with: URL(string: self.ProductDetailsData.ItemImageList[0].imageUrl))
//                self.SpecialPriceLB.text = "\(self.ProductDetailsData.productdetails.specialPrice!)"
//                self.OldPriceLb.attributedText = "\(self.ProductDetailsData.productdetails.price!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//                self.PercentageLb.text = "\(self.ProductDetailsData.productdetails.offerPercentage ??  0) % OFF"
//                self.PercentageLb.textColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.offercolor!)ff")
//                self.PercentageView.backgroundColor = UIColor(hex: "\(self.ProductDetailsData.productdetails.offerbackground!)ff")
                self.BottomPiceLb.text = "\(self.ProductDetailsData.productdetails.specialPrice!)"
                self.BottomStrikePrice.attributedText = "\(self.ProductDetailsData.productdetails.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
                self.ProductTV.reloadData()
                self.ProductImageCV.reloadData()
                self.ThumnailCV.reloadData()
                self.EmptyView.isHidden = true
                if self.ProductDetailsData.productdetails.avalablestock == 0{
                    self.outOfstockView.isHidden = false
                    self.AddtoBagBTN.isUserInteractionEnabled = false
                }else{
                    self.outOfstockView.isHidden = true
                    self.AddtoBagBTN.isUserInteractionEnabled = true
                }
                
                if self.isFrom == .other{
                    self.EmptyView.isHidden = true
                }
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    
    func ProductDetailsSplitApi1(){
        //https://www.masho.com/api_v4?userId&productId=17667&guest_Id=2305339&action=viewpagesplitout_one
        if selectedsizes != nil{
            productIDForAPI = selectedsizes!
        }
        else {
            productIDForAPI = productID
//            productIDForAPI = "19185"
        }

            self.EmptyView.isHidden = false

        let parameters = ["action":"viewpagesplitout_one",
                          "userId":"\(UserID)",
                          "guest_Id": guest_id,
                          "productId": productIDForAPI] as [String : Any]
        
//        let parameters = ["action":"viewpagesplitout_one",
//                          "userId":"\(UserID)",
//                          "guest_Id": guest_id,
//                          "productId": "19185"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
           
            
            print(response)
            self.view.hideSkeleton()
            switch status {
            case .noNetwork:
                print("network error")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
               
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.ProductDetailsSplitApi2()
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String:Any]{
                    self.ProductDetailsData = MHProductDetailsModel(fromData: data)
                }
                self.logEvent(name: self.ProductDetailsData.productdetails.productName ?? "",price: "\(self.ProductDetailsData.productdetails.specialPrice!)")
                self.AnimationImage.kf.setImage(with: URL(string: self.ProductDetailsData.ItemImageList[0].imageUrl))
                self.BottomPiceLb.text = "\(self.ProductDetailsData.productdetails.specialPrice!)"
                self.BottomStrikePrice.attributedText = "\(self.ProductDetailsData.productdetails.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
//                self.ProductTV.reloadData()
//                self.ProductImageCV.reloadData()
//
//                self.ThumnailCV.reloadData()
                
//                for image in self.ProductDetailsData.ItemImageList{
//                    if image.type == 1{
//                        self.thumImgArray.append(image)
//                    }
//                }
                if self.ProductDetailsData.productdetails.avalablestock == 0{
                    self.outOfstockView.isHidden = false
                    self.AddtoBagBTN.isUserInteractionEnabled = false
                }else{
                    self.outOfstockView.isHidden = true
                    self.AddtoBagBTN.isUserInteractionEnabled = true
                }
                
                if self.isFrom == .other{
                    self.EmptyView.isHidden = true
                }
               
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            
//                })
                self.EmptyView.alpha = 0.0
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                self.refreshControlz.endRefreshing()
                self.ProductImageCV.reloadData()
                self.ThumnailCV.reloadData()
                self.ProductTV.reloadData()
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = true
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = true
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func ProductDetailsSplitApi2(){
//https://www.masho.com/api_v4?userId&productId=18584&guest_Id=2305339&action=viewpagesplitout_two
        let parameters = ["action":"viewpagesplitout_two",
                          "userId":"\(UserID)",
                          "guest_Id": guest_id,
                          "productId": productID] as [String : Any]
//        let parameters = ["action":"viewpagesplitout_two",
//                          "userId":"\(UserID)",
//                          "guest_Id": guest_id,
//                          "productId": "19211"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) {[weak self] (status, response) in
            guard let self = self else {return}
//          UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
//                            self.EmptyView.alpha = 0.0
//                        }, completion: nil)
//            self.EmptyView.isHidden = true
            switch status {
            case .noNetwork:
                print("network error")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String:Any]{
                    self.ProductDetailsData2 = MHProductDetailsModel2(fromData: data)
                }
                print("reloadSectionsreloadSections")
                self.ProductTV.reloadSections([10,11,12,13,14,15], with: .none)
//                self.ProductTV.reloadData()
//                self.ProductImageCV.reloadData()
//                self.ThumnailCV.reloadData()
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                if self.isFrom == .other{
                    self.EmptyView.isHidden = false
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func DropDownList(){
        //https://www.masho.com/api?action=dropmenu&userid=803
        let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = ["action" : "dropmenu",
                          "userid" : UserID,
                          "guestId": guest_id] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
                self.EmptyView.isHidden = true
            case .success :
                print("success")
                self.EmptyView.isHidden = false
                DropDownData.removeAll()
                DropDownDataSource.removeAll()
                if let data = response["data"] as? [[String: Any]]{
                    DropDownData = [DropDownModel]()
                    for item in data{
                        DropDownData.append(DropDownModel(fromData: item))
                    }
                }
                
                for item in DropDownData{
                    DropDownDataSource.append(item.text)
                }
                
                self.setDropdown()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
                self.EmptyView.isHidden = true
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                self.EmptyView.isHidden = true
            }
        }
    }
}
extension MHProductDetailsVC : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
        if collectionView == ProductImageCV{
            if ProductDetailsData != nil{
                return ProductDetailsData.ItemImageList.count
            }
        }else{
            if ProductDetailsData != nil{
                return thumImgArray.count
            }
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //MARK: - issues in vdo product change
        if collectionView == ProductImageCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: indexPath) as! MHProductImageCVC
            if ProductDetailsData != nil{
                if ProductDetailsData.ItemImageList[indexPath.row].type == 2 {
                    cell.ProductImage.kf.setImage(with: URL(string: ProductDetailsData.ItemImageList[indexPath.row].thumpnail))
                } else{
                cell.ProductImage.kf.setImage(with: URL(string: ProductDetailsData.ItemImageList[indexPath.row].imageUrl))
                }
            }
            cell.layoutIfNeeded()
            cell.layoutSubviews()
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MHThumbnailCVC", for: indexPath) as! MHThumbnailCVC
            if ProductDetailsData != nil{
                cell.ProductImage.kf.setImage(with: URL(string: thumImgArray[indexPath.row].imageUrl))
                
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //370 : 493
        let height = collectionView.frame.height
        let multiplier = height / 493
        let width = 370 * multiplier
        return CGSize(width: width, height: height)
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ProductImageCV {
            if self.ProductDetailsData != nil {
                FullScreenIcon.isHidden = false
                ViewImageInFullScreenBTN.isHidden = false
                self.imageSource.removeAll()
//                for image in self.ProductDetailsData.ItemImageList {
//                    let img = AlamofireSource(urlString: image.imageUrl)
//                    self.imageSource.append(img!)
//                }
                self.ImageSlideSetup(inputSource: self.imageSource)
                IMGslideShow.setCurrentPage(indexPath.row, animated: true)
                CloseFullimageIcon.image = UIImage(named: "close (2)")
                IMGslideShow.backgroundColor = .black
                fullimageDisplayingView.isHidden = false
                ThumnailView.isHidden = true
                self.StringArray.removeAll()
//                for imageList in self.ProductDetailsData.imageoverview{
//                    self.StringArray.append(imageList.imageUrl)
//                }
                self.index = indexPath.row
                //                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                //                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                //                        self.ThumnailView.transform = CGAffineTransform(translationX: 0, y: self.ThumnailView.frame.height)
                //                    }, completion: nil)
                //                }
            }
        }else{
            IMGslideShow.setCurrentPage(indexPath.row, animated: true)
            ThumnailCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}


extension MHProductDetailsVC{
    
    func json(from object:Any) -> String? {
            guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
                return nil
            }
            return String(data: data, encoding: String.Encoding.utf8)
        }
//    func AddToCartItems(size: String,length: String)
    func AddToCartItems(from:String)
    {
        let wherefrom = from
        print("coming from",wherefrom)
        
        var parameters = [String : Any]()
       

             parameters = [  "action":"addtocart",
                                "userId":"\(UserID)",
                                "guest_Id": guest_id,
//                                "productId" : productID,
                             "productId" : productIDForAPI,
                             "data": json(from: dataDict) ?? ""
                                                ] as [String : Any]

        print("params for add to cart ===",parameters)
        
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                    self.AnimationImage.transform = .identity
                }, completion: { [weak self] _ in
//                    SelectedSize = self.productID
                    var msg = response["message"] as? String ?? ""
                    print(response["message"])
                    //Banner.main.showBanner(title: "", subtitle: response["message"] as? String ?? "", style: .success)
//                    let banner1 = GrowingNotificationBanner(title: "", subtitle: msg, style: .success)
//                    banner1.show()
//                    banner1.show(bannerPosition: .bottom)
                    utility.displayBanner(message: msg, style: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                        self!.AnimationImage.isHidden = true
                        self!.MainPopUpView.isHidden = true
                        let MHCartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                        MHCartVC.isFromProductDetails = true
                        //                MHCartVC.categoryArray = self.categoryArray
                        frompush = false
                        self?.navigationController?.pushViewController(MHCartVC, animated: true)
                    }
                    
                })
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                if let data = response["data"] as? [String: Any]{
                    self.AddToBagData = MHpopupModel(fromData: data)
                }
                if self.AddToBagData != nil{
                    if self.AddToBagData.popup == 1{
                        self.PopUpMainTitle.text = self.AddToBagData.message
                        self.PopUpSubTitle.text = self.AddToBagData.message2
                        self.PopUpBTNView.backgroundColor = UIColor(hex: "\(self.AddToBagData.button.background ?? "#ffffff")ff")
                        self.PopUpBTNTitle.text = self.AddToBagData.button.text
                        self.PopUpBTNTitle.textColor = UIColor(hex: "\(self.AddToBagData.button.color ?? "#ffffff")ff")
                        self.MainPopUpView.isHidden = false
                    }else{
                        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                            self.AnimationImage.transform = .identity
                        }, completion: {[weak self] _ in
//                            SelectedSize = self.productID
                            self!.AnimationImage.isHidden = true
                            self!.MainPopUpView.isHidden = true
                            let MHCartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                            MHCartVC.isFromProductDetails = true
                            //                MHCartVC.categoryArray = self.categoryArray
                            frompush = false
                            self?.navigationController?.pushViewController(MHCartVC, animated: true)
                        })
                        
                    }
                }
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension MHProductDetailsVC{
    func AddOrRemovefav(){
        isDashboardUpdted = true
        updatedProduct = productID
        var type:String = ""
        if ProductDetailsData.productdetails.favorite == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = [  "action":"Shortlist",
                            "userid":"\(UserID)",
                            "guestId": guest_id,
                            "productid": productID,
                            "type" : type] as [String : Any]
        //        self.view.alpha = 0.5
        //        self.view.isUserInteractionEnabled = false
                KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                if self.ProductDetailsData.productdetails.favorite == 0{
                    self.ProductDetailsData.productdetails.favorite = 1
                }else{
                    self.ProductDetailsData.productdetails.favorite = 0
                }
                if let tvc = self.ProductTV.cellForRow(at: IndexPath(row: 0, section: 0)) as? MHProductImageTVC {
             //   if let tvc = self.ProductCV.cellForItem(at: IndexPath(item: 0, section: 1)) as? newdesigncell {

                    if self.ProductDetailsData.productdetails.favorite == 1{
                        tvc.FavImg.image = UIImage(named: "favSelected")
                    }else{
                        tvc.FavImg.image = UIImage(named: "favUnselected")
                    }
                    
                }
                //
                 
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension MHProductDetailsVC{
    func setData(){
//        self.ProductNameLb.text = BasicData.productname
//        if BasicData.shortlist == "1"{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData.specialprice!)"
//        self.OldPriceLb.attributedText = "\(BasicData.strikeprice!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData.offerPercentage!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData.specialprice!)"
        self.BottomStrikePrice.attributedText = "\(BasicData.strikeprice!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData.mashoAssured == 1{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
       // cell?.ProductTitleLB.text = BasicData.imagetitle
        cell?.RatingView.rating = Double(BasicData.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData.deliveryMode
        cell?.paymentModeLb.text = BasicData.paymentMode
//        cell?.returnPolicyLb.text = BasicData.returnPolicy
        cell?.shippingLb.text = BasicData.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData.image!))
    }
    func setData2(){
//        self.ProductNameLb.text = BasicData1.productName
//        if BasicData1.favorite == 1{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData1.specialPrice!)"
//        self.OldPriceLb.attributedText = "\(BasicData1.price!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData1.offerPercentage!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData1.specialPrice!)"
        self.BottomStrikePrice.attributedText = "\(BasicData1.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData1.assured == 1{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData1.productTitle
        cell?.RatingView.rating = Double(BasicData1.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData1.deliveryMode
        cell?.paymentModeLb.text = BasicData1.paymentMode
//        cell?.returnPolicyLb.text = BasicData1.returnPolicy
        cell?.shippingLb.text = BasicData1.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData1.imageUrl!))
    }
    func setData3(){
//        self.ProductNameLb.text = BasicData2.productname
//        if BasicData2.shortlist == "1"{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData2.specialprice!)"
//        self.OldPriceLb.attributedText = "\(BasicData2.strikeprice!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData2.offerpercent!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData2.specialprice!)"
        self.BottomStrikePrice.attributedText = "\(BasicData2.strikeprice!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData2.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData2.productname
        cell?.RatingView.rating = Double(BasicData2.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData2.deliveryMode
        cell?.paymentModeLb.text = BasicData2.paymentMode
//        cell?.returnPolicyLb.text = BasicData2.returnPolicy
        cell?.shippingLb.text = BasicData2.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData2.image!))
    }
    func setData4(){
//        self.ProductNameLb.text = BasicData3.productname
//        if BasicData3.favorite == 1{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData3.specialprice!)"
//        self.OldPriceLb.attributedText = "\(BasicData3.strikeprice!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData3.offerpercent!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData3.specialprice!)"
        self.BottomStrikePrice.attributedText = "\(BasicData3.strikeprice!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData3.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData3.productname
        cell?.RatingView.rating = Double(BasicData3.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData3.deliveryMode
        cell?.paymentModeLb.text = BasicData3.paymentMode
//        cell?.returnPolicyLb.text = BasicData3.returnPolicy
        cell?.shippingLb.text = BasicData3.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData3.mainimage!))
    }
    func setData5(){
//        self.ProductNameLb.text = BasicData4.productname
//        if BasicData4.favorite == "1"{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData4.specialprice!)"
//        self.OldPriceLb.attributedText = "\(BasicData4.strikeprice!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData4.offerPercentage!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData4.specialprice!)"
        self.BottomStrikePrice.attributedText = "\(BasicData4.strikeprice!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData4.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData4.productname
        cell?.RatingView.rating = Double(BasicData4.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData4.deliveryMode
        cell?.paymentModeLb.text = BasicData4.paymentMode
//        cell?.returnPolicyLb.text = BasicData4.returnPolicy
        cell?.shippingLb.text = BasicData4.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData4.image))
    }
    func setData6(){
//        self.ProductNameLb.text = BasicData5.productName
//        if BasicData5.favorite == "1"{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData5.price!)"
//        self.OldPriceLb.attributedText = "\(BasicData5.strikeprice!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData5.offerpercent!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData5.price!)"
        self.BottomStrikePrice.attributedText = "\(BasicData5.strikeprice!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData5.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData5.productTitle
        cell?.RatingView.rating = Double(BasicData5.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData5.deliveryMode
        cell?.paymentModeLb.text = BasicData5.paymentMode
//        cell?.returnPolicyLb.text = BasicData5.returnPolicy
        cell?.shippingLb.text = BasicData5.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData5.imageUrl!))
    }
    func setData7(){
//        self.ProductNameLb.text = BasicData6.productName
//        if BasicData6.favorite == 1{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData6.specialPrice!)"
//        self.OldPriceLb.attributedText = "\(BasicData6.price!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData6.offerPercentage!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData6.price!)"
        self.BottomStrikePrice.attributedText = "\(BasicData6.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData6.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData6.productTitle
        cell?.RatingView.rating = Double(BasicData6.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData6.deliveryMode
        cell?.paymentModeLb.text = BasicData6.paymentMode
//        cell?.returnPolicyLb.text = BasicData6.returnPolicy
        cell?.shippingLb.text = BasicData6.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData6.imageUrl!))
    }
    func setData8(){
//        self.ProductNameLb.text = BasicData7.productName
//        if BasicData7.favorite == 1{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData7.specialPrice!)"
//        self.OldPriceLb.attributedText = "\(BasicData7.price!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData7.offerPercentage!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData7.price!)"
        self.BottomStrikePrice.attributedText = "\(BasicData7.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData7.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData7.productTitle
        cell?.RatingView.rating = Double(BasicData7.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData7.deliveryMode
        cell?.paymentModeLb.text = BasicData7.paymentMode
//        cell?.returnPolicyLb.text = BasicData7.returnPolicy
        cell?.shippingLb.text = BasicData7.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData7.imageUrl!))
    }
    func setData9(){
//        self.ProductNameLb.text = BasicData8.productName
//        if BasicData8.favorite == 1{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData8.price!)"
//        self.OldPriceLb.attributedText = "\(BasicData8.price!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData8.offerpercent!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData8.price!)"
        self.BottomStrikePrice.attributedText = "\(BasicData8.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData8.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
        //cell?.ProductTitleLB.text = BasicData8.productTitle
        cell?.RatingView.rating = Double(BasicData8.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData8.deliveryMode
        cell?.paymentModeLb.text = BasicData8.paymentMode
//        cell?.returnPolicyLb.text = BasicData8.returnPolicy
        cell?.shippingLb.text = BasicData8.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
//        celll?.ProductImage.kf.setImage(with: URL(string: BasicData8.imageUrl!))
    }
    func setData10(){
//        self.ProductNameLb.text = BasicData9.productName
//        if BasicData9.favorite == 1{
//            self.FavImageView.image = UIImage(named: "favSelected")
//        }else{
//            self.FavImageView.image = UIImage(named: "favUnselected")
//        }
//
//        self.SpecialPriceLB.text = "\(BasicData9.price!)"
//        self.OldPriceLb.attributedText = "\(BasicData9.price!)".updateStrikeThroughFont(UIColor(red: 217/255, green: 36/255, blue: 36/255, alpha: 1))
//        self.PercentageLb.text = "\(BasicData9.offerpercent!) % OFF"
//        self.PercentageLb.textColor = UIColor.white
//        self.PercentageView.backgroundColor = UIColor.magenta
        self.BottomPiceLb.text = "\(BasicData9.price!)"
        self.BottomStrikePrice.attributedText = "\(BasicData9.price!)".updateStrikeThroughFont(UIColor.white.withAlphaComponent(0.8))
        let cell = ProductTV.dequeueReusableCell(withIdentifier: "MHProductRateTVC", for: IndexPath(row: 0, section: 1)) as? MHProductRateTVC
        if BasicData9.mashoAssured == "1"{
            cell?.AssuredLogo.isHidden = false
        }else{
            cell?.AssuredLogo.isHidden = true
        }
//        cell?.ProductTitleLB.text = BasicData9.productTitle
        cell?.RatingView.rating = Double(BasicData9.rating)!
        cell?.ToyalRatingLb.text = "0"
        cell?.ReviewsCountLb.text = "0 Reviews"
//        cell?.SelfiesCountLb.text = "0 Selfies"
//        cell?.deliveryModeLb.text = BasicData9.deliveryMode
        cell?.paymentModeLb.text = BasicData9.paymentMode
//        cell?.returnPolicyLb.text = BasicData9.returnPolicy
        cell?.shippingLb.text = BasicData9.shipping
        let celll = ProductImageCV.dequeueReusableCell(withReuseIdentifier: "MHProductImageCVC", for: IndexPath(row: 0, section: 0)) as? MHProductImageCVC
        celll?.ProductImage.kf.setImage(with: URL(string: BasicData9.imageUrl!))
    }
}
extension MHProductDetailsVC: ImageSlideshowDelegate{
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        self.index = page
    }
}
extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
func resolutionForLocalVideo(url: URL) -> CGSize? {
    guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
   let size = track.naturalSize.applying(track.preferredTransform)
   return CGSize(width: abs(size.width), height: abs(size.height))
}
func sizeOfImageAt(url: URL) -> CGSize? {
    // with CGImageSource we avoid loading the whole image into memory
    guard let source = CGImageSourceCreateWithURL(url as CFURL, nil) else {
        return nil
    }
    
    let propertiesOptions = [kCGImageSourceShouldCache: false] as CFDictionary
    guard let properties = CGImageSourceCopyPropertiesAtIndex(source, 0, propertiesOptions) as? [CFString: Any] else {
        return nil
    }
    
    if let width = properties[kCGImagePropertyPixelWidth] as? CGFloat,
       let height = properties[kCGImagePropertyPixelHeight] as? CGFloat {
        return CGSize(width: width, height: height)
    } else {
        return nil
    }
}
func verifyUrl (urlString: String?) -> Bool {
    if let urlString = urlString {
        if let url = NSURL(string: urlString) {
            return UIApplication.shared.canOpenURL(url as URL)
        }
    }
    return false
}
extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
