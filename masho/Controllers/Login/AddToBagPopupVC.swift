//
//  AddToBagPopupVC.swift
//  masho
//
//  Created by Appzoc on 12/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import DropDown
import KVSpinnerView
import AVFoundation
var SelecteD = false
var SelectedSize : String! = ""
var SelectedLength : String! = ""
var dataDict = [String: Any]()
var Pdict = [Bool:Int]()

var DataFrom : String! = ""
protocol animationDelegate{
    func Animation(from:String)
}
class AddToBagPopupVC: UIViewController {
    
    @IBOutlet weak var popupTV: IntrinsicTableView!
    @IBOutlet weak var BackImage: UIImageView!
    @IBOutlet weak var GradientView: UIView!
    let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
    let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var type : Int!
    var isFreeSize = false
    var screenShot : UIImage!
    var dropDown = DropDown()
    var postfieldsArray = [MHpostfieldsModel]()
    var productID:String!
    var presentingvc = UIViewController()
    var AddToBagPopupObject = Type2CirclePopupTVC()
    var delegate : animationDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        let utterance = AVSpeechUtterance(string: "Please confirm your product size")
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = 0.5
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
        
        BackImage.image = screenShot
        self.GradientView.isHidden = true
        popupTV.transform = CGAffineTransform(translationX: 0, y: popupTV.frame.height)
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.popupTV.transform = .identity
                self.GradientView.isHidden = false
            }, completion: nil)
//        SizedropDownSetup()
//        LengthdropDownSetup()
    }
    
    func dropDownSetup(anchorView: UIView,dataSource: [String],cell: Type2ListPopupTVC, indx : Int){
        
        dropDown.anchorView = anchorView
        dropDown.dataSource = dataSource
        DropDown.appearance().cornerRadius = 4
        dropDown.direction = .top
        DropDown.appearance().textFont = UIFont(name: "Roboto-Medium", size: 13)!
        DropDown.appearance().textColor = UIColor.darkGray
        dropDown.dismissMode = .onTap
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            cell.SizeOrLengthLb.text = item
            if self.postfieldsArray[indx].required == true{
                SelectedSize = item
                Pdict[self.postfieldsArray[indx].required] = indx
             print("add data to dictionary")
                print("is required",self.postfieldsArray[indx].required,"on -",indx)
                print("clicked field name - ",self.postfieldsArray[indx].fieldname)
                dataDict[self.postfieldsArray[indx].fieldname] = item
                DataFrom = "DropDown"
            }
            if cell.SizeOrLengthLb.text != item{
            print("please select something!")

            }
}
        
    }
    
    @IBAction func PerformTap(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.popupTV.transform = CGAffineTransform(translationX: 0, y: self.popupTV.frame.height)
            self.GradientView.isHidden = true
        }, completion: { _ in
            dataDict.removeAll()
            Pdict.removeAll()
            self.dismiss(animated: false)
        })
    }
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}
extension AddToBagPopupVC: UITableViewDelegate,UITableViewDataSource,ViewDropDowndelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if postfieldsArray[section].field == "radio"{
                return postfieldsArray.count
            }else{
                return postfieldsArray.count
            }
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if postfieldsArray[indexPath.section].field == "radio"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Type2CirclePopupTVC", for: indexPath) as!  Type2CirclePopupTVC
//                SelectedSize
                cell.TitleLb.text = self.postfieldsArray[indexPath.row].label
                cell.postfieldsOptionsArray = self.postfieldsArray
                cell.customSection = indexPath.row
                cell.productID = self.productID
                cell.SizeCV.reloadData()
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Type2ListPopupTVC", for: indexPath) as!  Type2ListPopupTVC
                cell.TitleLb.text = self.postfieldsArray[indexPath.row].label
                cell.SizeOrLengthLb.text = postfieldsArray[indexPath.row].validationtxt
                SelectedSize = ""
                SelectedLength = ""
                
                

                
                
//                if postfieldsArray[indexPath.row].fieldname == "size"{
//                    for options in postfieldsArray[indexPath.row].options{
//                        if SelectedSize == options.size{
//                            cell.SizeOrLengthLb.text = options.size
//                            SelectedSize = options.size
//                        }
//                    }
//                }else{
//                    for options in postfieldsArray[indexPath.row].options{
//                        cell.SizeOrLengthLb.text = options.size
//                        SelectedLength = options.size
//                    }
//                }
//                if self.postfieldsArray[indexPath.row].options.count != 0{
//                    cell.SizeOrLengthLb.text = self.postfieldsArray[indexPath.row].options[0].size
//                }
                
                cell.delegate = self
                cell.index = indexPath.row
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if postfieldsArray[indexPath.section].field == "radio"{
                var CVHeight : CGFloat!
                if self.postfieldsArray[indexPath.row].options.count % 7 == 0{
                    CVHeight = CGFloat((postfieldsArray[indexPath.row].options.count / 7))
                }else{
                    CVHeight = CGFloat(((postfieldsArray[indexPath.row].options.count / 7) + 1))
                }
                return (((tableView.frame.width - 40) / 7)  * CVHeight) + 75
            }else{
                if self.postfieldsArray[indexPath.row].options.count == 0{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }
        }else{
            if postfieldsArray.count == 0{
                return 0
            }else{
                return UITableView.automaticDimension
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if postfieldsArray[indexPath.row].field != "radio"{
            var indexes  = Array(dataDict.keys)
            var filterarray = postfieldsArray.filter{$0.required == true}
            var i = filterarray.count
            if filterarray.count == dataDict.count{
                print("all good")
                print("datas in dict=",dataDict)
                if let vc = presentingvc as? MHProductDetailsVC {
                    vc.AddToCartItems(from: "radio bottom")
                    dismiss(animated: true)
                }
            }
            else {
               
                
                print("fieldnames only -",indexes)
                
                Banner.main.showBanner(title: "", subtitle: "Please confirm your product size", style: .danger)
            }
        }
        else{
            print("datas in dict=",presentingvc)
            if let vc = presentingvc as? MHProductDetailsVC {
                vc.AddToCartItems(from: "radio bottom")
                dismiss(animated: true)
            }
           // self.delegate.Animation(from: "radio bottom")
        }
        
        
//        Banner.main.showBanner(title: "", subtitle: "Please confirm your product size", style: .danger)
//        print("clicked")
//        if indexPath.section == 1{
//
//            if postfieldsArray[indexPath.row].field != "radio"{
//                if self.postfieldsArray[indexPath.row].options.count == 0{
//                    if SelectedSize != "" || SelectedLength != ""{
//                        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//                            self.popupTV.transform = CGAffineTransform(translationX: 0, y: self.popupTV.frame.height)
//                            self.GradientView.isHidden = true
//                        }, completion: { _ in
//                            self.dismiss(animated: false, completion: nil)
//                            // self.navigationController?.popViewController(animated: false)
//                            self.delegate.Animation(data: SelectedLength!, from: SelectedSize!)
//                        })
//                    }else{
//                        Banner.main.showBanner(title: "", subtitle: "Please confirm your product size", style: .danger)
//                    }
//                }else{
//                    if isFreeSize{
//                        if SelectedLength != ""{
//                            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//                                self.popupTV.transform = CGAffineTransform(translationX: 0, y: self.popupTV.frame.height)
//                                self.GradientView.isHidden = true
//                            }, completion: { _ in
//                                self.dismiss(animated: false, completion: nil)
//                               // self.navigationController?.popViewController(animated: false)
////                                self.delegate.Animation(length: SelectedLength!, size: "")
//                            })
//                        }else{
//                            Banner.main.showBanner(title: "", subtitle: "Please confirm your product length", style: .danger)
//                        }
//                    }else{
//                        if SelectedSize != "" && SelectedLength != ""{
//                            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//                                self.popupTV.transform = CGAffineTransform(translationX: 0, y: self.popupTV.frame.height)
//                                self.GradientView.isHidden = true
//                            }, completion: { _ in
//                                self.dismiss(animated: false, completion: nil)
//                               // self.navigationController?.popViewController(animated: false)
////                                self.delegate.Animation(length: SelectedLength!, size: SelectedSize!)
//                            })
//                        }else{
//                            Banner.main.showBanner(title: "", subtitle: "Please confirm your product size", style: .danger)
//                        }
//                    }
//
//                }
//
//            }else{
//                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//                    self.popupTV.transform = CGAffineTransform(translationX: 0, y: self.popupTV.frame.height)
//                    self.GradientView.isHidden = true
//                }, completion: { _ in
//                    self.dismiss(animated: false, completion: nil)
////                    self.delegate.Animation(data: SelectedLength!, from: SelectedSize!)
//                })
//            }
//        }
    }
    
    func ViewDropDown(_ cell: Type2ListPopupTVC,index: Int){
        var data = [String]()
        for item in self.postfieldsArray[index].options{
            data.append(item.size)
        }
        
        self.dropDownSetup(anchorView: cell.anchorView,dataSource: data,cell: cell, indx : index)
        dropDown.show()
    }
}
extension AddToBagPopupVC{
    func AddToCartItems(){
        for postfields in postfieldsArray{
            if isFreeSize{
                for options in postfields.options{
                    if SelectedLength == options.size{
                        SelectedLength = "\(options.sizeId!)"
                    }
                }
            }else{
                if postfields.fieldname == "size"{
                    for options in postfields.options{
                        if SelectedSize == options.size{
                            SelectedSize = "\(options.sizeId!)"
                        }
                    }
                }else{
                    for options in postfields.options{
                        if SelectedLength == options.size{
                            SelectedLength = "\(options.sizeId!)"
                        }
                    }
                }
            }
        }
        print("size already selected",selectedsizes)
        print("size",SelectedSize!)
        print("Length",SelectedLength!)
    }
}
