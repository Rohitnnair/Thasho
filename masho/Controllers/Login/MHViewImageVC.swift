//
//  MHViewImageVC.swift
//  masho
//
//  Created by Appzoc on 13/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import Firebase
import ImageSlideshow
class MHViewImageVC: UIViewController {
    @IBOutlet weak var IMGslideShow: ImageSlideshow!
    @IBOutlet weak var ThumnailView: UIView!
    @IBOutlet weak var ThumnailCV: UICollectionView!
    var ProductDetailsData : MHProductDetailsModel!
    var index : Int?
 var inde = 0

    var imageSource = [AlamofireSource]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        IMGslideShow.zoomEnabled = true
        IMGslideShow.circular = true
        let pagecontrol = UIPageControl()
        pagecontrol.hidesForSinglePage = true
        pagecontrol.currentPageIndicatorTintColor = UIColor.MHGold
        pagecontrol.pageIndicatorTintColor = UIColor.lightGray.withAlphaComponent(0.7)
        IMGslideShow.pageIndicator = pagecontrol
        IMGslideShow.setImageInputs(imageSource)
        IMGslideShow.setCurrentPage(index!, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        logEvent()
    }
    
    func logEvent(){
        Analytics.logEvent("View_Image", parameters: [:])
    }
    
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    var i = 0
    @IBAction func performTapped(_ sender: UITapGestureRecognizer) {
        if i == 0{
            i = 1
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.ThumnailView.transform = CGAffineTransform(translationX: 0, y: self.ThumnailView.frame.height)
            }, completion: nil)
        }else{
            i = 0
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.ThumnailView.transform = .identity
            }, completion: nil)
        }
    }
}
extension MHViewImageVC : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ProductDetailsData.ItemImageList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MHThumbnailCVC", for: indexPath) as! MHThumbnailCVC
        cell.ProductImage.kf.setImage(with: URL(string: ProductDetailsData.ItemImageList[indexPath.row].imageUrl))
       if  inde == indexPath.row
       {
        cell.ProductImage.layer.borderWidth = 1
        cell.ProductImage.layer.borderColor = #colorLiteral(red: 0.8296601772, green: 0.6882546544, blue: 0.2148788273, alpha: 1)
       }else {
        cell.ProductImage.layer.borderWidth = 0
        cell.ProductImage.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
       }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //370 : 493
        let height = collectionView.frame.height
        let multiplier = height / 493
        let width = 370 * multiplier
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        IMGslideShow.setCurrentPage(indexPath.row, animated: true)
        ThumnailCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        inde = indexPath.item
        
        ThumnailCV.reloadData()
        
    }
}
