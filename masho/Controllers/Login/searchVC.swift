//
//  searchVC.swift
//  masho
//
//  Created by admin on 23/04/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit

class searchVC: UIViewController,UITextFieldDelegate {
    var TableID : String!
    var UserID : String!
    var guest_id : Int! = nil
    var searchData = [searchModel]()
    @IBOutlet weak var searchTF : UITextField!
    @IBOutlet weak var SearchTV:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTF.delegate = self
        searchTF.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    }

    @IBAction func BackBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
//    func shouldChangeCharactersInRange(_ textField: UITextField) -> Bool {
//        print(textField.text)
//        return true
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            print(textField.text!)
            self.searchContent(txt: textField.text!)
        })
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}
extension searchVC{
    
    func searchContent(txt:String){
        let parameters = ["action":"searchautocomplete",
                          "userId": UserID!,
                          "guestId": guest_id!,
                          "name": txt,
                          "tableid": TableID!] as [String : Any]
//        self.view.alpha = 0.5
//        self.view.isUserInteractionEnabled = false
//        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                self.searchData.removeAll()
                if let data = response["data"] as? Json{
                    if let result = data["result"] as? JsonArray{
                        for item in result{
                            self.searchData.append(searchModel(fromData: item))
                        }
//                        self.searchData.sort {
//                        $0.searchkey < $1.searchkey
//                    }
//                    self.loginData = LoginModel(fromData: data)
//                    print(self.loginData.otp)
//                    let otpVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHOTPVC") as! MHOTPVC
//                    otpVC.otp = self.loginData.otp!
//                    otpVC.OTPmessage = self.loginData.message!
//                    otpVC.Countrycode = self.CountryCodeLb.text
//                    otpVC.code = self.mobcode
//                    otpVC.PhoneNumber = self.PhoneTF.text!
//                    otpVC.isFromLogin = true
//                    self.navigationController?.pushViewController(otpVC, animated: true)
                    }
                    self.searchTF.placeholder = data["text"] as? String
                    self.SearchTV.reloadData()
                }
            case .failure :
                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
class SearchTVC: UITableViewCell{
    @IBOutlet weak var searchLb:UILabel!
}
extension searchVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVC") as! SearchTVC
        cell.searchLb.text = searchData[indexPath.row].searchkey
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
        vc.categoryId = "\(self.searchData[indexPath.row].api_id!)"
        vc.api_Key = Int(self.searchData[indexPath.row].api_id) ?? 0
        vc.SubCategoryId = self.searchData[indexPath.row].catsubcat
        vc.searchkey = self.searchData[indexPath.row].searchkey
        vc.searchvalue =  self.searchData[indexPath.row].searchvalue
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

