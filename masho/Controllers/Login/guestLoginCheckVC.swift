//
//  guestLoginCheckVC.swift
//  masho
//
//  Created by Appzoc on 12/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
class guestLoginCheckVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.guestLogin()
        })
    }
    
    func guestLogin(){
        let parameters = ["action":"guestlogin"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .get,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
                //Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }

}
