//
//  MHCountryCodeVC.swift
//  masho
//
//  Created by Appzoc on 11/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Kingfisher
protocol countryCodeSelectorDelegate{
    func didSelectCountryCode(CountrycodeData: CountryCodeModel)
}
class MHCountryCodeVC: UIViewController,UITextFieldDelegate {
    var delegate : countryCodeSelectorDelegate!
    var searchedArray = [CountryCodeModel]()
    var countryCodeArray = [CountryCodeModel]()
    var screenShot : UIImage!
    @IBOutlet weak var BackGrountImage: UIImageView!
    @IBOutlet weak var SearchTF: SkyFloatingLabelTextField!
    @IBOutlet weak var MyTable : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchTF.delegate = self
        print(countryCodeArray)
        searchedArray = countryCodeArray
        BackGrountImage.image = screenShot
        // Do any additional setup after loading the view.
    }
    @IBAction func EditChanged(_ sender: UITextField) {
        if sender == SearchTF{
            let searchedArray1 = countryCodeArray.filter { $0.code.localizedCaseInsensitiveContains(SearchTF.text!) || $0.name.localizedCaseInsensitiveContains(SearchTF.text!) }
            searchedArray = (SearchTF.text?.isEmpty)! ? countryCodeArray : searchedArray1
            self.MyTable.reloadData()
        }
    }
    
    @IBAction func PerformTap(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: false)
    }
    
}


class CountryCodeTVC: UITableViewCell{
    @IBOutlet weak var CodeLb: UILabel!
    @IBOutlet weak var FlagIMG: UIImageView!
}






extension MHCountryCodeVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeTVC", for: indexPath) as! CountryCodeTVC
        cell.CodeLb.text = "\(searchedArray[indexPath.row].code!) (\(searchedArray[indexPath.row].name!))"
//        cell.FlagIMG.kf.setImage(with: URL(string: "https://www.masho.com/assets/flag/32x32/dz.png"))
        cell.FlagIMG.kf.setImage(with: URL(string: "\(searchedArray[indexPath.row].flag!)"))
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didSelectCountryCode(CountrycodeData: searchedArray[indexPath.row])
        self.navigationController?.popViewController(animated: false)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
