//
//  Country_Language_CurrencyPopupVC.swift
//  masho
//
//  Created by Appzoc on 09/06/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
protocol updateCountryDetailsDelegate {
    func updateCountryDetails(CurrencyCode: String,languageCode: String,CountryCode : String,countryName: String )
}
class Country_Language_CurrencyPopupVC: UIViewController {
    var CurrencyPopupScreen : CurrencyPopupScreens = .sidemenu

    var SideMenucountrydetails : SideMenucountrydetailsModel!
    var Choice : Int = 0
//    var isSelectLanguage = false
    var delegate : updateCountryDetailsDelegate!
    var CurrencyCode : String!
    var languageCode : String!
    var CountryCode  : String!
    var countryName  : String!
    var CurrencyByCountryIdArray : getCurrencyByCountryIdModel!
    
    @IBOutlet weak var TableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var BackGroundImageView : UIImageView!
    @IBOutlet weak var GradientView: UIView!
    @IBOutlet weak var AlertView: BaseView!
    @IBOutlet weak var AlertView2: UIView!
    @IBOutlet weak var GradientView2: UIView!
    
    @IBOutlet weak var SubTitle1: UILabel!
    @IBOutlet weak var SubTitle2: UILabel!
    @IBOutlet weak var SubTitle3: UILabel!
    
    @IBOutlet weak var Flag: UIImageView!
    
    @IBOutlet weak var Content1: UILabel!
    @IBOutlet weak var Content2: UILabel!
    @IBOutlet weak var Content3: UILabel!
    @IBOutlet weak var contentTV: UITableView!
    
    var BackGroundImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BackGroundImageView.image = BackGroundImage!
        
        GradientView.isHidden = true
        GradientView2.isHidden = true
        AlertView2.isHidden = true
        
        self.AlertView.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.AlertView2.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.AlertView.transform = .identity
            self.GradientView.isHidden = false
        }, completion: nil)
//        var sortedArray = names.sorted { $0.localizedCaseInsensitiveCompare($1) == NSComparisonResult.OrderedAscending }

        SideMenucountrydetails.countryselect.datas = SideMenucountrydetails.countryselect.datas.sorted { $0.text.localizedCaseInsensitiveCompare($1.text) == ComparisonResult.orderedAscending }
        SubTitle1.text = SideMenucountrydetails.countryselect.text
        SubTitle2.text = SideMenucountrydetails.languageselect.text
        SubTitle3.text = SideMenucountrydetails.Currencyselect.text
        Content1.text  = SideMenucountrydetails.countryname
        Content2.text  = SideMenucountrydetails.Language
        self.Content3.text  = SideMenucountrydetails.Currency//"------ Select Currency ------"
        self.CurrencyCode = SideMenucountrydetails.Currencyid
        self.CountryCode = SideMenucountrydetails.countryid
        self.countryName = SideMenucountrydetails.countryname
        self.languageCode = "\(SideMenucountrydetails.languageid!)"
//        getcurrency(countryID : SideMenucountrydetails.countryid)
        Flag.kf.setImage(with: URL(string: self.SideMenucountrydetails.image!))
        if self.SideMenucountrydetails.countryid != nil{
            getcurrency(countryID : self.SideMenucountrydetails.countryid, stats: false)
        }
    }
    @IBAction func CancelBtn(_ sender: UIButton) {
        if self.languageCode != "" && self.CurrencyCode != ""{
            UIView.animate(withDuration: 0.2, delay: 1, options: .curveEaseOut, animations: {
                self.GradientView.isHidden = true
                self.AlertView.transform = CGAffineTransform(scaleX: 0, y: 0)
            }, completion: { _ in
                if self.CurrencyPopupScreen == .dashboard{
                    self.delegate.updateCountryDetails(CurrencyCode: self.CurrencyCode, languageCode: self.languageCode, CountryCode: self.CountryCode, countryName: self.countryName)
                }
                self.navigationController?.popViewController(animated: false)
            })
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    @IBAction func cancelCountryListBTNtapped(_ sender: Any) {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.AlertView2.transform = CGAffineTransform(scaleX: 0, y: 0)
        }, completion: { _ in
            self.AlertView2.isHidden = true
            self.GradientView2.isHidden = true
        })
    }
    
    @IBAction func ApplyBtn(_ sender: UIButton) {
        if self.languageCode != "" && self.CurrencyCode != ""{
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
                self.GradientView.isHidden = true
                self.AlertView.transform = CGAffineTransform(scaleX: 0, y: 0)
            }, completion: { _ in
                if let ShowCountryPopup =  UserDefaults.standard.value(forKey: "ShowCountryPopup") as? Bool{
                    if ShowCountryPopup{
                        self.delegate.updateCountryDetails(CurrencyCode: self.CurrencyCode, languageCode: self.languageCode, CountryCode: self.CountryCode, countryName: self.countryName)
                    }
                }
                if self.CountryCode != self.SideMenucountrydetails.countryid || self.countryName != self.SideMenucountrydetails.countryname || self.CurrencyCode != self.SideMenucountrydetails.Currencyid || self.languageCode != "\(self.SideMenucountrydetails.languageid!)" {
                    self.delegate.updateCountryDetails(CurrencyCode: self.CurrencyCode, languageCode: self.languageCode, CountryCode: self.CountryCode, countryName: self.countryName)
                }
                
                self.navigationController?.popViewController(animated: false)
            })
        }else{
            if self.languageCode == ""{
                Banner.main.showBanner(title: "", subtitle: "Please select Language", style: .danger)
            }
            if self.CurrencyCode == ""{
                Banner.main.showBanner(title: "", subtitle: "Please select Currency", style: .danger)
            }
        }
        

    }
    
    @IBAction func ViewContentpopupBTNtapped(_ sender: UIButton) {
        print(sender.tag)
        if sender.tag == 0{
            Choice = sender.tag
            TableViewHeightConstraint.constant = CGFloat(100 * SideMenucountrydetails.countryselect.datas.count)
        }else if sender.tag == 1{
            Choice = sender.tag
            TableViewHeightConstraint.constant = CGFloat(100 * SideMenucountrydetails.languageselect.datas.count)
        }else{
            Choice = sender.tag
            TableViewHeightConstraint.constant = CGFloat(100 * SideMenucountrydetails.Currencyselect.datas.count)
        }
        GradientView2.isHidden = false
        AlertView2.isHidden = false
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
        self.AlertView2.transform = .identity
        }, completion: nil)
        contentTV.reloadData()
        
    }
    
    
}
extension Country_Language_CurrencyPopupVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Choice == 0{
            return SideMenucountrydetails.countryselect.datas.count
        }else if Choice == 1{
            return CurrencyByCountryIdArray.language.count
        }else{
             return CurrencyByCountryIdArray.currency.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Choice == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "country_language_currency_1_TVC") as! country_language_currency_1_TVC
            cell.CountryLb.text = SideMenucountrydetails.countryselect.datas[indexPath.row].text
            cell.Flag.kf.setImage(with: URL(string: self.SideMenucountrydetails.countryselect.datas[indexPath.row].image!))
            return cell
        }else if Choice == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "country_language_currency_TVC") as! country_language_currency_TVC
            cell.TitleLb.text = CurrencyByCountryIdArray.language[indexPath.row].text
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "country_language_currency_TVC") as! country_language_currency_TVC
            cell.TitleLb.text = CurrencyByCountryIdArray.currency[indexPath.row].text
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        self.GradientView2.isHidden = true
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.AlertView2.transform = CGAffineTransform(scaleX: 0, y: 0)
        }, completion: { _ in
            self.AlertView2.isHidden = true
            self.GradientView2.isHidden = true
            if self.Choice == 0{
                self.Content1.text = self.SideMenucountrydetails.countryselect.datas[indexPath.row].text
                self.Flag.kf.setImage(with: URL(string: self.SideMenucountrydetails.countryselect.datas[indexPath.row].image!))
//                self.isSelectLanguage = true
                self.CountryCode = self.SideMenucountrydetails.countryselect.datas[indexPath.row].value
                self.countryName = self.SideMenucountrydetails.countryselect.datas[indexPath.row].text
                self.getcurrency(countryID : self.SideMenucountrydetails.countryselect.datas[indexPath.row].value, stats: true)
            }
            else if self.Choice == 1{
                self.languageCode = self.CurrencyByCountryIdArray.language[indexPath.row].value
                self.Content2.text = self.CurrencyByCountryIdArray.language[indexPath.row].text
            }else{
                self.CurrencyCode = self.CurrencyByCountryIdArray.currency[indexPath.row].value
                self.Content3.text = self.CurrencyByCountryIdArray.currency[indexPath.row].text
            }
        })
    }
}
extension Country_Language_CurrencyPopupVC{
    func getcurrency(countryID : String,stats:Bool){
        let ID : Int = Int(countryID)!
        let parameters = [  "action":"getcurrencybycountryid",
                            "country": ID] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                
                print("success")
                print(response["data"]!)
                
                if let currencyData = response["data"] as? [String : Any]{
                    self.CurrencyByCountryIdArray = getCurrencyByCountryIdModel(fromData: currencyData)
                }
//                if self.isSelectLanguage{
//                    self.isSelectLanguage = false
                var matchFound = false
                if stats {
                    for str in self.CurrencyByCountryIdArray.currency{
                        if str.text.hasSuffix(self.countryName) { // true
                            matchFound = true
                            self.Content3.text  = str.text
                            self.CurrencyCode = str.value
                        }
    //                    else{
    //                        self.Content3.text  = "------ Select Currency ------"
    //                        self.CurrencyCode = ""
    //                    }
                    }
                }
                
                if matchFound == false{
//                    self.Content3.text  = "------ Select Currency ------"
//                    self.CurrencyCode = ""
                }
                    self.Content2.text = self.CurrencyByCountryIdArray.language[0].text
                    self.languageCode = self.CurrencyByCountryIdArray.language[0].value
//                }
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
