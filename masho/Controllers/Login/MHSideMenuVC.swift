//
//  MHSideMenuVC.swift
//  masho
//
//  Created by Appzoc on 26/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import Firebase
import SafariServices

var isLanguageUpdated = false
var isLogged : Bool!
var isLogOut: Bool!
var isFromSideMenu = false

class MHSideMenuVC: UIViewController {
//    var categoryArray = ["Abayas - Burqas","Hijabs - carves - Wraps","Full Sleeve Kurti's","Bags","My Cart","My Wish List","My Viewed List","Logout"]
    var sideMenuIcons = ["Category1","Category2","Category3","Category4","supermarket","heart","wishlist","arrow"]
    var categoryArray = [categoryListmodel]()
    var SideMenuArray = [SideMenuModel]()
    var SettingsArray = [SideMenuSettingsModel]()
    var ContactUsArray = [SideMenuContactUsModel]()
    var ColorArray = [UIColor]()
    var SideMenucountrydetails : SideMenucountrydetailsModel!
    var WelcomeDic : welcomeModel?

    var Device_token : String!
    var UserID : String!
    var TableID : String!
    var userName : String!
    var guest_id : Int! = nil
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var CameraiconView: UIView!
    @IBOutlet weak var welcomeUser: UILabel!
    @IBOutlet weak var AppVersion: UILabel!
    @IBOutlet weak var welcomeMessage: UILabel!
    @IBOutlet weak var SideMenuCV: UICollectionView!
    var SolidColorArray = [UIColor.red,
                           UIColor.green,
                           UIColor.blue,
                           UIColor.orange,
                           UIColor.brown,
                           UIColor.cyan,
                           UIColor.green,
                           UIColor.magenta,
                           UIColor.purple,
                           UIColor.darkGray,
                           UIColor.black,
                           UIColor.red,
                           UIColor.green,
                           UIColor.blue,
                           UIColor.orange,
                           UIColor.brown,
                           UIColor.cyan,
                           UIColor.green,
                           UIColor.magenta,
                           UIColor.purple,
                           UIColor.darkGray,
                           UIColor.black]
    override func viewDidLoad() {
        super.viewDidLoad()
        print("release: \(UIApplication.release)")
        print("build: \(UIApplication.build)")
        print("version: \(UIApplication.version)")

        AppVersion.text = "App Version - V \(UIApplication.version)"
        isLogged = false
        isLogOut = false
        self.categoryArray = categoryListArray1
        ProfileImage.layer.cornerRadius = ProfileImage.frame.height / 2
        transparentView.layer.cornerRadius = transparentView.frame.height / 2
        CameraiconView.layer.cornerRadius = CameraiconView.frame.height / 2
        
        self.welcomeMessage.text = "Welcome to Masho.com"
        if WelcomeDic != nil{
            self.ProfileImage.kf.setImage(with: URL(string: (self.WelcomeDic?.image)!))
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        logEvent()
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        if userName == ""{
            welcomeUser.text = "Hi, User"
        }else{
            welcomeUser.text = "Hi, \(userName!)"
        }
        Leftmenu()
     
        
    }
    func logEvent(){
        Analytics.logEvent("Sidemenu_Opened", parameters: [:])
    }
    func checkLogin() -> Bool{
        let logInStatus = UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool ?? false
        return logInStatus
    }
    func redirectToLogin(){
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC")
            self.navigationController?.push(viewController: vc)
        }
    }
   
    @IBAction func UploadImageBTNtapped(_ sender: Any) {
        if checkLogin(){
            let actionSheet = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                self.present(imagePicker, animated: false, completion: nil)
            }
            let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = .camera
                imagePicker.delegate = self
                self.present(imagePicker, animated: false, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            actionSheet.addAction(galleryAction)
            actionSheet.addAction(cameraAction)
            actionSheet.addAction(cancelAction)
            DispatchQueue.main.async {
                self.present(actionSheet, animated: true, completion: nil)
            }
        }else{
            redirectToLogin()
        }
    }
    
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func OpenLanguagePOPupArrowAction(_ sender: Any) {
        let screenshot  = self.view.takeScreenshot()
        let PopupVC = StoryBoard.login.instantiateViewController(withIdentifier: "Country_Language_CurrencyPopupVC") as! Country_Language_CurrencyPopupVC
        PopupVC.CurrencyPopupScreen = .sidemenu
        PopupVC.BackGroundImage = screenshot
        PopupVC.delegate = self
        PopupVC.SideMenucountrydetails = self.SideMenucountrydetails
        self.navigationController?.pushViewController(PopupVC, animated: false)
    }
    
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}

extension MHSideMenuVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        if let jpegData = tempImage.jpegData(compressionQuality: 0.8){
            uploadUserImage(image: jpegData)
        }
        DispatchQueue.main.async {
            self.transparentView.isHidden = true
            self.ProfileImage.image = tempImage
        }
        
        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
    
    func uploadUserImage(image:Data){
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        let userID = UserDefaults.standard.object(forKey: "userID") as? String ?? ""
        Webservice(url: "", parameters: ["userid":"\(userID)","action":"uploadphotoapi"]).executeMultipartPOST(dataParameters: ["image":image], mimeType: "image/jpeg", shouldReturn: .json) { (json, didComplete, response) in
            print(json)
            DispatchQueue.main.async {
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            }
            
        }
    }
    
}


extension MHSideMenuVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return SideMenuArray.count
        }else if section == 1{
            return 1
        }else if section == 2{
            return 1
        }else if section == 3{
            return 1
        }else if section == 4{
            return SettingsArray.count
        }else if section == 5{
            return 1
        }else{
            return ContactUsArray.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SideMenuCVC", for: indexPath) as! SideMenuCVC
            cell.SideMenuLb.text = SideMenuArray[indexPath.row].leftmenuname
            cell.SideMenuIcon.kf.setImage(with: URL(string: SideMenuArray[indexPath.row].image))
            cell.BackgroundColorView.backgroundColor = ColorArray[indexPath.row].withAlphaComponent(0.1)
            return cell
        }else if indexPath.section == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sectionTitleCVC", for: indexPath) as! sectionTitleCVC
            cell.TitleLb.text = "Country, Language and Currency"
            DispatchQueue.main.async{
                cell.SeperatorLine.isHidden = true
                cell.ArrowIMG.isHidden = false
                cell.BTN.isHidden = false
            }
            return cell
        }else if indexPath.section == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "country_language_currency_CVC", for: indexPath) as! country_language_currency_CVC
            cell.CountryLb.text = self.SideMenucountrydetails.countryname
            for data in self.SideMenucountrydetails.Currencyselect.datas{
                if data.value == self.SideMenucountrydetails.Currencyid{
                    var firstPart = ""
                    if let range = data.text.range(of: "-") {
                        firstPart = String(data.text[data.text.startIndex..<range.lowerBound])
                        print(firstPart) // print Hello
                    }
                    cell.currencyLb.text = firstPart
                    break
                }
            }
            cell.languageLb.text = self.SideMenucountrydetails.Language
            cell.Flag.kf.setImage(with: URL(string: self.SideMenucountrydetails.image!))
            return cell
        }else if indexPath.section == 3{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sectionTitleCVC", for: indexPath) as! sectionTitleCVC
            cell.TitleLb.text = "Setting Center"
            DispatchQueue.main.async{
                cell.SeperatorLine.isHidden = false
                cell.ArrowIMG.isHidden = true
                cell.BTN.isHidden = true
            }
            return cell
        }else if indexPath.section == 4{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sectionContentCVC", for: indexPath) as! sectionContentCVC
            cell.TitleLb.text = SettingsArray[indexPath.row].name
            cell.IconImage.kf.setImage(with: URL(string: SettingsArray[indexPath.row].image))
            return cell
        }else if indexPath.section == 5{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sectionTitleCVC", for: indexPath) as! sectionTitleCVC
            cell.TitleLb.text = "Contact Us"
            DispatchQueue.main.async{
                cell.SeperatorLine.isHidden = false
                cell.ArrowIMG.isHidden = true
                cell.BTN.isHidden = true
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sectionContentCVC", for: indexPath) as! sectionContentCVC
            cell.TitleLb.text = ContactUsArray[indexPath.row].name
            cell.IconImage.kf.setImage(with: URL(string: ContactUsArray[indexPath.row].image))
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            return CGSize(width: collectionView.frame.width / 3.15, height: collectionView.frame.width / 3.15)
        }else if indexPath.section == 1{
            return CGSize(width: collectionView.frame.width, height: 50)
        }else if indexPath.section == 2{
            return CGSize(width: collectionView.frame.width, height: 55)
        }else if indexPath.section == 3{
            return CGSize(width: collectionView.frame.width, height: 50)
        }else if indexPath.section == 4{
            return CGSize(width: collectionView.frame.width / 4, height: 90)
        }else if indexPath.section == 5{
            return CGSize(width: collectionView.frame.width, height: 50)
        }else{
            return CGSize(width: collectionView.frame.width / 4, height: 90)
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let cellData = SideMenuArray[indexPath.row]
            print("cellData",cellData.leftmenuname)
            if cellData.type == 1 {
                let isLogin = cellData.leftmenuname.lowercased().contains("login")
                let isLogOut = cellData.leftmenuname.lowercased().contains("logout")
                if isLogin {
                    isFromSideMenu = false
                    let loginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
                    self.navigationController?.pushViewController(loginVC, animated: false)
                } else if isLogOut {
                    isLanguageUpdated = true
                    self.logout()
                } else if cellData.api.lowercased().contains("getProductsByCatId".lowercased()) {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(SideMenuArray[indexPath.row].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                } else if cellData.api.lowercased().contains("getCartItems".lowercased()) {
                   
                        let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                        CartVC.categoryArray = self.categoryArray
                        frompush = false
                        self.navigationController?.pushViewController(CartVC, animated: true)
                  
                } else if cellData.api.lowercased().contains("categorys".lowercased()) {
                                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                                    vc.isFromSidemenu = true
                                    self.navigationController!.pushViewController(vc, animated: true)
                    
                } else if cellData.api.lowercased().contains("myviewlist".lowercased()) {
                   
                        let RescentProductListVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
                        frompush = false
                        self.navigationController?.pushViewController(RescentProductListVC, animated: true)
                   
                } else if cellData.api.lowercased().contains("favouriteList".lowercased()) {
                   
                        let wishlistVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHWishListVC") as! MHWishListVC
                        wishlistVC.categoryArray = self.categoryArray
                        frompush = false
                        self.navigationController?.pushViewController(wishlistVC, animated: true)
                   
                } else if cellData.api.lowercased().contains("getproductdetails".lowercased()) {
                    let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            //        MHProductDetailsVC.categoryArray = categoryArray
                    MHProductDetailsVC.isFrom = .other
                    //MHProductDetailsVC.BasicData7 = ProductArray[indexPath.row]
//                    MHProductDetailsVC.productID = SideMenuArray[indexPath.row].api_id.description
                    productID = SideMenuArray[indexPath.row].api_id.description
                     frompush = false
                    self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
                } else if cellData.api.lowercased().contains("showcategory".lowercased()) {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    vc.isFromSidemenu = true
                    self.navigationController!.pushViewController(vc, animated: true)
                }
            } else if cellData.type == 2 {
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = cellData.api
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            } else if cellData.type == 3 {
            
                if let url = URL(string: cellData.api) {
                    let controller = SFSafariViewController(url: url)
                    self.present(controller, animated: true, completion: nil)
                }
               
            }
           
//            let cel = collectionView.cellForItem(at: indexPath) as! SideMenuCVC
//            if cel.SideMenuLb.text == "SignUp/Login"{
//                isFromSideMenu = false
//                let loginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
//                self.navigationController?.pushViewController(loginVC, animated: false)
//            }else if cel.SideMenuLb.text == "Logout"{
//                isLanguageUpdated = true
//                self.logout()
//
//            }else if cel.SideMenuLb.text == "My Viewed List"{
//                //            if checkLogin(){
//                let RescentProductListVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
//                frompush = false
//                self.navigationController?.pushViewController(RescentProductListVC, animated: true)
//                //            }else{
//                //                redirectToLogin()
//                //            }
//            }else if cel.SideMenuLb.text == "My Bag"{
//                //            if checkLogin(){
//                let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
////                CartVC.categoryArray = self.categoryArray
//                frompush = false
//                self.navigationController?.pushViewController(CartVC, animated: true)
//                //            }else{
//                //                redirectToLogin()
//                //            }
//            }else if cel.SideMenuLb.text == "My Wishlist"{
//                //            if checkLogin(){
//                let wishlistVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHWishListVC") as! MHWishListVC
////                wishlistVC.categoryArray = self.categoryArray
//                frompush = false
//                self.navigationController?.pushViewController(wishlistVC, animated: true)
//                //            }else{
//                //                redirectToLogin()
//                //            }
//            }else if cel.SideMenuLb.text == "My Orders"{
//                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//                WebViewVC.WebURL = SideMenuArray[indexPath.row].api
//                self.navigationController!.pushViewController(WebViewVC, animated: true)
//            }else if cel.SideMenuLb.text == "Rewards"{
//                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//                WebViewVC.WebURL = SideMenuArray[indexPath.row].api
//                self.navigationController!.pushViewController(WebViewVC, animated: true)
//            }else if cel.SideMenuLb.text == "Product Categories " {
//                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
//                vc.isFromSidemenu = true
////                self.SolidColorArray.shuffle()
////                vc.colorArray = self.SolidColorArray
////                WebViewVC.WebURL = SideMenuArray[indexPath.row].api
//                self.navigationController!.pushViewController(vc, animated: true)
//            } else {
//                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
//                vc.categoryId = "\(SideMenuArray[indexPath.row].api_id!)"
//                vc.SubCategoryId = "sc"
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
        }else if indexPath.section == 2{
            let screenshot  = self.view.takeScreenshot()
            let PopupVC = StoryBoard.login.instantiateViewController(withIdentifier: "Country_Language_CurrencyPopupVC") as! Country_Language_CurrencyPopupVC
            PopupVC.CurrencyPopupScreen = .sidemenu
            PopupVC.BackGroundImage = screenshot
            PopupVC.delegate = self
            PopupVC.SideMenucountrydetails = self.SideMenucountrydetails
            self.navigationController?.pushViewController(PopupVC, animated: false)
        }else if indexPath.section == 4{
            if SettingsArray[indexPath.row].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = SettingsArray[indexPath.row].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }
            if SettingsArray[indexPath.row].type == 3{
                guard let newURL = URL(string: SettingsArray[indexPath.row].link) else { return }
                UIApplication.shared.open(newURL)
            }
        }else if indexPath.section == 6{
            if ContactUsArray[indexPath.row].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = ContactUsArray[indexPath.row].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }
            if ContactUsArray[indexPath.row].type == 3 {
                guard let newURL = URL(string: ContactUsArray[indexPath.row].link) else { return }
                UIApplication.shared.open(newURL)
            }
        }else{
            print(" ")
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0{
            return 20
        }else if section == 2{
            return 10
        }else if section == 4{
            return 10
        }else{
            return 0
        }
    }
}

extension MHSideMenuVC: updateCountryDetailsDelegate{
    func Leftmenu(){
        
        let parameters = ["action":"leftmenu",
                          "userid": self.UserID!,
                          "guestId": guest_id!] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response)
                self.SideMenuArray.removeAll()
                if let leftMenuData = response["data"] as? [[String : Any]]{
                    for data in leftMenuData{
                        self.SideMenuArray.append(SideMenuModel(fromData: data))
                    }
                }
                if let WelcomeData = response["logindetails"] as? [String : Any]{
                    self.WelcomeDic = welcomeModel(fromData: WelcomeData)
                }
                if let countrydetails = response["countrydetails"] as? [String : Any]{
                    self.SideMenucountrydetails = SideMenucountrydetailsModel(fromData: countrydetails)
                }
                if let SettingsData = response["settings"] as? [[String : Any]]{
                    self.SettingsArray = [SideMenuSettingsModel]()
                    for item in SettingsData{
                        self.SettingsArray.append(SideMenuSettingsModel(fromData: item))
                    }
                }
                if let contactusData = response["contactus"] as? [[String : Any]]{
                    self.ContactUsArray = [SideMenuContactUsModel]()
                    for item in contactusData{
                        self.ContactUsArray.append(SideMenuContactUsModel(fromData: item))
                    }
                }
                self.ColorArray.removeAll()
                self.SolidColorArray.shuffle()
                for i in 0 ..< self.SideMenuArray.count{
                    self.ColorArray.append(self.SolidColorArray[i])
                }
                self.welcomeMessage.text = self.WelcomeDic?.welcome_message
                self.ProfileImage.kf.setImage(with: URL(string: (self.WelcomeDic?.image)!))
                self.SideMenuCV.reloadData()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func updateCountryDetails(CurrencyCode: String,languageCode: String,CountryCode : String,countryName: String ){
        UserDefaults.standard.set(countryName, forKey: "CountryName")
        let parameters = ["action" : "updatecountrysettings",
                          "userId" : self.UserID!,
                          "guestId": guest_id!,
                          "currency" : CurrencyCode,
                          "language" : languageCode,
                          "country" : CountryCode,
                          "countryname" : countryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                
                isLanguageUpdated = true
                
                self.Leftmenu()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func logout(){
        //https://www.masho.com/api?action=logout&Userid=731&tableid=TdNwLUfpD7xORIn
        let parameters = ["action" : "logout",
                          "Userid" : self.UserID!,
                          "tableid": self.TableID!] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                UserDefaults.standard.set("", forKey: "userName")
                UserDefaults.standard.set("", forKey: "userID")
                UserDefaults.standard.set("", forKey: "tableID")
                UserDefaults.standard.set(0, forKey: "guestId")
                UserDefaults.standard.set(false, forKey: "isLoggedIn")
                
                let LoginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
                self.navigationController?.pushViewController(LoginVC, animated: false)
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension UIApplication {
    static var release: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String? ?? "x.x"
    }
    static var build: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String? ?? "x"
    }
    static var version: String {
        return "\(release).\(build)"
    }
}
