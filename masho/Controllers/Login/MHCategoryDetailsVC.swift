//
//  MHCategoryDetailsVC.swift
//  masho
//
//  Created by Appzoc on 26/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import CCBottomRefreshControl
import KVSpinnerView
import SkeletonView
import Firebase
import DropDown
class MHCategoryDetailsVC: UIViewController {
    var categoryArray = [categoryListmodel]()
    var GetProductDic : getProductsmodel!
    var ProductArray = [CategoryProductModel]()
    var ProductArrayCopy = [CategoryProductModel]()
    let Device_token : String = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
    let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
    var nav_Title = ""
    let refreshControl = UIRefreshControl()
    var wantLoader = true
//    var is_favArray = [false,false,false,false,false,false]
//    var api_Key:IntOrString!
    var api_Key : Int!
    var limit = 20
    var totalEntries = 0
    var pageNo = 0
    var index = 0
    var dropDown = DropDown()
    var firstTime = true
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    @IBOutlet weak var CategoryView: UIView!
    @IBOutlet weak var CategoryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var NavTitle: UILabel!
    @IBOutlet weak var CategoryCV: UICollectionView!
    @IBOutlet weak var ProductCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(nav_Title)
        print(api_Key)
        setDropdown()
        wantLoader = true
//        self.categoryArray = categoryListArray1
        self.logEvent(category: nav_Title)
        self.categoryDetails(api_key: Int((self.api_Key!)), pageno: 0)
        
        ///// new line
      //  self.categoryDetails(api_key: Int(self.api_Key!) ?? 0, pageno: 0)
        
        NavTitle.text = nav_Title
        refreshControl.triggerVerticalOffset = 100.0
        refreshControl.tintColor = UIColor.MHGold
        ProductCV.bottomRefreshControl = refreshControl
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//            self.categoryDetails(api_key: Int(self.api_Key!), pageno: 0)
//        })
        refreshControl.addTarget(self, action: #selector(reloadList), for: .valueChanged)
//        NotificationCenter.default.addObserver(self, selector: #selector(reloadList), name: Notification.dashboardContentSetup, object: nil)
    }
    
    @objc func reloadList(){
        if self.totalEntries > self.pageNo{
            self.categoryDetails1(api_key: Int(api_Key) , pageno: pageNo + 1)
        }else{
            self.refreshControl.endRefreshing()
        }
    }
    func logEvent(category : String){
        Analytics.logEvent("Category_List_Opened", parameters: ["category" : category ])
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.categoryDetails(api_key: self.api_Key , pageno: 0)
        })
        
        if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
            if cartitemsCount == nil || cartitemsCount == 0{
                cartItemsView.isHidden = true
            }else{
                cartItemsView.isHidden = false
                CartItemLb.text = "\(cartitemsCount)"
            }
        }
    }
    
    @IBAction func navigationBTNtapped(_ sender: UIButton) {
        let dashboardvc = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
//        dashboardvc.categoryArray = self.categoryArray
//        frompush = false
        self.navigationController?.pushViewController(dashboardvc, animated: true)
    }
    @IBAction func CartBTNtapped(_ sender: Any) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
        WebViewVC.categoryArray = self.categoryArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
    }
    
    @IBAction func BackBTNtapped(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        if frompush {
            
           let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
       
        else {
            self.navigationController?.popViewController(animated: true)
            
        }
        
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        dropDown.show()
    }
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                    CartVC.categoryArray = self.categoryArray
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}
extension MHCategoryDetailsVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,FavoriteProductDelegate{
    
    
    func didSelectFavorite(indx: Int) {
        if let selectedProductFavStatus = ProductArray[indx].shortlist{
            var type:String = ""
            if selectedProductFavStatus == "0"{
                type = "1"
            }else{
                type = "2"
            }
            let productID = ProductArray[indx].productID ?? ""
            let deviceToken = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
            let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? "" //userID
            let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
            let parameters = ["action":"Shortlist",
                              "productid":"\(productID)",
                "userid":"\(userID)",
                "guestId": guest_id,
                "type":"\(type)",
                "deviceid": deviceToken,
                "devicetype": 2,
                "firebaseRegID": "1234"] as [String : Any]
            self.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            KVSpinnerView.show()
            NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
                print(response)
                switch status {
                case .noNetwork:
                    print("network error")
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                    Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
                case .success :
                    print("success")
                    print(response["data"]!)
                    if self.ProductArray[indx].shortlist == "0"{
                        self.ProductArray[indx].shortlist = "1"
                    }else{
                        self.ProductArray[indx].shortlist = "0"
                    }
                    self.ProductCV.reloadData()
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                case .failure :
                    print("failure")
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                    Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
                case .unknown:
                    print("unknown")
                    self.view.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    KVSpinnerView.dismiss()
                    Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                }
            }
        }
        
        ProductCV.reloadData()
    }
    
    func redirectToLogin() {
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC")
            self.navigationController?.push(viewController: vc)
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == CategoryCV{
            return 1
        }else{
            return 2
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CategoryCV{
            return categoryArray.count
        }else{
            if section == 0{
                return 1
            }else{
                return ProductArray.count
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CategoryCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCVC", for: indexPath) as! CategoryCVC
            cell.categoryLb.text = categoryArray[indexPath.row].name
            cell.CategoryIcon.kf.setImage(with: URL(string: categoryArray[indexPath.row].image))
            if index == indexPath.row{
                cell.ShadowView.borderColor = UIColor.MHGold
            }else{
                cell.ShadowView.borderColor = UIColor.lightGray.withAlphaComponent(0.4)
            }
            return cell
        }else{
            if indexPath.section == 0{
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryProductCVC", for: indexPath) as! CategoryProductCVC
                //            if cell == nil {
                //                cell = UITableViewCell(style: .default, reuseIdentifier: "CategoryProductCVC") as! CategoryProductCVC
                //            }
                cell.Title.text = ProductArray[indexPath.row].productname!
                cell.Productimage.kf.setImage(with: URL(string: ProductArray[indexPath.row].image!))
                cell.PercentageLb.text = "\(ProductArray[indexPath.row].offerpercent!) % Off"
                cell.StrikePrice.textColor = UIColor.red
                cell.StrikePrice.attributedText = "\(ProductArray[indexPath.row].strikeprice!)".updateStrikeThroughFont(UIColor.red)
                cell.OfferPrice.text  = "\(ProductArray[indexPath.row].specialprice!)"
                cell.delegate = self
                cell.index = indexPath.row
                if ProductArray[indexPath.row].shortlist == "0"{
                    cell.FavImg.image = UIImage(named: "favUnselected")
                }else{
                    cell.FavImg.image = UIImage(named: "favSelected")
                }
                //            if is_favArray[indexPath.row]{
                //                cell.FavImg.image = UIImage(named: "heart1")
                //                cell.FavView.layer.borderColor = UIColor.red.cgColor
                //            }else{
                //                cell.FavImg.image = UIImage(named: "Fav")
                //                cell.FavView.layer.borderColor = UIColor.black.withAlphaComponent(0.2).cgColor
                //            }
                return cell
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == CategoryCV{
            return CGSize(width: collectionView.frame.width / 3.4, height: collectionView.frame.height)
        }else{
            if indexPath.section == 0{
                return CGSize(width: collectionView.frame.width, height: 110)
            }else{
                let width = collectionView.frame.width / 2.1
                let multiplier = width / 8
                let height = 10 * multiplier
                return CGSize(width: width, height: height + 65)
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView != CategoryCV{
            if indexPath.row == ProductArray.count - 1 {
//                if totalEntries / limit <= totalEntries {
                if refreshControl.isRefreshing == false{
                    
//                    self.perform(#selector(loadTable), with: nil, afterDelay: 0.3)
                }
                
                    
//                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CategoryCV{
            self.logEvent(category: categoryArray[indexPath.row].name)
            self.index = indexPath.row
            self.NavTitle.text = categoryArray[indexPath.row].name
            self.ProductArray.removeAll()
            self.CategoryCV.reloadData()
            self.ProductCV.reloadData()
            self.api_Key = (categoryArray[indexPath.row].api_id)
            self.refreshControl.endRefreshing()
            self.firstTime = true
            self.categoryDetails(api_key: self.api_Key, pageno: 0)
            self.CategoryCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }else{
            if indexPath.section == 1{
                let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                MHProductDetailsVC.isFrom = .categoryDetails
                MHProductDetailsVC.BasicData2 = ProductArray[indexPath.row]
//                MHProductDetailsVC.categoryArray = categoryArray
//                MHProductDetailsVC.productID = ProductArray[indexPath.row].productID
                productID = ProductArray[indexPath.row].productID
                 frompush = false
                self.navigationController!.pushViewController(MHProductDetailsVC, animated: true)
            }
            
//            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = "https://www.masho.com/" + ProductArray[indexPath.row].productlink
//            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offset = ProductCV.contentOffset.y
//        let percentage = offset / 135
////        CategoryViewHeightConstraint.constant = 135 - ProductCV.contentOffset.y
//        CategoryView.alpha = 1 - percentage
//
//    }
    @objc func loadTable() {
        refreshControl.beginRefreshing()
        categoryDetails(api_key: api_Key, pageno: pageNo + 1)
        
    }
}
//extension MHCategoryDetailsVC: SkeletonCollectionViewDataSource,SkeletonCollectionViewDelegate{
//    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if skeletonView == CategoryCV{
//            return categoryArray.count
//        }else{
//            if section == 0{
//                return 1
//            }else{
//                return ProductArray.count
//            }
//
//        }
//    }
//    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
//        if skeletonView == CategoryCV{
//            return "CategoryCVC"
//        }else{
//
//            return "CategoryProductCVC"
//
//
//        }
//    }
//}
extension MHCategoryDetailsVC{
    func categoryDetails(api_key: Int,pageno: Int){

        let parameters = ["action":"getProductsByCatId",
                          "api_id":"\(api_key)",
                          "pageno":"\(pageno)",
                          "batchSize": "\(limit)",
            "userid" : UserID,
            "guest_id": guest_id] as [String : Any]
//        self.view.showAnimatedSkeleton()
//        self.CategoryCV.showSkeleton(usingColor: UIColor.red, transition: .none)
//        self.ProductCV.showSkeleton(usingColor: UIColor.blue, transition: .none)
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
       NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                var ProductArrayCopy1 = [CategoryProductModel]()
                var ProductArrayCopy2 = [CategoryProductModel]()
                ProductArrayCopy1.removeAll()
                ProductArrayCopy2.removeAll()
                if let PagedData = response["data"] as? [String:Any]{
                    self.GetProductDic = getProductsmodel(fromData: PagedData)
                    if let CategoryProducts = PagedData["products"] as? [[String:Any]]{
                        for item in CategoryProducts{
                            ProductArrayCopy1.append(CategoryProductModel(fromData: item))
                        }
                    }
                    self.categoryArray = [categoryListmodel]()
                    if let categories = PagedData["category"] as? [[String:Any]]{
                        for item in categories{
                            self.categoryArray.append(categoryListmodel(fromData: item))
                        }
                    }
                }
                if self.firstTime{
                    self.firstTime = false
                    for item in ProductArrayCopy1{
                        for item1 in ProductArrayCopy1{
                            if item.productID == item1.productID{
                                ProductArrayCopy2.append(item)
                            }
                        }
                    }
//                    if self.ProductArray.count == 0{
                        self.ProductArray = ProductArrayCopy2
//                    }
                }
                self.refreshControl.endRefreshing()
                self.CategoryCV.reloadData()
                self.ProductCV.reloadData()
                self.index = self.categoryArray.firstIndex{$0.api_id == self.api_Key} ?? -1
                self.CategoryCV.scrollToItem(at: IndexPath(item: self.index, section: 0), at: .centeredHorizontally, animated: true)
                self.totalEntries = self.GetProductDic.lastPage
                self.pageNo = Int(self.GetProductDic.currentPage)!
                print(self.ProductArray.count)
//                self.ProductCV.hideSkeleton()
//                self.CategoryCV.hideSkeleton()
//                self.view.hideSkeleton()
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
//    func categoryDetails1(api_key: IntOrString,pageno: Int){
    func categoryDetails1(api_key: Int,pageno: Int){
        
        let parameters = ["action":"getProductsByCatId",
                          "api_id":"\(api_key)",
            "pageno":"\(pageno)",
            "batchSize": "\(limit)",
            "userid" : UserID,
            "guest_id": guest_id] as [String : Any]

        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let PagedData = response["data"] as? [String:Any]{
                    self.GetProductDic = getProductsmodel(fromData: PagedData)
                    if let CategoryProducts = PagedData["products"] as? [[String:Any]]{
                        for item in CategoryProducts{
                            self.ProductArray.append(CategoryProductModel (fromData: item))
                        }
                    }
                    self.categoryArray = [categoryListmodel]()
                    if let categories = PagedData["category"] as? [[String:Any]]{
                        for item in categories{
                            self.categoryArray.append(categoryListmodel(fromData: item))
                        }
                    }
                }
                self.totalEntries = self.GetProductDic.lastPage
                self.pageNo = Int(self.GetProductDic.currentPage)!
                self.index = self.categoryArray.firstIndex{$0.api_id == self.api_Key!} ?? -1
                if self.index != -1{
                    self.CategoryCV.scrollToItem(at: IndexPath(item: self.index, section: 0), at: .centeredHorizontally, animated: true)
                }
                
                print(self.ProductArray.count)
                self.CategoryCV.reloadData()
                self.ProductCV.reloadData()
                self.refreshControl.endRefreshing()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension Array where Element: Equatable {
    func uniqueElements() -> [Element] {
        var out = [Element]()
        for element in self {
            if !out.contains(element) {
                out.append(element)
            }
        }
        return out
    }
}
