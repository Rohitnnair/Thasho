//
//  addToBagPopupCells.swift
//  masho
//
//  Created by Appzoc on 19/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
protocol ViewDropDowndelegate {
    func ViewDropDown(_ cell: Type2ListPopupTVC,index: Int)
}
class Type2ListPopupTVC: UITableViewCell{
    var delegate :ViewDropDowndelegate!
    var index : Int!
    @IBOutlet weak var SizeOrLengthLb: UILabel!
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var anchorView: UIView!
    @IBAction func ViewDropDownBTNtapped(_ sender: UIButton) {
        delegate.ViewDropDown(self, index: index)
    }
    
}
class Type2CirclePopupTVC: UITableViewCell,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    var postfieldsOptionsArray = [MHpostfieldsModel]()
    var index  : Int?
    var productID = ""
    var customSection = 0
    var size : String! = nil
    var length : String! = nil
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var SizeCV: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postfieldsOptionsArray[customSection].options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MHProductSizeCVC", for: indexPath) as! MHProductSizeCVC
        
        cell.SizeLb.text = postfieldsOptionsArray[customSection].options[indexPath.row].size
        DispatchQueue.main.async {
            cell.BAckView.layer.cornerRadius = cell.BAckView.frame.width / 2
        }
        if postfieldsOptionsArray[customSection].options[indexPath.row].size == "Large"{
            cell.SizeLb.text = "L"
        }else if postfieldsOptionsArray[customSection].options[indexPath.row].size == "Medium"{
            cell.SizeLb.text = "M"
        }else if postfieldsOptionsArray[customSection].options[indexPath.row].size == "Small"{
            cell.SizeLb.text = "S"
        }else{
            cell.SizeLb.text = postfieldsOptionsArray[customSection].options[indexPath.row].size
        }
        if index == indexPath.row || productID == postfieldsOptionsArray[customSection].options[indexPath.row].sizeId{
            cell.BAckView.layer.borderColor = UIColor.MHGold.cgColor
            cell.BAckView.backgroundColor = UIColor.MHGold
            cell.SizeLb.textColor = UIColor.white
            
            
            print("clicked fieldname = ",postfieldsOptionsArray[customSection].fieldname,"value clicked",postfieldsOptionsArray[customSection].options[indexPath.row].sizeId)
            
            dataDict[postfieldsOptionsArray[0].fieldname] =
            postfieldsOptionsArray[customSection].options[indexPath.row].sizeId
            SelecteD = true
            print("dict = ",dataDict)
            if SelectedSize != nil {
                dataDict[postfieldsOptionsArray[0].fieldname] =
                postfieldsOptionsArray[customSection].options[indexPath.row].sizeId
                print("Data ready to go ")
            }
            
//            if postfieldsOptionsArray[customSection].fieldname == "size"{
//                SelectedSize = postfieldsOptionsArray[customSection].options[indexPath.row].sizeId
//            }else{
//                SelectedLength = postfieldsOptionsArray[customSection].options[indexPath.row].sizeId
//            }
        }else{
            cell.BAckView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
            cell.BAckView.backgroundColor = UIColor.white
            cell.SizeLb.textColor = UIColor.black
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 7, height: collectionView.frame.width / 7)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        productID = ""
        collectionView.reloadData()
        
    }
    
}
