//
//  RescentProductsCells.swift
//  masho
//
//  Created by Appzoc on 19/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
protocol rescentProductsFavDelegate{
    func rescentProductsFav(index: Int, cell: RescentProductsCVC)
}
class RescentProductsCVC: UICollectionViewCell {
    var delegate : rescentProductsFavDelegate!
    var index : Int!
    @IBOutlet weak var FavImage: UIImageView!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var StrikePrice: UILabel!
    @IBOutlet weak var PriceLb: UILabel!
    @IBOutlet weak var PrecentageLb: UILabel!
    
    @IBAction func AddtoFavBTNtapped(_ sender: UIButton){
        delegate.rescentProductsFav(index: index, cell: self)
    }
    
}
