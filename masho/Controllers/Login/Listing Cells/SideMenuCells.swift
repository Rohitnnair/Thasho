//
//  SideMenuCells.swift
//  masho
//
//  Created by Appzoc on 09/06/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class SideMenuProfileTVC:UITableViewCell{
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var CameraiconView: UIView!
    @IBOutlet weak var welcomeMessage: UILabel!
}
class SideMenuAppVersionTVC:UITableViewCell{
    @IBOutlet weak var AppversionLBL: UILabel!
}
class SideMenuCVC: UICollectionViewCell {
    @IBOutlet weak var SideMenuLb: UILabel!
    @IBOutlet weak var SideMenuIcon: UIImageView!
    @IBOutlet weak var BackgroundColorView: UIView!
}
class SideMenuCountryCurrencyTVC:UITableViewCell{
    @IBOutlet weak var Flag: UIImageView!
    @IBOutlet weak var CountryLb: UILabel!
    @IBOutlet weak var languageLb: UILabel!
    @IBOutlet weak var currencyLb: UILabel!
}
class SideMenuOptionsTVC:UITableViewCell{
    @IBOutlet weak var SideMenuOptionsLb:UILabel!
    @IBOutlet weak var SideMenuOptionIcon:UIImageView!
}
class sectionTitleCVC: UICollectionViewCell {
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var SeperatorLine: UIView!
    @IBOutlet weak var ArrowIMG: UIImageView!
    @IBOutlet weak var BTN: UIButton!
}
class sectionContentCVC: UICollectionViewCell{
    @IBOutlet weak var IconImage: UIImageView!
    @IBOutlet weak var TitleLb: UILabel!
}
class country_language_currency_CVC: UICollectionViewCell{
    @IBOutlet weak var Flag: UIImageView!
    @IBOutlet weak var CountryLb: UILabel!
    @IBOutlet weak var languageLb: UILabel!
    @IBOutlet weak var currencyLb: UILabel!
}
class country_language_currency_TVC: UITableViewCell{
    @IBOutlet weak var TitleLb: UILabel!
}
class country_language_currency_1_TVC: UITableViewCell{
    @IBOutlet weak var Flag: UIImageView!
    @IBOutlet weak var CountryLb: UILabel!
}
class SideMenuTVC:UITableViewCell,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    var SideMenuArray = [SideMenuModel]()
    var ColorArray = [UIColor]()
    @IBOutlet weak var SideMenuCV: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SideMenuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SideMenuCVC", for: indexPath) as! SideMenuCVC
        cell.SideMenuIcon.kf.setImage(with: URL(string: (SideMenuArray[indexPath.row].image)!))
        cell.SideMenuLb.text = SideMenuArray[indexPath.row].leftmenuname
        cell.BackgroundColorView.backgroundColor = ColorArray[indexPath.row].withAlphaComponent(0.1)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3
        let height = width * (100/105)
        return CGSize(width: width, height: height)
    }
    
}
