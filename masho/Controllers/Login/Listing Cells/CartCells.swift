//
//  CartCells.swift
//  masho
//
//  Created by Appzoc on 13/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
class CartFirstCell : UITableViewCell{
    
    @IBOutlet weak var BackView: UIView!
    @IBOutlet weak var TextLb: UILabel!
}
class CartSecondCell : UITableViewCell{
    @IBOutlet weak var TextLb: UILabel!
}
class CartThirdCell : UITableViewCell{
    @IBOutlet weak var WhiteView: UIView!
    @IBOutlet weak var TextLb1: UILabel!
    @IBOutlet weak var TextLb2: UILabel!
}
protocol showDropdownDelegate{
    func showDropdown(cell: MHCartProductTVC,index: Int)
}
class MHCartProductTVC: UITableViewCell{
    var delegate : showDropdownDelegate!
    var index : Int!
    @IBOutlet weak var TextLb: UILabel!
    @IBOutlet weak var EmptyView: UIView!
    @IBOutlet weak var qtyPriceLb: UILabel!
    @IBOutlet weak var RemoveBTN: UIButton!
    @IBOutlet weak var QuantityLb: UILabel!
    @IBOutlet weak var PriceLb: UILabel!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var ProductTitle: UILabel!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var BorderLine: UIView!
    @IBOutlet weak var anchorView: UIView!
    @IBAction func showdropDownBTNtapped(_ sender: UIButton) {
        delegate.showDropdown(cell: self, index: index)
    }
    
}
class MHOrderSummaryTVC: UITableViewCell{
    @IBOutlet weak var BackView: UIView!
    @IBOutlet weak var FirstLb: UILabel!
    @IBOutlet weak var PriceLb: UILabel!
    @IBOutlet weak var SubTxtLb: UILabel!
}
class QuantityCVC: UICollectionViewCell{
    
    @IBOutlet weak var QtyLb: UILabel!
    @IBOutlet weak var CircleView: BaseView!
}
