//
//  SplashVC.swift
//  masho
//
//  Created by Appzoc on 12/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import CoreLocation
class SplashVC: UIViewController,CLLocationManagerDelegate {
    var GuestData : guestModel! = nil
    var CountryCode : String = "IN"
    var CountryName : String = "India"
    let locale = Locale.current
    var locationManager = CLLocationManager()
    var updateData : updateModel!
    
    let guestId = UserDefaults.standard.object(forKey: "guestId") as? Int
    var isAccessLocation = UserDefaults.standard.value(forKey: "isAccessLocation") as? Bool ?? false
    let ShowCountryPopup = UserDefaults.standard.value(forKey: "ShowCountryPopup") as? Bool ?? true
    @IBOutlet weak var NoThanksBTN: UIButton!
    @IBOutlet weak var newUpdateView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.activityStartAnimating(activityColor: UIColor(red: 250/256, green: 102/256, blue: 57/256, alpha: 1))
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.newUpdateView.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.view.activityStopAnimating()
            
            self.AutoUpdate()
        })
       
//        if CLLocationManager.locationServicesEnabled() {
//            self.locationManager.delegate = self
//            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            self.locationManager.startUpdatingLocation()
//        }
       
    }
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        NameOfCountry(location: CLLocation(latitude: locValue.latitude, longitude: locValue.longitude))
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    func NameOfCountry(location: CLLocation){
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
            guard let self = self else { return }
            
            if let _ = error {
                //TODO: Show alert informing the user
                return
            }
            
            guard let placemark = placemarks?.first else {
                //TODO: Show alert informing the user
                return
            }
            
            self.CountryName = placemark.country ?? ""
            UserDefaults.standard.set(self.CountryName, forKey: "CountryName")
            self.CountryCode = placemark.isoCountryCode ?? ""
//            print("Country",Country)
            print("CountryCode",self.CountryCode)
            self.AutoUpdate()
        }
    }
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    @IBAction func NoThanksBTNtapped(_ sender: Any) {
        if guestId == nil{
            self.guestlogin()
        }else{
            UserDefaults.standard.set(false, forKey: "ShowCountryPopup")
            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: false)
        }
    }
    
    @IBAction func UpdateBTNtapped(_ sender: Any) {
        if let url = URL(string: "https://apps.apple.com/us/app/thasho/id1616825242"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
}
extension SplashVC{
    func guestlogin(){
        self.view.activityStopAnimating()
        self.view.activityStartAnimating(activityColor: UIColor(red: 250/256, green: 102/256, blue: 57/256, alpha: 1))
        let parameters = [  "action" : "guestlogin",
                            "countrycode" : self.CountryCode,
                            "countryname" : self.CountryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.activityStopAnimating()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let guestData = response["data"] as? [String:Any]{
                    self.GuestData = guestModel(fromData: guestData)
                }
                UserDefaults.standard.set(self.GuestData.guestid, forKey: "guestId")
                UserDefaults.standard.set(true, forKey: "isAccessLocation")
                if self.ShowCountryPopup != false{
                    UserDefaults.standard.set(true, forKey: "ShowCountryPopup")
                }
                self.view.activityStopAnimating()
                let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
                self.navigationController?.pushViewController(dashBoardVC, animated: false)
            case .failure :
                print("failure")
                self.view.activityStopAnimating()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.activityStopAnimating()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }
    func AutoUpdate(){
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let systemVersion = UIDevice.current.systemVersion as? String
        print(appVersion)
        print(systemVersion)
        self.view.activityStopAnimating()
        self.view.activityStartAnimating(activityColor: UIColor(red: 250/256, green: 102/256, blue: 57/256, alpha: 1))
        let parameters = [  "action" : "appupdate",
                            "version" : appVersion!,
                            "os" : systemVersion!,
                            "apptype" : 2 ] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.activityStopAnimating()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let appUpdateData = response["data"] as? [String : Any]{
                    self.updateData = updateModel(fromData: appUpdateData)
                }
                self.view.activityStopAnimating()
                if self.updateData != nil{
                    if self.updateData.normal == 0 && self.updateData.force == 0{
                        if self.isAccessLocation{
                            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
                            self.navigationController?.pushViewController(dashBoardVC, animated: false)
                        }else{
                            UserDefaults.standard.set(self.CountryName, forKey: "CountryName")
                            self.guestlogin()
                        }
//                        if self.guestId == nil{
//                            self.guestlogin()
//                        }else{
//                            UserDefaults.standard.set(false, forKey: "ShowCountryPopup")
//                            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
//                            self.navigationController?.pushViewController(dashBoardVC, animated: false)
//                        }
                    }else{
                        self.newUpdateView.isHidden = false
                        if self.updateData.normal == 0 && self.updateData.force == 1{
                            self.NoThanksBTN.isHidden = true
                        }else{
                            self.NoThanksBTN.isHidden = false
                        }
                    }
                }
                
            case .failure :
                print("failure")
                self.view.activityStopAnimating()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.activityStopAnimating()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)

            }
        }
    }
}
class updateModel {
    var normal : Int!
    var force : Int!
    init(fromData data: [String:Any]) {
        self.normal = data["normal"] as? Int
        self.force = data["force"] as? Int
    }
}
