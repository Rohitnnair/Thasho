
import Foundation
import NotificationBannerSwift


class Banner {
    
    static let main = Banner()
    private init(){}
    var banner:GrowingNotificationBanner?
    var currentTitle : String = "as"
    func showBanner(title: String, subtitle: String,style:BannerStyle?){
        banner?.bannerQueue.removeAll()
        DispatchQueue.main.async {
            self.currentTitle = title
            self.banner?.dismiss()
            let neWbanner = GrowingNotificationBanner(title: title, subtitle: subtitle, style: style ?? .info)
            neWbanner.backgroundColor = UIColor.MHGold
            neWbanner.show()
            neWbanner.autoDismiss = true
            neWbanner.dismissOnTap = true
            neWbanner.dismissOnSwipeUp = true
            neWbanner.duration = 1
//            neWbanner.dismissDuration = 0.2
            self.banner = neWbanner
            self.currentTitle  = title
        }
    }

    
}

class utility {
    class func displayBanner(duration: TimeInterval = 1, title: String = "", message: String, alignment: NSTextAlignment? = nil, colour: UIColor? = nil, style: BannerStyle) {
            DisplayBanner.shared.show(duration: duration, title: title, message: message, alignment: alignment, colour: colour, style: style)
        }
}
