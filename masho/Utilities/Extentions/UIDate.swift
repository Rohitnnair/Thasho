//
//  UIDate.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

extension Date {
    func getStringFrom(dateFormat outFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = outFormat
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: self)
    }
}
