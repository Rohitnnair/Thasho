//
//  Common.swift
//  DayToFresh
//
//  Created by farhan k on 11/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    func change(textFont : UIFont?,backGroundColor:UIColor?,textColor:UIColor?,tintColor:UIColor?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            if let textField = view as? UITextField {
                textField.font = textFont
                if let bgColor = backGroundColor {
                    textField.backgroundColor = bgColor
                }
                if let _textColor = textColor {
                    textField.textColor = _textColor
                }
                if let _tintColor = tintColor {
                    textField.tintColor = _tintColor
                }
            }
        }
    }
    
}

