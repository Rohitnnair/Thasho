//
//  BannerNotification.swift
//  EmployeeApp
//
//  Created by Arun Induchoodan on 19/10/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import AVFoundation

class DisplayBanner {
    
    static let shared = DisplayBanner()
    
    private init(){}
    
    func show(duration:TimeInterval = 1,
              title:String = "",
              message:String,
              alignment:NSTextAlignment? = nil,
              colour:UIColor? = nil,
              style:BannerStyle = .success) {
        DispatchQueue.main.async {
            if UIApplication.shared.applicationState == .active {
                if NotificationBannerQueue.default.numberOfBanners == 0{
                    let banner = FloatingNotificationBanner(title: title,
                                                            subtitle: message,
                                                            titleTextAlign: alignment,
                                                            subtitleTextAlign:alignment,
                                                            style: style)
                    banner.duration = duration
                    if colour != nil {
                        banner.backgroundColor = colour!
                    }
                    AudioServicesPlayAlertSound(1519)
                    banner.show(
                        bannerPosition:.bottom,
                        cornerRadius: 8,
                        shadowColor: UIColor(red: 0.431, green: 0.459, blue: 0.494, alpha: 1),
                        shadowBlurRadius: 0,
                        shadowEdgeInsets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
                }
            }
        }
        
    }
    
  
    func showNotificationMessage(title : String, message: String, type: Int, extra: String){
        let banner = NotificationBanner(title: title, subtitle: message, leftView: nil, style: .success)
        banner.layer.cornerRadius = 5
        banner.backgroundColor = UIColor.black
        banner.duration = 5
        banner.onTap = {
            if let nav = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController{
                if let topController = nav.topViewController{
                switch type{
                case 1:
                    if let topController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                        print(topController)
                        if let cntrlr = topController.topViewController {
                            print(cntrlr)
//                            if ((cntrlr as? JMainTaBarController) != nil) {
//                                if let xvc = cntrlr as? JMainTaBarController {
//                                    if xvc.selectedIndex == 1 {
//                                        print(cntrlr)
//                                        xvc.viewDidLoad()
//                                    }else {
//                                        print(cntrlr)
//                                        xvc.selectedIndex = 1
//                                        xvc.viewDidLoad()
//                                    }
//                                }
//                            }
                        }

                    }
                case 2:
                    break
                default:
                    break
                }
                }
        }
        
        // topController should now be your topmost view controller
    }
                
    banner.onSwipeUp = {
            banner.dismiss()
            // Do something regarding the banner
        }
        banner.show(queuePosition: .front, bannerPosition: .top)
    }
//
//    static func failureBanner(duration:TimeInterval = 1,title:String,message:String,color:UIColor = #colorLiteral(red: 0.0862745098, green: 0.0862745098, blue: 0.09019607843, alpha: 1)){
//        DispatchQueue.main.async {
//            if UIApplication.shared.applicationState == .active {
//                if NotificationBannerQueue.default.numberOfBanners == 0{
//                    let banner = FloatingNotificationBanner(title: title,
//                                                            subtitle: message,
//                                                            style: .danger)
//                    banner.backgroundColor = color
//                    banner.show(
//                        bannerPosition:.bottom,
//                        cornerRadius: 8,
//                        shadowColor:.clear,
//                        shadowBlurRadius: 16,
//                        shadowEdgeInsets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
//                }
//            }
//        }
//    }
//
//    static func failureBanner(message:String,color:UIColor = #colorLiteral(red: 0.0862745098, green: 0.0862745098, blue: 0.09019607843, alpha: 1)){
//        failureBanner(title: "", message: message,color:color)
//    }
//
//    static func showNetoworkConnectionBanner(isConnected:Bool,duration:TimeInterval = 1,title:String?,message:String?){
//        DispatchQueue.main.async {
//            if UIApplication.shared.applicationState == .active {
//                if NotificationBannerQueue.default.numberOfBanners == 0 {
//                    let style:BannerStyle = isConnected ? .success : .danger
//                    let banner = FloatingNotificationBanner(title: title,
//                                                            subtitle: message,
//                                                            titleTextAlign: .center,
//                                                            subtitleTextAlign:.center,
//                                                            style: style)
//                    banner.duration = 1
//                    AudioServicesPlayAlertSound(1519)
//                    banner.show(
//                        bannerPosition:.bottom,
//                        cornerRadius: 8,
//                        shadowColor: UIColor(red: 0.431, green: 0.459, blue: 0.494, alpha: 1),
//                        shadowBlurRadius: 0,
//                        shadowEdgeInsets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
//                }
//           }
//        }
//
//    }
    
}
