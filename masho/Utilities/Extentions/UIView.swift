//
//  UIView.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

extension UIView {
    
    /* Usage Example
     * bgView.addBottomRoundedEdge(desiredCurve: 1.5)
     */
    func addBottomRoundedEdge(desiredCurve: CGFloat?) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }
    
    func animShow(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
    
    func makeRoundedCorner(clipToBounds:Bool) {
        DispatchQueue.main.async {
            self.clipsToBounds = clipToBounds
            self.layer.cornerRadius = self.frame.width/2
        }
    }
    
    func makeHalfRoundCorner(clipToBounds:Bool) {
        DispatchQueue.main.async {
            self.clipsToBounds = clipToBounds
            self.layer.cornerRadius = self.frame.height/2
        }
    }
    
    func scaleViewTo(_ x:CGFloat,_ y:CGFloat,_ z:CGFloat) {
        self.layer.transform = CATransform3DMakeScale(x, y, z)
    }
    
    func removeScale() {
        UIView.animate(withDuration: 0.2) {
            self.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }
    }
    
    func drawDottedLine(line dashPattern:[NSNumber],color:UIColor) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = dashPattern // 7 is the length of dash, 3 is length of the gap.
        let p0 = CGPoint(x: self.bounds.minX, y: self.bounds.midY)
        let p1 = CGPoint(x: self.bounds.maxX, y: self.bounds.midY)
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
        func takeScreenshot() -> UIImage {
            
            // Begin context
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
            
            // Draw view in that context
            drawHierarchy(in: self.bounds, afterScreenUpdates: true)
            
            // And finally, get image
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            if (image != nil)
            {
                return image!
            }
            return UIImage()
        }
    
    
    
    
    func activityStartAnimating(activityColor: UIColor) {
        
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = UIColor.clear
        backgroundView.tag = 475647
        
        
        var btr: UIImageView!
        btr = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        btr.center = CGPoint(x: self.frame.width / 2, y: (self.frame.height / 4) * 3)//self.center
//        aView.frame = CGRectMake( 100, 200, aView.frame.size.width, aView.frame.size.height ); // set new position exactly

        btr.image = UIImage(named: "loader")
        btr.contentMode = .scaleAspectFit
        rotateView(targetView: btr)
        
        backgroundView.addSubview(btr)
        
        self.addSubview(backgroundView)
    }
    
    func rotateView(targetView: UIView, duration: Double = 0.5) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(M_PI))
        }) { finished in
            self.rotateView(targetView: targetView, duration: 0.5)
        }
    }
    
    
    func activityStopAnimating() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
    func addDashedBorder() {
    let color = UIColor.lightGray.cgColor
    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 2
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [3,3]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 10).cgPath
    self.layer.addSublayer(shapeLayer)
    }
}
class CustomDashedView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0

    var dashBorder: CAShapeLayer?

    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}
