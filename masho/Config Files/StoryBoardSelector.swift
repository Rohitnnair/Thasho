//
//  StoryBoardSelector.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

struct StoryBoard {
    
    static var login:UIStoryboard{
        return UIStoryboard(name: "Login", bundle: nil)
    }
    static var main:UIStoryboard{
        return UIStoryboard(name: "Main", bundle: nil)
    }
    static var new:UIStoryboard{
        return UIStoryboard(name: "New", bundle: nil)
    }
    static var filter:UIStoryboard{
        return UIStoryboard(name: "Filter", bundle: nil)
    }
}
