//
//  SessionHandler.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import Foundation

class SessionHandler {
    
    static let userDefaults = UserDefaults.standard
    
    static var userID: String? {
        get {
            if let value = userDefaults.value(forKey: "userID") as? String {
                return value
            } else {
                return nil
            }
        } set {
            userDefaults.set(newValue, forKey: "userID")
        }
    }
    
    static var userName:String? {
        get {
            if let value = userDefaults.value(forKey: "userName") as? String {
                return value
            } else {
                return nil
            }
        } set {
            userDefaults.set(newValue, forKey: "userName")
        }
    }
    
    static var isLoggedIn: Bool {
        get {
            if let _ = userID {
                return true
            } else {
                return false
            }
        }
    }

    static var isAddressSelected : Bool = false 
}
